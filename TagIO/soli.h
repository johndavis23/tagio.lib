// soli.h
#ifndef __soli_h__
#define __soli_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	SOUND_LIST_DEFINITION_GROUP_TAG= 'soli',
	SOUND_LIST_DEFINITION_VERSION_NUMBER= 1,

	MAXIMUM_SOUND_LIST_DEFINITIONS_PER_MAP= 16
};

/* ---------- sound list */

enum
{
	MAXIMUM_SOUNDS_PER_SOUND_LIST_DEFINITION= 30
};

struct sound_list_entry
{
	file_tag tag;
	
	short unused[1];

	short index;
};

#define SIZEOF_STRUCT_SOUND_LIST_DEFINITION 256
struct sound_list_definition
{
	short number_of_sounds;
	
	short unused[7];
	
	struct sound_list_entry sounds[MAXIMUM_SOUNDS_PER_SOUND_LIST_DEFINITION];
};


//=====================================================================================================
bool read_sound_list_definition( sound_list_definition* def, tag id );
bool save_sound_list_definition( sound_list_definition* def, tag id );

bool read_sound_list_definition( sound_list_definition* def, tag_entry* e );
bool save_sound_list_definition( sound_list_definition* def, tag_entry* e );




#endif