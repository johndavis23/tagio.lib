// lpgr.h
#ifndef __lpgr_h__
#define __lpgr_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	LOCAL_PROJECTILE_GROUP_DEFINITION_GROUP_TAG= 0x6C706772, // 'lpgr' (looks like text)
	LOCAL_PROJECTILE_GROUP_DEFINITION_VERSION_NUMBER= 1,

	MAXIMUM_LOCAL_PROJECTILE_GROUP_DEFINITIONS_PER_MAP= 64
};

/* ---------- local projectile group */

enum // sequences
{
	_group_sequence_in_flight,
	_group_sequence_stain,
	NUMBER_OF_LOCAL_PROJECTILE_GROUP_SEQUENCES,
	
	MAXIMUM_NUMBER_OF_LOCAL_PROJECTILE_GROUP_SEQUENCES= 2
};

enum { // transfer modes
	_transfer_mode_normal,
	_transfer_mode_fade_in_over_time,
	_transfer_mode_fade_in_over_animation,
	_transfer_mode_fade_out_over_time,
	_transfer_mode_fade_out_over_animation
};

enum // local projectile group definition flags
{
	_group_projectiles_never_get_recreated_bit,
	_group_projectiles_affected_by_wind_bit,
	_group_projectiles_deleted_when_it_hits_the_ground_bit,
	_group_projectiles_deleted_after_time_expires_bit,
	
	_group_projectiles_deleted_after_animation_completes_bit,
	_group_projectiles_does_not_animate_bit,
	_group_has_startup_transition_bit,
	_group_has_finite_duration_bit,
	
	_group_has_ending_transition_bit,
	_group_startup_transition_affects_transparency_bit,
	_group_startup_transition_affects_target_number_bit,
	_group_always_update_physics_bit,
	
	_group_particles_stay_relative_to_the_center_bit,
	_group_ending_transition_affects_transparency_bit,
	_group_ending_transition_affects_target_number_bit,
	_group_ending_transition_stop_creating_particles_bit,
	
	_group_delete_after_particles_are_gone_bit,
	_group_has_startup_delay_bit,
	_group_startup_transition_affects_scale_bit,
	_group_ending_transition_affects_scale_bit,
	
	_group_particles_retain_spawn_center_bit,
	_group_projectiles_are_created_symmetrically_bit,
	_group_projectiles_dont_respect_height_above_ground_bit,
	_group_projectiles_always_start_at_first_animation_frame_bit,
	
	_group_casts_reflections_bit,
	_group_is_media_surface_effect_bit,
	_group_update_per_tick_bit,
	_group_uses_owner_color_table_index_bit,
	
	_group_uses_skelmodel_bit,
	_group_chains_another_lpgr_bit,
	_group_uses_sprite_bit,
	_group_projectiles_rotate_based_on_view_angle_bit,
	
	NUMBER_OF_LOCAL_PROJECTILE_GROUP_DEFINITION_FLAGS,

	_group_projectiles_never_get_recreated_flag= FLAG(_group_projectiles_never_get_recreated_bit),
	_group_projectiles_affected_by_wind_flag= FLAG(_group_projectiles_affected_by_wind_bit),
	_group_projectiles_deleted_when_it_hits_the_ground_flag= FLAG(_group_projectiles_deleted_when_it_hits_the_ground_bit),
	_group_projectiles_deleted_after_time_expires_flag= FLAG(_group_projectiles_deleted_after_time_expires_bit),
	_group_projectiles_deleted_after_animation_completes_flag= FLAG(_group_projectiles_deleted_after_animation_completes_bit),
	_group_projectiles_does_not_animate_flag= FLAG(_group_projectiles_does_not_animate_bit),

	_group_has_startup_transition_flag= FLAG(_group_has_startup_transition_bit),
	_group_has_finite_duration_flag= FLAG(_group_has_finite_duration_bit), 
	_group_has_ending_transition_flag= FLAG(_group_has_ending_transition_bit),
	_group_startup_transition_affects_transparency_flag= FLAG(_group_startup_transition_affects_transparency_bit),
	_group_startup_transition_affects_target_number_flag= FLAG(_group_startup_transition_affects_target_number_bit),
	_group_always_update_physics_flag= FLAG(_group_always_update_physics_bit),
	_group_particles_stay_relative_to_the_center_flag= FLAG(_group_particles_stay_relative_to_the_center_bit),
	_group_ending_transition_affects_transparency_flag= FLAG(_group_ending_transition_affects_transparency_bit),
	_group_ending_transition_affects_target_number_flag= FLAG(_group_ending_transition_affects_target_number_bit),
	_group_ending_transition_stop_creating_particles_flag= FLAG(_group_ending_transition_stop_creating_particles_bit),
	_group_delete_after_particles_are_gone_flag= FLAG(_group_delete_after_particles_are_gone_bit),
	_group_has_startup_delay_flag= FLAG(_group_has_startup_delay_bit),
	_group_startup_transition_affects_scale_flag= FLAG(_group_startup_transition_affects_scale_bit),
	_group_ending_transition_affects_scale_flag= FLAG(_group_ending_transition_affects_scale_bit),
	_group_particles_retain_spawn_center_flag= FLAG(_group_particles_retain_spawn_center_bit),
	_group_projectiles_are_created_symmetrically_flag= FLAG(_group_projectiles_are_created_symmetrically_bit),
	_group_projectiles_dont_respect_height_above_ground_flag= FLAG(_group_projectiles_dont_respect_height_above_ground_bit),
	_group_projectiles_always_start_at_first_animation_frame_flag= FLAG(_group_projectiles_always_start_at_first_animation_frame_bit),
	_group_casts_reflections_flag= FLAG(_group_casts_reflections_bit),
	_group_is_media_surface_effect_flag= FLAG(_group_is_media_surface_effect_bit),
	_group_update_per_tick_flag= FLAG(_group_update_per_tick_bit),
	_group_uses_owner_color_table_index_flag= FLAG(_group_uses_owner_color_table_index_bit),
	_group_uses_skelmodel_flag= FLAG(_group_uses_skelmodel_bit),
	_group_chains_another_lpgr_flag= FLAG(_group_chains_another_lpgr_bit),
	_group_uses_sprite_flag= FLAG(_group_uses_sprite_bit),
	_group_projectiles_rotate_based_on_view_angle_flag= FLAG(_group_projectiles_rotate_based_on_view_angle_bit)
};

#define SIZEOF_STRUCT_LOCAL_PROJECTILE_GROUP_DEFINITION 128
struct local_projectile_group_definition
{
	unsigned long flags;
	
	file_tag collection_reference_tag;
	file_tag physics_tag; // Can be NONE
	
	short sequence_indexes[MAXIMUM_NUMBER_OF_LOCAL_PROJECTILE_GROUP_SEQUENCES];
	
	short_fixed scale_lower_bound, scale_delta;
	short_fixed animation_rate_lower_bound, animation_rate_delta;
	
	short target_number_lower_bound, target_number_delta;
	world_distance radius_lower_bound, radius_delta;

	int expiration_time_lower_bound, expiration_time_delta;

	short transfer_mode;

	short_world_distance height_offset;
	
	short startup_transition_time_lower_bound, startup_transition_time_delta;
	short ending_transition_time_lower_bound, ending_transition_time_delta;
	int duration_lower_bound, duration_delta;

	short_world_distance initial_z_velocity;
	short_world_distance z_acceleration;
	
	short startup_delay_lower_bound, startup_delay_delta;
	
	short unused2[4];
	
	file_tag chain_to_lpgr; // can be NONE
	file_tag local_light_tag;
	
	short unused[14];

	// computed during postprocessing
	
	short collection_reference_index;
	short collection_index, color_table_index;
	short physics_index;
	short chain_to_lpgr_index;
	short local_light_index;
};


//=====================================================================================================
bool read_local_projectile_group_definition( local_projectile_group_definition* def, tag id );
bool save_local_projectile_group_definition( local_projectile_group_definition* def, tag id );

bool read_local_projectile_group_definition( local_projectile_group_definition* def, tag_entry* e );
bool save_local_projectile_group_definition( local_projectile_group_definition* def, tag_entry* e );


#endif