
#include "meli.h"


#include "tagpick.h"
#include <assert.h>


// ==============================  BYTE SWAPPING DATA  ========================================
byte_swap_code mesh_lighting_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	// fixed_vector3d incident_light; // direction of incident light (.k<0)
	_4byte,	_4byte,	_4byte,

	// The diffuse light intensity ranges from 0.0 to maximum_diffuse_light_intensity.
	// (ambient_light_intensity +  diffuse_light_intensity) is pinned
	// between 0.0 and maximum_total_light_intensity.
	// fixed ambient_light_intensity;	
	// fixed maximum_diffuse_light_intensity;
	// fixed model_shading_diffuse_light_intensity; // when a pixel is shaded by the model, it 
													// gets this value for the diffuse light intensity.	
	_4byte,
	_4byte,
	_4byte,
	
	// fixed maximum_total_light_intensity; // [0.0,1.0]
	_4byte,

	// short maximum_shadow_distance; // in mesh texture pixels
	_2byte,	

	// short filter_width;
	_2byte,	
	// real gaussian_width;
	_4byte,

	//short unused[14];
	14*sizeof(short),

	_end_bs_array
};

mesh_lighting_data default_meli =
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};


// ==============================  EDIT WINDOW  ========================================
void edit_meli( tag_entry* e )
{
}

void defaultnew_meli( tag_entry *e )
{
	mesh_lighting_data def;
	byte_swap_move_be( "meli", &def, &default_meli, sizeof(mesh_lighting_data), 1, mesh_lighting_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_meli( tag_entry *e )
{
    #pragma unused( e )

#if 0 // no dependencies
	mesh_lighting_data def;
	read_mesh_lighting_data( &def, e );
#endif	// no dependencies
}


// ==============================  READ TAG DATA  ========================================
bool read_mesh_lighting_data( mesh_lighting_data* def, tag_entry* e )
{
	return load_tag_data_into_struct( "meli", e, def, SIZEOF_STRUCT_MESH_LIGHTING_DATA, 1, mesh_lighting_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_mesh_lighting_data( mesh_lighting_data* def, tag_entry* e )
{
	return save_tag_data_from_struct( "meli", e, def, SIZEOF_STRUCT_MESH_LIGHTING_DATA, 1, mesh_lighting_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_mesh_lighting_data( mesh_lighting_data* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'meli', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_mesh_lighting_data( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_mesh_lighting_data( mesh_lighting_data* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'meli', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_mesh_lighting_data( def, e );
}


