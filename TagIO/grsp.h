
#ifndef __grsp_h__
#define __grsp_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

enum {
	GROUND_SPRITE_NAME_LENGTH = 63,
	GROUND_SPRITE_LAYERS_PER_MAP = 4
};

struct ground_sprite_layer {
	file_tag sprites;
	unsigned mask_offset;
	unsigned mask_length;
	void *mask_data; // runtime
	unsigned short density;
	short padding[9];
	char name[GROUND_SPRITE_NAME_LENGTH+1];
};

struct ground_sprites_definition {
	struct ground_sprite_layer layers[GROUND_SPRITE_LAYERS_PER_MAP];
};



#endif

