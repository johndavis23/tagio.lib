
// glli.h
#ifndef __glli_h__
#define __glli_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif


#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	GLOBAL_LIGHT_DEFINITION_GROUP_TAG = 'glli',	// 'glli'
	GLOBAL_LIGHT_DEFINITION_VERSION_NUMBER = 1
};


struct glli_definition {
	struct rgb_color ambient_color;
	struct rgb_color sky_color;
	struct rgb_color key_color;
	struct rgb_color fill_color;
	fixed_vector3d key_incident_direction;
	fixed_vector3d fill_incident_direction;
};



//=====================================================================================================
bool read_glli_definition( glli_definition* def, tag id );
bool save_glli_definition( glli_definition* def, tag id );

bool read_glli_definition( glli_definition* def, tag_entry* e );
bool save_glli_definition( glli_definition* def, tag_entry* e );


#endif