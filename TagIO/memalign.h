
#ifndef __memalign_h__
#define __memalign_h__

#ifdef __cplusplus
extern "C" {
#endif

void *alloc_align( int size, int align );
void free_align( void *ptr );

#ifdef __cplusplus
}
#endif

#endif