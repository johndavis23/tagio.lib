// core.h
#ifndef __core_h__
#define __core_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* -------------- defines from other files ------------ */
#define MAXIMUM_HUE_CHANGES_PER_COLLECTION 8


/* ---------- constants */

enum
{
	COLLECTION_REFERENCE_GROUP_TAG= 0x636f7265, // 'core' (looks like text)
	COLLECTION_REFERENCE_VERSION_NUMBER= 1,
	
	MAXIMUM_COLLECTION_REFERENCES_PER_MAP= 320,
	
	MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE= 5
};

/* ---------- collection references */

#define SIZEOF_STRUCT_COLLECTION_REFERENCE 384
struct collection_reference
{
	file_tag collection_tag;

	short number_of_permutations;
	short_fixed tint_fraction;

	struct rgb_color colors[MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE][MAXIMUM_HUE_CHANGES_PER_COLLECTION];

	struct rgb_color tint_color;

	short unused[18];

	// computed when postprocessing this definition
	
	short collection_index;
	short color_table_indexes[MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE];
};



//=====================================================================================================
bool read_collection_reference( collection_reference* def, tag id );
bool save_collection_reference( collection_reference* def, tag id );

bool read_collection_reference( collection_reference* def, tag_entry* e );
bool save_collection_reference( collection_reference* def, tag_entry* e );



#endif