// meli.h
#ifndef __meli_h__
#define __meli_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif


#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

#ifndef __mesh_h__
#include "mesh.h"
#endif

/* ---------- constants */

enum
{
	MESH_LIGHTING_DEFINITION_GROUP_TAG= 0x6d656c69, // 'meli' (looks like text)
	MESH_LIGHTING_VERSION_NUMBER= 1,

	MAXIMUM_MAXIMUM_SHADOW_DISTANCE= 8*SUBMESH_CELL_TEXTURE_WIDTH
};

/* ---------- mesh lighting data structure */

#define SIZEOF_STRUCT_MESH_LIGHTING_DATA 64
struct mesh_lighting_data
{
	fixed_vector3d incident_light; // direction of incident light (.k<0)

	// The diffuse light intensity ranges from 0.0 to maximum_diffuse_light_intensity.
	// (ambient_light_intensity +  diffuse_light_intensity) is pinned
	// between 0.0 and maximum_total_light_intensity.
	fixed ambient_light_intensity;	
	fixed maximum_diffuse_light_intensity;
	fixed model_shading_diffuse_light_intensity; // when a pixel is shaded by the model, it 
												// gets this value for the diffuse light intensity.	
	fixed maximum_total_light_intensity; // [0.0,1.0]

	short maximum_shadow_distance; // in mesh texture pixels
	
	short filter_width;
	real gaussian_width;
	
	short unused[14];
};


//=====================================================================================================
bool read_mesh_lighting_data( mesh_lighting_data* def, tag id );
bool save_mesh_lighting_data( mesh_lighting_data* def, tag id );

bool read_mesh_lighting_data( mesh_lighting_data* def, tag_entry* e );
bool save_mesh_lighting_data( mesh_lighting_data* def, tag_entry* e );



#endif