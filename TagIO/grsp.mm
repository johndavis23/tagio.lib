
#include "grsp.h"

 

 
 
#include "tagpick.h"
#include <string.h>
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
byte_swap_code ground_sprite_byte_swapping_data[]=
{
	_begin_bs_array, GROUND_SPRITE_LAYERS_PER_MAP,

	_4byte, // sprites
	_4byte, // mask_offset
	_4byte, // mask_length
	4,		// mask_data_ptr at runtime
	_2byte, // density
	9*2,	// padding
	GROUND_SPRITE_NAME_LENGTH+1,
	
	_end_bs_array
};


// ==============================  EDIT WINDOW  ========================================
void edit_grsp( tag_entry* e )
{
	
}

void defaultnew_grsp( tag_entry* e )
{
	ground_sprites_definition temp;
	memset( &temp, 0, sizeof(temp) );
	for (int i = 0; i < GROUND_SPRITE_LAYERS_PER_MAP; ++i) {
		temp.layers[i].sprites = -1;
	}
	byte_swap_data_be( "new ground sprite", &temp, sizeof(temp), 1, ground_sprite_byte_swapping_data );
	set_entry_data( e, &temp, sizeof(temp), false );
}

void dependency_grsp( tag_entry *e )
{
	ground_sprites_definition def;
	load_tag_data_into_struct( "grsp", e, &def, sizeof(ground_sprites_definition), 1, ground_sprite_byte_swapping_data);
	for (int i = 0; i < GROUND_SPRITE_LAYERS_PER_MAP; ++i)
	{
		add_tag_dependency( 'spri', def.layers[i].sprites );
	}
}


