
#include "mons.h"
#include "tagpick.h"
#include <assert.h>
#include <string.h>
#include "skelmodel.h"
#include "formatters.h"


// ==============================  BYTE SWAPPING DATA  ========================================

byte_swap_code monster_attack_sequence_byte_swapping_data[]=
{
	_begin_bs_array, MAXIMUM_NUMBER_OF_ATTACK_SEQUENCES,
		// word flags;
		_2byte,
		// short sequence_index;
		// fixed_fraction skip_fraction;
		_2byte,
		_2byte,
		// short unused;
		sizeof(short),
	_end_bs_array
};

byte_swap_code monster_attack_skelanim_byte_swapping_data[]=
{
	_begin_bs_array, 1,
		1,	// byte state_index
	_end_bs_array
};

byte_swap_code monster_attack_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

		// word flags;
		// fixed_fraction miss_fraction;
		_2byte,
		_2byte,
		
		// file_tag projectile_tag;
		_4byte,
		
		// short_world_distance minimum_range, maximum_range;
		_2byte, _2byte,

		// struct monster_attack_sequence sequences[MAXIMUM_NUMBER_OF_ATTACK_SEQUENCES];
/*		_begin_bs_array, MAXIMUM_NUMBER_OF_ATTACK_SEQUENCES,
			// word flags;
			_2byte,
			// short sequence_index;
			// fixed_fraction skip_fraction;
			_2byte,
			_2byte,
			// short unused;
			sizeof(short),
		_end_bs_array, */
		MAXIMUM_NUMBER_OF_ATTACK_SEQUENCES * sizeof(monster_attack_sequence),

		// short repetitions;
		_2byte,

		// short_world_distance initial_velocity_lower_bound, initial_velocity_delta;
		// short_world_distance initial_velocity_error; // applied randomly in x, y, z
		_2byte, _2byte,
		_2byte,
		
		// short recovery_time;
		// short recovery_time_experience_delta;
		_2byte,		
		_2byte,
		
		// short_world_distance velocity_improvement_with_experience;
		// short_fixed mana_cost;
		_2byte,
		_2byte,

		// word extra_flags;
		_2byte,

		// short projectile_type;
		sizeof(short),
		
	_end_bs_array,
};

static byte_swap_code monster_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	
	// file_tag collection_tag;
	_4byte,

	// short sequence_indexes[MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SEQUENCES];
	_begin_bs_array, MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SEQUENCES, _2byte, _end_bs_array,

	// char terrain_costs[MAXIMUM_NUMBER_OF_TERRAIN_TYPES];
	MAXIMUM_NUMBER_OF_TERRAIN_TYPES,

	// word impassable_terrain_flags;
	// short_world_distance pathfinding_radius;
	_2byte,
	_2byte,
	
	// short_fixed_fraction movement_modifiers[MAXIMUM_NUMBER_OF_TERRAIN_TYPES];
	MAXIMUM_NUMBER_OF_TERRAIN_TYPES,

	// fixed_fraction absorbed_fraction;
	_2byte,

	// short_world_distance warning_distance; // used to awake nearby friends to new target
	// short_world_distance critical_distance; // used for fight vs. flight
	_2byte,
	_2byte,

	// short_fixed healing_fraction;
	_2byte,

	// short initial_ammunition_lower_bound, initial_ammunition_delta;
	_2byte, _2byte,

	// short_world_distance activation_distance; // range of "sight and hearing" for automatic attack vs. retreat
	// short unused;
	_2byte,
	sizeof(short),

	// angle turning_speed; // not a function of terrain
	// short_world_distance base_movement_speed;
	_2byte,
	_2byte,

	// fixed_fraction left_handed_fraction; // the fraction of this type that is left-handed
	// short size;
	_2byte,
	_2byte,

	// file_tag object_tag;
	_4byte,

	// short number_of_attacks;
	// short desired_projectile_volume;
	_2byte,
	_2byte,
	
	// struct monster_attack_definition attacks[MAXIMUM_NUMBER_OF_ATTACKS_PER_MONSTER];
	MAXIMUM_NUMBER_OF_ATTACKS_PER_MONSTER * sizeof(struct monster_attack_definition), // byteswap it later

	// file_tag map_action_tag; // for ambient life
	_4byte,

	// short attack_frequency_lower_bound, attack_frequency_delta;
	_2byte, _2byte,

	// file_tag exploding_projectile_group_tag;
	_4byte,

	// short unused4;
	// short maximum_ammunition_count;
	sizeof(short),
	_2byte,

	// fixed_fraction hard_death_system_shock, flinch_system_shock;
	_2byte, _2byte,

	// file_tag melee_impact_projectile_group_tag;
	// file_tag dying_projectile_group_tag;
	_4byte,
	_4byte,
	
	// file_tag spelling_string_list_tag; // sigular and plural spelling
	// file_tag names_string_list_tag; // common names for this monster
	// file_tag flavor_string_list_tag; // flavor texts for this monster
	_4byte,
	_4byte,
	_4byte,
	
	// short unused3;
	sizeof(short),

	// short monster_class;
	// short monster_allegiance;
	// short experience_point_value;
	_2byte,
	_2byte,
	_2byte,
	
	// file_tag sounds_tags[MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS];
	_begin_bs_array, MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS, _4byte, _end_bs_array,

	// file_tag blocked_impact_projectile_group_tag;
	// file_tag absorbed_impact_projectile_group_tag;
	_4byte,
	_4byte,

	// file_tag ammunition_projectile_tag;
	_4byte,

	// short visiblity_type;
	_2byte,

	// short combined_power;
	// short_world_distance longest_range;
	_2byte,
	_2byte,
	
	// short cost;
	_2byte,

	// char sound_types[MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS];
	MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS,

	// word pad3
	sizeof(word),
	
	// file_tag entrance_projectile_group_tag;
	// file_tag local_projectile_group_tag;
	// file_tag special_ability_string_list_tag;
	// file_tag exit_projectile_group_tag;
	_4byte,
	_4byte,
	_4byte,
	_4byte,

	// short_fixed maximum_mana;
	// short_fixed mana_recharge_rate;
	_2byte,
	_2byte,
	
	// fixed_fraction berserk_system_shock;
	// fixed_fraction berserk_vitality;
	_2byte,
	_2byte,

	// file_tag portrait_collection_tag;
	_4byte,
	
	// short kills_for_maximum_experience_bonus;
	_2byte,
	
	// short experience_bonus_size;
	// short_world_distance experience_bonus_radius;
	_2byte,
	_2byte,
	
	// fixed_fraction blocking_fraction
	_2byte,
	
	// carrying_tag
	_4byte,
	
	// short unused2[216];
	207*sizeof(short),

	// computed during postprocessing
	
	// short portrait_collection_index;
	// short next_name_string_index;
	// short ammunition_projectile_type;
	3*sizeof(short),

	// short sound_indexes[MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS];
	MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS*sizeof(short),
	
	// short spelling_string_list_index; // sigular and plural spelling
	// short names_string_list_index; // common names for this monster
	// short flavor_string_list_index; // flavor texts for this monster
	// short special_ability_string_list_index;
	// short exploding_projectile_group_type;
	// short melee_impact_projectile_group_type;
	// short dying_projectile_group_type;
	// short blocked_impact_projectile_group_type;
	// short absorbed_impact_projectile_group_type;
	// short entrance_projectile_group_type, exit_projectile_group_type;
	// short collection_index; // from collection_tag
	// short object_type; // from object_tag
	// short local_projectile_group_type;
	14*sizeof(short),
	
	_end_bs_array
};

monster_definition default_mons =
{
	0,
	static_cast<file_tag>(-1),
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0,
	0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0,
	0,
	0,
	0,
	0, 0,
	0,
	0,
	0,
	0,
	0,
	0,
	-1,
	0,
	0,
	// monster attack definition
	0, 0, -1, 0, 0, /*att sequence*/0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /*end*/ 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, static_cast<file_tag>(-1), 0, 0, /*att sequence*/0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /*end*/ 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, static_cast<fixed_fraction>(-1), 0, 0, /*att sequence*/0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /*end*/ 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, static_cast<word>(-1), 0, 0, /*att sequence*/0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /*end*/ 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// end monster attack definition
	-1,
	0, 0,
	static_cast<file_tag>(-1),
	0,
	0,
	0, 0,
	-1,
	static_cast<fixed_fraction>(-1),
	static_cast<fixed_fraction>(-1),
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	0,
	0,
	0,
	0,
	-1, -1, -1, static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	0,
	0,
	0,
	0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0,
	-1,
	-1,
	static_cast<word>(-1),
	static_cast<file_tag>(-1),
	0,
	0,
	0,
	0,
	-1,
	5,
	0,
	0,
	static_cast<short>(32768),
	0
};

// ==============================  EDIT WINDOW  ========================================
void edit_mons( tag_entry* e )
{

}

void defaultnew_mons( tag_entry *e )
{
	monster_definition def;
	byte_swap_move_be( "mons", &def, &default_mons, sizeof(monster_definition), 1, monster_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_mons( tag_entry *e )
{
	monster_definition def;
	read_monster_definition( &def, e );
	bool skel = def.flags & _monster_has_3d_model_flag;
	if (skel)
	{
		add_tag_dependency( 'skmd', def.skelmodel_tag );
	}
	else
	{
		add_tag_dependency( '.256', def.collection_tag );
	}
	add_tag_dependency( 'obje', def.object_tag );
	for( int i = 0; i < def.number_of_attacks; i++ )
	{
		add_tag_dependency( 'proj', def.attacks[i].projectile_tag );
	}
	
	// FIXME: add a dependency for the map action tag?
	add_tag_dependency( 'prgr', def.exploding_projectile_group_tag );
	add_tag_dependency( 'prgr', def.melee_impact_projectile_group_tag );
	add_tag_dependency( 'prgr', def.dying_projectile_group_tag );
	add_tag_dependency( 'stli', def.spelling_string_list_tag );
	add_tag_dependency( 'stli', def.names_string_list_tag );
	add_tag_dependency( 'stli', def.flavor_string_list_tag );
	for (int i = 0; i < MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS; ++i)
	{
		add_tag_dependency( 'soun', def.sound_tags[i] );
	}
	add_tag_dependency( 'prgr', def.blocked_impact_projectile_group_tag );
	add_tag_dependency( 'prgr', def.absorbed_impact_projectile_group_tag );
	add_tag_dependency( 'proj', def.ammunition_projectile_tag );
	add_tag_dependency( 'prgr', def.entrance_projectile_group_tag );
	add_tag_dependency( 'lpgr', def.local_projectile_group_tag );
	add_tag_dependency( 'stli', def.special_ability_string_list_tag );
	add_tag_dependency( 'prgr', def.exit_projectile_group_tag );
	add_tag_dependency( '.256', def.portrait_collection_tag );
}

// ==============================  READ TAG DATA  ========================================
bool read_monster_definition( monster_definition* def, tag_entry* e )
{
	if (load_tag_data_into_struct( "mons", e, def, SIZEOF_STRUCT_MONSTER_DEFINITION, 1, monster_definition_byte_swapping_data)) {
		for (int i = 0; i < MAXIMUM_NUMBER_OF_ATTACKS_PER_MONSTER; ++i) {
			byte_swap_data_be( "monster attack", def->attacks + i, sizeof(monster_attack_definition), 1,
								monster_attack_definition_byte_swapping_data );
			if (def->flags & _monster_has_3d_model_flag) {
				byte_swap_data_be( "monster attack skelanim", &def->attacks[i].skelanim,
									sizeof(def->attacks[i].skelanim), 1,
									monster_attack_skelanim_byte_swapping_data );
			} else {
				byte_swap_data_be( "monster attack sequences", &def->attacks[i].sequences,
									sizeof(def->attacks[i].sequences), 1,
									monster_attack_sequence_byte_swapping_data );
			}
		}
		return TRUE;
	}
	
	return FALSE;
}


// ==============================  SAVE TAG DATA  ========================================
bool save_monster_definition( monster_definition* def, tag_entry* e )
{
	
	monster_definition *buf = (monster_definition *)malloc( sizeof(monster_definition) );
	byte_swap_move_be( "mons", buf, def, sizeof(monster_definition), 1, monster_definition_byte_swapping_data );
	for (int i = 0; i < MAXIMUM_NUMBER_OF_ATTACKS_PER_MONSTER; ++i) {
		byte_swap_data_be( "monster attack", buf->attacks + i, sizeof(monster_attack_definition), 1,
							monster_attack_definition_byte_swapping_data );
		if (def->flags & _monster_has_3d_model_flag) {
			byte_swap_data_be( "monster attack skelanim", &buf->attacks[i].skelanim,
								sizeof(buf->attacks[i].skelanim), 1,
								monster_attack_skelanim_byte_swapping_data );
		} else {
			byte_swap_data_be( "monster attack sequences", &buf->attacks[i].sequences,
								sizeof(buf->attacks[i].sequences), 1,
								monster_attack_sequence_byte_swapping_data );
		}
	}
	set_entry_data( e, buf, sizeof(monster_definition), TRUE );
	return true;
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_monster_definition( monster_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'mons', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_monster_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_monster_definition( monster_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'mons', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_monster_definition( def, e );
}


const char *AnimStateNames[] = {
	"Idle",
	"Alert",
	"Gesture",
	"Move",
	"Run",
	"Turn Left",
	"Turn Right",
	"Pause for Obstacle",
	"Hold for Attack",
	"Block Attack",
	"Hit Light from Front",
	"Hit Light from Back",
	"Hit Hard from Front",
	"Hit Hard from Back",
	"Die Soft from Front",
	"Die Soft from Back",
	"Die Hard from Front",
	"Die Hard from Back",
	"Explosion Death",
	"Pick Up Object",
	"Set Down Object",
	"Taunt",
	"Celebrate",
	"Receive Healing",
	"Enter Map",
	"Exit Map",
	"Auxiliary 1",
	"Auxiliary 2",
	0
};

const char *SequenceNames[] = {
	"Idle",
	"Moving",
	"Placeholder",
	"Pausing for Obstacle",
	"Turning",
	"Blocking",
	"Taking Damage",
	"PIcking up Object",
	"HEad Shot",
	"Celebration",
	"Transition to Secondary Idle",
	"Secondary Idle",
	"Transition to Primary Idle",
	"Running",
	"Ammo Icon",
	"Hold for Attack",
	"Taunt",
	"Glide",
	0
};

struct MonsterSequenceData {
	short sequences[32];
	//XGStringList mons;
	//XGStringList anim;
};

const char *monster_sound_type_desc[] =
{
	"None",
	"Attack Order",
	"Multiple Attack Order",
	"Attack Order vs. Undead",
	"Move Order",
	"Multiple Move Order",
	"Selection",
	"Multiple Selection",
	"Hit Friendly Unit",
	"Hit By Friendly Unit",
	"Attack Obstructed By Friendly Unit",
	"Attack Enemy Unit",
	"Attack Enemy Unit With Friends Nearby",
	"Sprayed By Gore",
	"Caused Enemy Death",
	"Caused Friendly Death",
	"Caused Death of Enemy Undead",
	"Caused Multiple Enemy Deaths",
	""
};

struct MonsterSoundsTable
{
	tag_file *source;
	char sound_type[MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS];
	file_tag sound_tag[MAXIMUM_NUMBER_OF_MONSTER_DEFINITION_SOUNDS];
};

struct MonsterTerrainData
{
	short_world_distance base_movement_speed;
	short_fixed_fraction movement_modifiers[MAXIMUM_NUMBER_OF_TERRAIN_TYPES];
	char terrain_costs[MAXIMUM_NUMBER_OF_TERRAIN_TYPES];
};

static void ExtractCollectionNames( tag_entry *e, XGStringList *out )
{
	e = 0;
	char buf[40];
	for (int i = 0; i < 20; ++i) {
		sprintf( buf, "Andrew is lazy %d", i );
		//out->Insert( i, buf );
	}
}

#pragma mark -

struct attack_dialog_data {
	tag_file *source;
	const monster_definition *def;
	monster_attack_definition a;
};

