// spri.cpp
#include "spri.h"

 

 
#include "tagpick.h"
#include <assert.h>
#include <string.h>

 
 
 
#include "texturestack.h"
#include "jpeg.h"
#include "formatters.h"

static byte_swap_code _4byte_bs[] = 
{
	_begin_bs_array, 1, 
	_4byte,
	_end_bs_array
};

static byte_swap_code sprite_frame_definition_bs[] =
{
	_begin_bs_array, 1,
	
	// float xy[4][2]
	_begin_bs_array, 4,
		_begin_bs_array, 2,
			_4byte,
		_end_bs_array,
	_end_bs_array,
		
	// float st[4][2]
	_begin_bs_array, 4,
		_begin_bs_array, 2,
			_4byte,
		_end_bs_array,
	_end_bs_array,
		
	// float slot_st[2][2]
	_begin_bs_array, 2,
		_begin_bs_array, 2,
			_4byte,
		_end_bs_array,
	_end_bs_array,
	
	// float slot_xy[2]
	_begin_bs_array, 2,
		_4byte,
	_end_bs_array,
	
	// float slot_wh[2]
	_begin_bs_array, 2, 
		_4byte,
	_end_bs_array,
	
	_4byte,
	_4byte,
	_4byte,
	
	sizeof(void*),
	sizeof(int)*2,
	sizeof(short)*4,
	
	_end_bs_array
};

static byte_swap_code sprite_sequence_definition_bs[] = 
{
	_begin_bs_array, 1, 
	
	// int num_frames
	_4byte,
	// int probability
	_4byte,
	// int time
	_4byte,
	// int flags,
	_4byte, 
	
	sizeof(int*),
	sizeof(int*),
	sizeof(float*),
	sizeof(float*),
	sizeof(tag*),
	sizeof(float*),
	
	_end_bs_array
	
	// note there is stuff at the end but it is variable, so we
	// have to swap load it in seperately.
};

static byte_swap_code sprite_type_definition_bs[] = 
{
	_begin_bs_array, 1,
	
	// int num_frames
	_4byte,
	// int num_sequences
	_4byte,
	// int total_sequence_probability
	_4byte,
	// int flags
	_4byte,
	
	sizeof(tag),
	sizeof( struct sprite_frame_definition*),
	sizeof( struct sprite_sequence_definition*),
	
	_end_bs_array
	
	// note there is stuff at the end but it is variable, so we
	// have to swap load it in seperately.
};

void load_frame_texture( sprite_frame_definition* frame );
void load_frame_texture( sprite_frame_definition* frame )
{
	stack_set* ss;
	tag_entry* tx_entry;
	stacked_texture* tex;
	
	if( frame->data )
		free( frame->data );
	frame->data = 0;
	
	tx_entry = get_entry( 'txst', frame->texture_stack_tag );
	if( !tx_entry )
		return;
	ss = load_stackset( tx_entry );
	if( !ss )
		return;
	tex = get_texture( ss, frame->stack_num, frame->tex_num );
	if( !tex )
		return;
	
	frame->width = tex->width;
	frame->height = tex->height;
	frame->channels = tex->channels;
	frame->data = malloc(tex->width*tex->height*tex->channels);
	assert( frame->data );
	
	if( tex->format == 1 ) // jpg.
    {
        decompress_jpeg( frame->data, tex->compressed_data, tex->compressed_length, tex->width, tex->height, tex->channels, TRUE ); 
    }
        else
		memcpy( frame->data, tex->compressed_data, tex->compressed_length );
		
	dispose_stackset( ss );
}

void defaultnew_spri( tag_entry* e )
{
	sprite_type_definition def;
	
	def.num_frames = 0;
	def.num_sequences = 0;
	def.total_sequence_probability = 0;
	def.id = 0;
	def.flags = 0;
	def.frames = 0;
	def.sequences = 0;
	
	save_sprite_type_definition( &def, e );
}

void edit_spri( tag_entry* e )
{

}


void dependency_spri( tag_entry *e )
{
	sprite_type_definition *def;
	def = alloc_sprite_type_definition( MAX_SPRITE_FRAMES, MAX_SPRITE_SEQUENCES, MAX_FRAMES_PER_SEQUENCE );
	
	read_sprite_type_definition( def, e, false );
	int i, j;
	for (i = 0; i < def->num_frames; ++i)
	{
		add_tag_dependency( 'txst', def->frames[i].texture_stack_tag );
	}
	for (i = 0; i < def->num_sequences; ++i)
	{
		for (j = 0; j < def->sequences[i].num_frames; ++j)
		{
			add_tag_dependency( 'soun', def->sequences[i].sound_tags[j] );
		}
	}
	
	free_sprite_type_definition( def );
}


bool read_sprite_type_definition( sprite_type_definition* def, tag id, bool load_textures )
{
	tag_entry* e;
	
	assert( def );
	e = get_entry( 'spri', id );
	if( 0 == e )
		return FALSE;
	
	return read_sprite_type_definition( def, e, load_textures );
}

bool save_sprite_type_definition( sprite_type_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	e = get_entry( 'spri', id );
	if( 0 == e )
		return FALSE;
		
	return save_sprite_type_definition( def, e );
}

bool read_sprite_type_definition( sprite_type_definition* def, tag_entry* e, bool load_textures )
{
	int i;
	unsigned int length;
	const unsigned char* data;
	sprite_type_definition temp;
	sprite_sequence_definition temp_seq;
	
	if( !get_entry_data( e, (const void**)&data, &length ) )
		return FALSE;
	
	free_sprite_type_definition_frame_textures( def );
	
	temp = *def; // save off ptr's.
		
	byte_swap_move_be( "std", def, data, sizeof( sprite_type_definition ), 1, sprite_type_definition_bs );
	data += sizeof(sprite_type_definition);
	assert( def->num_frames <= MAX_SPRITE_FRAMES );
	assert( def->num_sequences <= MAX_SPRITE_SEQUENCES );
	
	// load the frames up.
	def->frames = temp.frames;
	def->sequences = temp.sequences;
	
	if(def->num_frames > 0)
	{
		byte_swap_move_be( "sfd", def->frames, data, sizeof( sprite_frame_definition ), def->num_frames, sprite_frame_definition_bs );
		data += (sizeof(sprite_frame_definition)*def->num_frames);
		for(i = 0; i < def->num_frames; i++)
			def->frames[i].data = 0;
	}
	
	// load the sequences up.
	for(i = 0; i < def->num_sequences; i++)
	{
		temp_seq = def->sequences[i];
		byte_swap_move_be( "ssd", &def->sequences[i], data, sizeof( sprite_sequence_definition ), 1, sprite_sequence_definition_bs );
		data += sizeof(sprite_sequence_definition);
		// restore ptrs to memory.
		def->sequences[i].frames = temp_seq.frames;
		def->sequences[i].ticks  = temp_seq.ticks;
		def->sequences[i].colors = temp_seq.colors;
		def->sequences[i].rots   = temp_seq.rots;
		def->sequences[i].sound_tags = temp_seq.sound_tags;
		def->sequences[i].scales = temp_seq.scales;
	}
	
	for(i = 0; i < def->num_sequences; i++)
	{
		byte_swap_move_be( "ssf", def->sequences[i].frames, data, sizeof(int), def->sequences[i].num_frames, _4byte_bs );
		data += sizeof(int)*def->sequences[i].num_frames;
		byte_swap_move_be( "sst", def->sequences[i].ticks, data, sizeof(int), def->sequences[i].num_frames, _4byte_bs );
		data += sizeof(int)*def->sequences[i].num_frames;
		byte_swap_move_be( "ssc", def->sequences[i].colors, data, sizeof(float), def->sequences[i].num_frames*4, _4byte_bs );
		data += sizeof(float)*def->sequences[i].num_frames*4;
		byte_swap_move_be( "ssr", def->sequences[i].rots, data, sizeof(float), def->sequences[i].num_frames, _4byte_bs );
		data += sizeof(float)*def->sequences[i].num_frames;
		byte_swap_move_be( "ssf", def->sequences[i].sound_tags, data, sizeof(int), def->sequences[i].num_frames, _4byte_bs );
		data += sizeof(tag)*def->sequences[i].num_frames;
		byte_swap_move_be( "sss", def->sequences[i].scales, data, sizeof(float), def->sequences[i].num_frames, _4byte_bs );
		data += sizeof(float)*def->sequences[i].num_frames;
	}
	
	if (load_textures)
	{
		load_sprite_type_definition_frame_textures( def );
	}
	
	return TRUE;
}

bool save_sprite_type_definition( sprite_type_definition* def, tag_entry* e )
{	
	/*
	i just want you to know this is going to be a pain in the ass.
	*/
	int i, size;
	unsigned char* data, *base;
	
	// compute the block size we need.
	size = sizeof(sprite_type_definition);
	size += sizeof(sprite_frame_definition)*def->num_frames;
	size += sizeof(sprite_sequence_definition)*def->num_sequences;
	
	for(i = 0; i < def->num_sequences; i++)
		size += (sizeof(float)+sizeof(int)+sizeof(int)+sizeof(tag)+sizeof(float)+(sizeof(float)*4))*def->sequences[i].num_frames;	
	
	assert( size > 0 );
	base = data = (unsigned char*)malloc(size);
	
	byte_swap_move_be( "std", data, def, sizeof( sprite_type_definition ), 1, sprite_type_definition_bs );
	data += sizeof(sprite_type_definition);
	
	if( def->num_frames > 0 )
	{
		byte_swap_move_be( "sfd", data, def->frames, sizeof( sprite_frame_definition ), def->num_frames, sprite_frame_definition_bs );
		data += sizeof(sprite_frame_definition)*def->num_frames;
	}
	
	if( def->num_sequences > 0 )
	{
		byte_swap_move_be( "ssd", data, def->sequences, sizeof( sprite_sequence_definition ), def->num_sequences, sprite_sequence_definition_bs );
		data += sizeof(sprite_sequence_definition)*def->num_sequences;
		
		for(i = 0; i < def->num_sequences; i++)
		{
			if( def->sequences[i].num_frames > 0 )
			{
				byte_swap_move_be( "ssf", data, def->sequences[i].frames, sizeof(int), def->sequences[i].num_frames, _4byte_bs );
				data += sizeof(int)*def->sequences[i].num_frames;
				byte_swap_move_be( "sst", data, def->sequences[i].ticks, sizeof(int), def->sequences[i].num_frames, _4byte_bs );
				data += sizeof(int)*def->sequences[i].num_frames;
				byte_swap_move_be( "ssc", data, def->sequences[i].colors, sizeof(float), def->sequences[i].num_frames*4, _4byte_bs );
				data += sizeof(float)*def->sequences[i].num_frames*4;
				byte_swap_move_be( "ssr", data, def->sequences[i].rots, sizeof(float), def->sequences[i].num_frames, _4byte_bs );
				data += sizeof(float)*def->sequences[i].num_frames;
				byte_swap_move_be( "ssf", data, def->sequences[i].sound_tags, sizeof(tag), def->sequences[i].num_frames, _4byte_bs );
				data += sizeof(tag)*def->sequences[i].num_frames;
				byte_swap_move_be( "sss", data, def->sequences[i].scales, sizeof(float), def->sequences[i].num_frames, _4byte_bs );
				data += sizeof(float)*def->sequences[i].num_frames;
			}
		}	
	}
	
	set_entry_data( e, base, size, TRUE );
	
	return TRUE;
}

sprite_type_definition* alloc_sprite_type_definition( int num_frames, int num_sequences, int max_frames_per_sequence )
{
	int i;
	sprite_type_definition* def;
	
	def = (sprite_type_definition*)malloc(sizeof(sprite_type_definition));
	assert( def );
	
	def->num_frames = num_frames;
	def->num_sequences = num_sequences;
	def->total_sequence_probability = 0;
	def->flags = 0;
	def->id = 0;
	def->frames = 0;
	def->sequences = 0;
	
	if( num_frames > 0 )
	{
		def->frames = (sprite_frame_definition*)malloc(sizeof(sprite_frame_definition)*num_frames);
		assert( def->frames );
		for(i = 0; i < num_frames; i++)
			def->frames[i].data = 0;
	}
	
	if( num_sequences > 0 )
	{
		def->sequences = (sprite_sequence_definition*)malloc(sizeof(sprite_sequence_definition)*num_sequences);
		assert( def->sequences );
		for(i = 0; i < num_sequences; i++)
		{
			def->sequences[i].num_frames = 0;
			def->sequences[i].probability = 0;
			def->sequences[i].time = 0;
			def->sequences[i].flags = 0;
			
			if( max_frames_per_sequence > 0 )
			{
				def->sequences[i].frames = (int*)malloc(sizeof(int)*max_frames_per_sequence);
				assert( def->sequences[i].frames );
				def->sequences[i].ticks  = (int*)malloc(sizeof(int)*max_frames_per_sequence);
				assert( def->sequences[i].ticks );
				def->sequences[i].colors = (float*)malloc(sizeof(float)*max_frames_per_sequence*4);
				assert( def->sequences[i].colors );
				def->sequences[i].rots = (float*)malloc(sizeof(float)*max_frames_per_sequence);
				assert( def->sequences[i].rots );
				def->sequences[i].sound_tags = (tag*)malloc(sizeof(tag)*max_frames_per_sequence);
				assert( def->sequences[i].sound_tags );
				def->sequences[i].scales = (float*)malloc(sizeof(float)*max_frames_per_sequence);
				assert( def->sequences[i].scales );
			}
		}
	}
	
	
	return def;
}

bool sprite_type_definitions_differ( sprite_type_definition* def_a, sprite_type_definition* def_b )
{
	int i;
	sprite_sequence_definition* seq_a, *seq_b;
	
	if( def_a->num_frames != def_b->num_frames ||
		def_a->num_sequences != def_b->num_sequences ||
		def_a->flags != def_b->flags )
			return TRUE;
			
	for(i = 0; i < def_a->num_frames; i++)
	{
		if( memcmp( &def_a->frames[i], &def_b->frames[i], 108 ) ) // hardcoded value of things that matter.
			return TRUE;
	}
	
	for(i = 0; i < def_a->num_sequences; i++)
	{
		seq_a = &def_a->sequences[i];
		seq_b = &def_b->sequences[i];
		
		if( seq_a->num_frames != seq_b->num_frames ||
			seq_a->probability != seq_b->probability ||
			seq_a->time != seq_b->time ||
			seq_a->flags != seq_b->flags )
				return TRUE;
				
		if( memcmp( seq_a->frames, seq_b->frames, sizeof(int)*seq_a->num_frames ) )
			return TRUE;
		if( memcmp( seq_a->ticks, seq_b->ticks, sizeof(int)*seq_a->num_frames ) )
			return TRUE;
		if( memcmp( seq_a->colors, seq_b->colors, sizeof(float)*seq_a->num_frames*4 ) )
			return TRUE;
		if( memcmp( seq_a->rots, seq_b->rots, sizeof(float)*seq_a->num_frames ) ) 
			return TRUE;
		if( memcmp( seq_a->sound_tags, seq_b->sound_tags, sizeof(tag)*seq_a->num_frames ) )
			return TRUE;
		if( memcmp( seq_a->scales, seq_b->scales, sizeof(float)*seq_a->num_frames ) )
			return TRUE;
	}
	
	return FALSE;
}

void free_sprite_type_definition( sprite_type_definition* def )
{
	assert( def );
	
	if( def->frames )
	{
		int i;
		for(i = 0; i < def->num_frames; i++)
		{
			if( def->frames[i].data )
				free( def->frames[i].data );
		}
		free( def->frames );
	}
	
	if( def->sequences )
	{	
		int i;
		for(i = 0; i < def->num_sequences; i++)
		{
			if( def->sequences[i].frames )
				free( def->sequences[i].frames );
			if( def->sequences[i].ticks )
				free( def->sequences[i].ticks );
			if( def->sequences[i].colors )
				free( def->sequences[i].colors );
			if( def->sequences[i].rots )
				free( def->sequences[i].rots );
			if( def->sequences[i].sound_tags )
				free( def->sequences[i].sound_tags );
			if( def->sequences[i].scales )
				free( def->sequences[i].scales );
		}

		free( def->sequences);
	}
	
	free( def );
	
}

void load_sprite_type_definition_frame_textures( sprite_type_definition* def )
{
	int i;
		
	for(i = 0; i < def->num_frames; i++)
	{
		load_frame_texture( &def->frames[i] );
	}
}

void free_sprite_type_definition_frame_textures( sprite_type_definition* def )
{
	int i;
	
	assert( def );
	for(i = 0; i < def->num_frames; i++)
	{
		if( def->frames[i].data )
			free( def->frames[i].data );
		def->frames[i].data = 0;
	}
}


