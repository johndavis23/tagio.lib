
#include "proj.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 
 
#include "skelmodel.h"



// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code projectile_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	
	// file_tag collection_tag;
	_4byte,

	// short sequence_indexes[MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SEQUENCES];
	
		_begin_bs_array, MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SEQUENCES,
		
		_2byte,
		
		_end_bs_array,
	
	// short splash_or_rebound_effect_type;
	// file_tag detonation_projectile_group_tag;
	_2byte,
	_4byte,
	
	// file_tag contrail_projectile_tag;
	// short ticks_between_contrails, maximum_contrail_count;
	_4byte,
	_2byte, _2byte,
	
	// file_tag object_tag;
	_4byte,

	// short_fixed inertia_lower_bound, inertia_delta; // 1.0 is standard, 0.0 is infinite (how much acceleration we accept)
	// short_world_distance random_initial_velocity; // applied in a random direction
	// short volume;
	// short tracking_priority;
	// fixed_fraction promotion_on_detonation_fraction; // chance this projectile will be promoted instead of detonating
	_2byte, _2byte,
	_2byte,
	_2byte,
	_2byte,
	_2byte,
	
	// fixed_fraction detonation_frequency, media_detonation_frequency;
	// short_world_distance detonation_velocity; // can be zero (will take highest of local and external)
	_2byte, _2byte,
	_2byte,
	
	// short projectile_class;
	_2byte,
	
	// file_tag lightning_tag;
	_4byte,
	
	// short unused[2];
	2*sizeof(short),
	// file_tag sound_tags[MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SOUNDS];
	_begin_bs_array, MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SOUNDS, _4byte, _end_bs_array,
	
	// short_fixed animation_rate_lower_bound, animation_rate_delta;
	// short lifespan_lower_bound, lifespan_delta; // in ticks, zero is maximum
	_2byte, _2byte,
	_2byte, _2byte,
	
	// fixed_fraction initial_contrail_frequency, final_contrail_frequency;
	// short contrail_frequency_delta;
	// angle guided_turning_speed;
	_2byte, _2byte,
	_2byte,
	_2byte,
	
	// file_tag promoted_projectile_tag; // the tag we become if we are promoted
	// file_tag promotion_projectile_group_tag; // the projectile group created if we are promoted
	// file_tag mesh_stain_texturestacks_tag;
	_4byte,
	_4byte,
	_4byte,
	
	// struct damage_definition damage;

		// short type;
		// word flags;
		_2byte,
		_2byte,
		
		// short damage_lower_bound, damage_delta;
		_2byte, _2byte,

		// short_world_distance radius_lower_bound, radius_delta;
		// short_world_distance rate_of_expansion; // world units per tick
		_2byte, _2byte,
		_2byte,

		// short_fixed damage_to_velocity;
		_2byte,
	
	// file_tag artifact_tag;
	// file_tag target_detonation_projectile_group_tag;
	// file_tag geometry_tag;
	_4byte,
	_4byte,
	_4byte,
		
	// short delay_lower_bound, delay_delta;
	_2byte,
	_2byte,

	// file_tag local_projectile_group_tag;
	_4byte,
	
	// short nearby_target_radius; // in cells; only valid if bit set
	// word pad2;
	_2byte,
	sizeof(short),
	
	// short skel_model_code;
	_2byte,
	
	// blood_rgba
	4 * sizeof(byte),
	
	// short unused3[40];
	35*sizeof(short),
	
	// computed during postprocessing
	
	// short mesh_stain_txst_index;
	// short local_projectile_group_type;
	// short geometry_index;
	// short target_detonation_projectile_group_type;
	// short lightning_index, artifact_index;
	// short detonation_projectile_group_type, contrail_projectile_type;
	// short promotion_projectile_group_type, promoted_projectile_type;
	10*sizeof(short),
	
	// short sound_indexes[MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SOUNDS];
	// short collection_index, color_table_index;
	// short object_type;
	8*sizeof(short),
	
	_end_bs_array
};

const char *DamageTypeNames[] = {
	"NONE",
	"Explosion",
	"Magical",
	"Holding (i.e. shocking)",
	"Healing",
	"Kinetic",
	"Slashing Metal",
	"Stoning",
	"Slashing Claws",
	"Explosive Electricity",
	"Fire",
	"Gas",
	"Charm",
	"Tain",
	0
};

// ==============================  EDIT WINDOW  ========================================
void edit_proj( tag_entry* e )
{
}


void dependency_proj( tag_entry *e )
{
	projectile_definition def;
	read_projectile_definition( &def, e );
	
	tag artType = (def.skelmodel_code == 0xFEDC) ? 'skmd' : '.256';
	tag contrailType = (def.flags & _projectile_uses_sprite_contrail_flag) ? 'spri' : 'proj';
	add_tag_dependency( artType, def.collection_tag );
	add_tag_dependency( 'prgr', def.detonation_projectile_group_tag );
	add_tag_dependency( contrailType, def.contrail_projectile_or_sprite_tag );
	add_tag_dependency( 'obje', def.object_tag );
	add_tag_dependency( 'ligh', def.lightning_tag );
	for (int i = 0; i < MAXIMUM_NUMBER_OF_PROJECTILE_DEFINITION_SOUNDS; ++i)
	{
		add_tag_dependency( 'soun', def.sound_tags[i] );
	}
	add_tag_dependency( 'proj', def.promoted_projectile_tag );
	add_tag_dependency( 'arti', def.artifact_tag );
	add_tag_dependency( 'prgr', def.promotion_projectile_group_tag );
	add_tag_dependency( 'prgr', def.target_detonation_projectile_group_tag );
	add_tag_dependency( 'lpgr', def.local_projectile_group_tag );
	add_tag_dependency( 'txst', def.bounce_stain_texturestacks_tag );
	add_tag_dependency( 'txst', def.detonation_stain_texturestacks_tag );
}

void update_proj( tag_entry *e )
{	
	for (;;)
	{
		int version = get_entry_version( e );
		switch (version)
		{
			case 1: {
					projectile_definition def;
					read_projectile_definition( &def, e );
					def.bounce_stain_texturestacks_tag = -1;
					def.detonation_stain_texturestacks_tag = -1;
					save_projectile_definition( &def, e );
					set_entry_version( e, 2 ); // no changes!
				} break;
				
			case 2:
				return;
			default:
				assert(0);
				return;
		}
	}
}


// ==============================  READ TAG DATA  ========================================
bool read_projectile_definition( projectile_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "proj", e, def, SIZEOF_STRUCT_PROJECTILE_DEFINITION, 1, projectile_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_projectile_definition( projectile_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "proj", e, def, SIZEOF_STRUCT_PROJECTILE_DEFINITION, 1, projectile_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_projectile_definition( projectile_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'proj', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_projectile_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_projectile_definition( projectile_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'proj', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_projectile_definition( def, e );
}



