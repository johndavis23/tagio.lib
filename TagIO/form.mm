
#include "form.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code formation_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// SHORT LINE
	// short maximum_units_per_short_line_row;
	// short_world_distance short_line_unit_separation;
	_2byte,
	_2byte,

	// LONG LINE
	// short maximum_units_per_long_line_row;
	// short_world_distance long_line_unit_separation;
	_2byte,
	_2byte,

	// LOOSE LINE
	// short maximum_units_per_loose_line_row;
	// short_world_distance loose_line_unit_separation;
	_2byte,
	_2byte,

	// STAGGERED LINE
	// short maximum_units_per_staggered_line_row;
	// short_world_distance staggered_line_unit_separation;
	_2byte,
	_2byte,

	// BOX
	// short_world_distance box_unit_separation;
	_2byte,

	// MOB
	// short_world_distance minimum_mob_unit_separation;
	// short_fixed mob_x_bias;
	_2byte,
	_2byte,

	// short_world_distance minimum_encirclement_radius;
	// short_world_distance encirclement_unit_separation;
	_2byte,
	_2byte,

	// SHALLOW ENCIRCLEMENT
	// angle shallow_encirclement_arc_length;
	_2byte,

	// DEEP ENCIRCLEMENT
	// angle deep_encirclement_arc_length;
	_2byte,

	// VANGUARD
	// short_world_distance vanguard_front_row_separation;
	// short_world_distance vanguard_unit_separation;
	// short_world_distance vanguard_row_separation;
	_2byte,
	_2byte,
	_2byte,
	
	// CIRCLE
	// short_world_distance circle_unit_separation;
	_2byte,
	
	// short unused;
	sizeof(short),
	
	_end_bs_array
};

formation_definition default_form =
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0
};

// ==============================  EDIT WINDOW  ========================================
void edit_form( tag_entry* e )
{
}

void defaultnew_form( tag_entry *e )
{
	formation_definition def;
	
	byte_swap_move_be( "form", &def, &default_form, sizeof(formation_definition), 1, formation_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_form( tag_entry *e )
{
    #pragma unused( e )

#if 0 // no dependencies
	formation_definition def;
	read_formation_definition( &def, e );
#endif
}



// ==============================  READ TAG DATA  ========================================
bool read_formation_definition( formation_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "form", e, def, SIZEOF_STRUCT_FORMATION_DEFINITION, 1, formation_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_formation_definition( formation_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "form", e, def, SIZEOF_STRUCT_FORMATION_DEFINITION, 1, formation_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_formation_definition( formation_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'form', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_formation_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_formation_definition( formation_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'form', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_formation_definition( def, e );
}


