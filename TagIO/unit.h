// unit.h
#ifndef __unit_h__
#define __unit_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	UNIT_DEFINITION_GROUP_TAG= 0x756e6974, // 'unit' (looks like text)
	UNIT_DEFINITION_VERSION_NUMBER= 2,
	
	MAXIMUM_UNIT_DEFINITIONS_PER_MAP= 512,
	MAXIMUM_CUSTOMIZATIONS_PER_UNIT= 4
};

/* ---------- unit definitions */

#define SIZEOF_STRUCT_UNIT_CUSTOMIZATION_SETTINGS 24
struct unit_customization_settings
{
	file_tag logo_txst_tag;
	int logo_table_index;
	struct rgb_color primary_color;
	struct rgb_color secondary_color;
};


#define SIZEOF_STRUCT_UNIT_DEFINITION 128
struct unit_definition
{
	file_tag monster_tag;
	int number_of_customizations;
	
	struct unit_customization_settings custom_colors[MAXIMUM_CUSTOMIZATIONS_PER_UNIT];

	short unused[7];
	
	short player_index; // taken from the palette entry for this unit after players have been assigned to teams
	file_tag parent_unit_tag; // only interesting if this is a custom unit definition

	// computed when postprocessing this definition

	short monster_type;
	short unused2;
};



//=====================================================================================================
bool read_unit_definition( unit_definition* def, tag id );
bool save_unit_definition( unit_definition* def, tag id );

bool read_unit_definition( unit_definition* def, tag_entry* e );
bool save_unit_definition( unit_definition* def, tag_entry* e );


#endif