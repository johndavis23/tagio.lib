// anim.h
#ifndef __anim_h__
#define __anim_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	MODEL_ANIMATION_GROUP_TAG= 0x616E696D,	// 'anim'
	MODEL_ANIMATION_VERSION_NUMBER= 1,
	
	MAXIMUM_MODEL_ANIMATION_DEFINITIONS_PER_MAP= 8,
	MAXIMUM_FRAMES_PER_ANIMATION= 31
};

/* ---------- flags */

enum {
	NUMBER_OF_MODEL_ANIMATION_FRAME_FLAGS
};

enum {
	_random_initial_frame_bit,
	_animation_cycles_bit,
	_animation_back_and_forth_bit,
	_animation_initially_running_bit,
	NUMBER_OF_MODEL_ANIMATION_DEFINITION_FLAGS,

	_random_initial_frame_flag= FLAG(_random_initial_frame_bit),
	_animation_cycles_flag= FLAG(_animation_cycles_bit),
	_animation_back_and_forth_flag= FLAG(_animation_back_and_forth_bit),
	_animation_initially_running_flag= FLAG(_animation_initially_running_bit)
};

/* ---------- structures */

/*
	The plan:
	
	You place a model animation marker down in Loathing.  This specifies:
		The model animation tag to use
		The initial position, yaw, and pitch.
	
	The model animation tag contains a series of model animation frames.
	Each frame needs to know
		The model tag to use
		The model permutation (specific to the model tag!)
		The offset in position
*/

#define SIZEOF_STRUCT_MODEL_ANIMATION_FRAME_DEFINITION 16
struct model_animation_frame_definition
{
	unsigned long flags;
	
	file_tag model_tag;
	short model_type; // postprocessed
	
	short permutation_index;

	word unused[2];
};

#define SIZEOF_STRUCT_MODEL_ANIMATION_DEFINITION 1024
struct model_animation_definition
{
	unsigned long flags;
	
	short number_of_frames;
	short ticks_per_frame;
	
	struct model_animation_frame_definition frames[MAXIMUM_FRAMES_PER_ANIMATION];

	word unused[222];

	short shadow_map_width, shadow_map_height;
	short shadow_bytes_per_row;
	word pad;
	
	short origin_offset_x, origin_offset_y;

	file_tag forward_sound_tag;
	file_tag backward_sound_tag;
	
	short forward_sound_type;
	short backward_sound_type;
	
	word unused2[20];

	int shadow_maps_offset, shadow_maps_size;
	byte *shadow_maps;
};


//=====================================================================================================
bool read_model_animation_definition( model_animation_definition* def, tag id );
bool save_model_animation_definition( model_animation_definition* def, tag id );

bool read_model_animation_definition( model_animation_definition* def, tag_entry* e );
bool save_model_animation_definition( model_animation_definition* def, tag_entry* e );


#endif