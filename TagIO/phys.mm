
#include "phys.h"





#include <assert.h>
#include "formatters.h"

// FIXME: move to myth_defs.h
#define ANGLE_TO_FLOAT(a) (((float)(a))/(float)FULL_CIRCLE*360)
#define FLOAT_TO_ANGLE(f) (angle)(((f)/360.0)*FULL_CIRCLE)

// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code physics_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	// short type; // type of physics model to use
	// word pad;
	_2byte,
	sizeof(short),
	
	// short buffer[SIZEOF_PHYSICS_BUFFER];
	SIZEOF_PHYSICS_BUFFER*sizeof(short),
	
	_end_bs_array
};

static byte_swap_code linear_physics_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	//unsigned long flags;
	_4byte,
	
	// fixed_vector3d linear_acceleration;
	_4byte,	_4byte,	_4byte,
	// fixed_vector3d initial_linear_velocity_min, initial_linear_velocity_delta;
	_4byte,	_4byte,	_4byte,
	_4byte,	_4byte,	_4byte,
	
	// fixed_vector3d random_acceleration;
	_4byte,	_4byte,	_4byte,

	// short unused[4];
	4*sizeof(short),
	
	_end_bs_array
};

static byte_swap_code cylindrical_physics_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	//unsigned long flags;
	_4byte,
	
	// angle angular_acceleration;
	// angle initial_angular_velocity_min, initial_angular_velocity_delta;
	_2byte,
	_2byte, _2byte,

	// short_world_distance initial_z_deviation;
	_2byte,

	// fixed attraction_constant;	
	// fixed radial_velocity;
	_4byte,
	_4byte,

	// fixed z_acceleration;
	_4byte,
	
	// short unused[18];
	18*sizeof(short),
	
	_end_bs_array
};

static byte_swap_code spherical_spring_physics_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	//unsigned long flags;
	_4byte,
	
	// angle initial_angular_velocity_min, initial_angular_velocity_delta;
	_2byte, _2byte,

	// fixed attraction_constant;
	_4byte,

	// short unused[24];
	24*sizeof(short),
	
	_end_bs_array
};

static byte_swap_code point_attractor_physics_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	//unsigned long flags;
	_4byte,

	// fixed maximum_velocity;
	// fixed radial_velocity;
	// fixed attraction_constant;	
	_4byte,
	_4byte,	
	_4byte,	

	// short_world_distance minimum_height_above_mesh;
	_2byte,

	// short unused[21];
	21*sizeof(short),
	
	_end_bs_array
};

static byte_swap_code converging_physics_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	//unsigned long flags;
	_4byte,
	
	// angle initial_angular_velocity_min, initial_angular_velocity_delta;
	_2byte, _2byte,

	// angle phi_min, phi_delta;
	_2byte, _2byte,

	// short unused[24];
	24*sizeof(short),
	
	_end_bs_array
};

static byte_swap_code explosive_gas_physics_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	//unsigned long flags;
	_4byte,
	
	//fixed starting_velocity_min, starting_velocity_delta;
	_4byte, _4byte,
	
	//fixed radial_acceleration;
	_4byte,
	
	//fixed_vector3d linear_acceleration;
	_4byte, _4byte, _4byte,
	
	//fixed_fraction drag;
	_2byte,
	
	//short unused[15];
	15 * sizeof(short),
	
	_end_bs_array
};


// ==============================  EDIT WINDOW  ========================================
void edit_phys( tag_entry* e )
{
	
}

void dependency_phys( tag_entry *e )
{
    #pragma unused( e )

#if 0 // no dependencies
	local_physics_definition def;
	read_local_physics_definition( &def, e );
#endif	// no dependencies
}


// ==============================  READ TAG DATA  ========================================
bool read_local_physics_definition( local_physics_definition* def, tag_entry* e )
{
	if( load_tag_data_into_struct( "phys", e, def, sizeof(local_physics_definition), 1, physics_definition_byte_swapping_data ) )
	{
		switch( def->type )
		{
			case _linear:
				byte_swap_data_be( "linear physics", &def->linear, sizeof(linear_physics_definition), 1, linear_physics_definition_byte_swapping_data );
				break;
				
			case _cylindrical:
				byte_swap_data_be( "cylindrical physics", &def->cylindrical, sizeof(cylindrical_physics_definition), 1, cylindrical_physics_definition_byte_swapping_data );
				break;
				
			case _spherical_spring:
				byte_swap_data_be( "spherical sptring physics", &def->spherical, sizeof(spherical_spring_physics_definition), 1, spherical_spring_physics_definition_byte_swapping_data );
				break;
				
			case _point_attractor:
				byte_swap_data_be( "point attractor physics", &def->attractor, sizeof(point_attractor_physics_definition), 1, point_attractor_physics_definition_byte_swapping_data );
				break;
				
			case _converging:
				byte_swap_data_be( "converging physics", &def->converging, sizeof(converging_physics_definition), 1, converging_physics_definition_byte_swapping_data );
				break;
				
			case _explosive_gas:
				byte_swap_data_be( "explosive gas physics", &def->converging, sizeof(explosive_gas_physics_definition), 1, explosive_gas_physics_definition_byte_swapping_data );
				break;
				
			default:
				return FALSE;
		}
	
		return TRUE;
	}
	return FALSE;
}

local_physics_definition default_phys =
{
	0,
	0,
	{ 0 }
};

void defaultnew_phys( tag_entry *e )
{
	local_physics_definition def;
	byte_swap_move_be( "phys", &def, &default_phys, sizeof(local_physics_definition), 1, physics_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}


// ==============================  SAVE TAG DATA  ========================================
bool save_local_physics_definition( local_physics_definition* def, tag_entry* e )
{
	local_physics_definition *physics = (local_physics_definition *)malloc( sizeof(local_physics_definition) );
	
	byte_swap_move_be( "phys", physics, def, sizeof(local_physics_definition), 1, physics_definition_byte_swapping_data );
	switch( def->type )
	{
		case _linear:
			byte_swap_data_be( "linear physics", &physics->linear, sizeof(linear_physics_definition), 1, linear_physics_definition_byte_swapping_data );
			break;
			
		case _cylindrical:
			byte_swap_data_be( "cylindrical physics", &physics->cylindrical, sizeof(cylindrical_physics_definition), 1, cylindrical_physics_definition_byte_swapping_data );
			break;
			
		case _spherical_spring:
			byte_swap_data_be( "spherical sptring physics", &physics->spherical, sizeof(spherical_spring_physics_definition), 1, spherical_spring_physics_definition_byte_swapping_data );
			break;
			
		case _point_attractor:
			byte_swap_data_be( "point attractor physics", &physics->attractor, sizeof(point_attractor_physics_definition), 1, point_attractor_physics_definition_byte_swapping_data );
			break;
			
		case _converging:
			byte_swap_data_be( "converging physics", &physics->converging, sizeof(converging_physics_definition), 1, converging_physics_definition_byte_swapping_data );
			break;
			
		case _explosive_gas:
			byte_swap_data_be( "explosive gas physics", &physics->explosive_gas, sizeof(explosive_gas_physics_definition), 1, explosive_gas_physics_definition_byte_swapping_data );
			break;
			
		default:
			free( physics );
			return FALSE;
		
	}
	
	set_entry_data( e, physics, sizeof(local_physics_definition), TRUE );
	return TRUE;
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_local_physics_definition( local_physics_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'phys', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_local_physics_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_local_physics_definition( local_physics_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'phys', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_local_physics_definition( def, e );
}


