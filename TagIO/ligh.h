// ligh.h
#ifndef __ligh_h__
#define __ligh_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

#ifndef __damage_h__
#include "damage.h"
#endif

/* ---------- constants */

enum
{
	LIGHTNING_GROUP_TAG= 0x6C696768, // 'ligh' (looks like text)
	LIGHTNING_VERSION_NUMBER= 1,
	
	MAXIMUM_LIGHTNING_DEFINITIONS_PER_MAP= 4
};

/* ---------- structures */

enum // lightning flags
{
	_lightning_duration_based_on_length_bit,
	_lightning_scars_ground_bit,
	_lightning_moves_objects_bit,
	_lightning_ignores_collisions_with_models_bit,
	
	NUMBER_OF_LIGHTNING_DEFINITION_FLAGS,
	
	_lightning_duration_based_on_length_flag= FLAG(_lightning_duration_based_on_length_bit),
	_lightning_scars_ground_flag= FLAG(_lightning_scars_ground_bit),
	_lightning_moves_objects_flag= FLAG(_lightning_moves_objects_bit),
	_lightning_ignores_collisions_with_models_flag= FLAG(_lightning_ignores_collisions_with_models_bit)
};

enum // sequences
{
	_lightning_sequence_fork,
	_lightning_sequence_main_bolt,
	_lightning_sequence_stain,
	NUMBER_OF_LIGHTNING_DEFINITION_SEQUENCES,
	
	MAXIMUM_NUMBER_OF_LIGHTNING_DEFINITION_SEQUENCES= 3
};

#define SIZEOF_STRUCT_LIGHTNING_DEFINITION 64
struct lightning_definition
{
	unsigned long flags;

	file_tag collection_reference_tag;

	long shock_duration; // in ticks
	long fade_duration;

	world_distance bolt_length;
	
	short sequence_indexes[MAXIMUM_NUMBER_OF_LIGHTNING_DEFINITION_SEQUENCES];

	short_world_distance velocity; // in WU per ticks
	short_fixed scale;
	
 	short fork_segment_minimum; // minimum number of fork segments (per fork)
	short fork_segment_delta;

	// These make the segments appear to connect properly
	short fork_overlap_amount; 
	short main_bolt_overlap_amount;
	
	// Specifies the bendiness of the forks
	angle angular_limit; // Shouldn't be set over 180 degrees

	struct damage_definition damage;

	short apparent_number_of_bolts;

	//------- postprocessed stuff
	short collection_reference_index;
	short collection_index;
	short color_table_index;
};


//=====================================================================================================
bool read_lightning_definition( lightning_definition* def, tag id );
bool save_lightning_definition( lightning_definition* def, tag id );

bool read_lightning_definition( lightning_definition* def, tag_entry* e );
bool save_lightning_definition( lightning_definition* def, tag_entry* e );

#endif