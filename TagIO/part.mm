
#include "part.h"


#include "tagpick.h"
#include <assert.h>


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code particle_system_byte_swapping_data[]=
{
	_begin_bs_array, 1,

//	unsigned long flags;
	_4byte,

//	file_tag collection_reference_tag;
	_4byte,

//	world_distance minimum_view_distance;
//	world_distance transparency_rolloff_point;
//	world_distance transparency_cutoff_point;
	_4byte,
	_4byte,
	_4byte,
	
// short sequence_index;
	_2byte,

//	short number_of_particles;
//	short maximum_particle_number_delta;
	_2byte,
	_2byte,

//	short_fixed scale;
	_2byte,	

//	fixed_vector3d velocity_lower_bound;
//	fixed_vector3d velocity_delta;
	_4byte, _4byte, _4byte,
	_4byte, _4byte, _4byte,
	
//	short_fixed x0_particle_number;
//	short_fixed x1_particle_number; 
//	short_fixed y0_particle_number;
//	short_fixed y1_particle_number; 
	_2byte,
	_2byte,
	_2byte,
	_2byte,

//	struct state_variables state[NUMBER_OF_SNOWING_STATES];

		_begin_bs_array, NUMBER_OF_SNOWING_STATES,

		// short duration_lower_bound, duration_delta;
		// fixed_fraction value_lower_bound, value_delta;
		// short transition_lower_bound, transition_delta;
		_2byte, _2byte,
		_2byte, _2byte,
		_2byte, _2byte,
		
		_end_bs_array,

	// file_tag ambient_sound_tag;
	_4byte,

	// file_tag splash_local_projectile_group_tag;
	_4byte,

	// short box_width;
	// short_world_distance box_top_height;
	// short_world_distance box_bottom_height;
	_2byte,
	_2byte,
	_2byte,
	
	// short max_splashes_per_cell;
	// short time_between_building_effects;
	_2byte,
	_2byte,

//	short unused[8];
	10*sizeof(short),
	
	//------- postprocessed stuff
//	short sprite_definition_index;
//  short ambient_sound_index;
//	short splash_local_projectile_group_type;
	3*sizeof(short),
	
	_end_bs_array
};

particle_system_definition default_part =
{
	0,
	static_cast<file_tag>(-1), //added static cast here JED
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0, 0, 0,
	0, 0, 0,
	0,
	0,
	0,
	0,
	// state variables
	0, 0,
	0, 0,
	0, 0,
	//end state variables
	-1,
	-1,
	0,
	0,
	0,
	0,
	0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0,
	0,
	0,
	0,
	0,
};


// ==============================  EDIT WINDOW  ========================================
void edit_part( tag_entry* e )
{
	//DEPRECATED
}

void defaultnew_part( tag_entry *e )
{
	particle_system_definition def;
	byte_swap_move_be( "part", &def, &default_part, sizeof(particle_system_definition), 1, particle_system_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}


void dependency_part( tag_entry *e )
{
	particle_system_definition def;
	read_particle_system_definition( &def, e );
	add_tag_dependency( 'spri', def.sprite_tag );
	add_tag_dependency( 'amso', def.ambient_sound_tag );
	add_tag_dependency( 'lpgr', def.splash_local_projectile_group_tag );
}

// ==============================  READ TAG DATA  ========================================
bool read_particle_system_definition( particle_system_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "part", e, def, sizeof(particle_system_definition), 1, particle_system_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_particle_system_definition( particle_system_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "part", e, def, sizeof(particle_system_definition), 1, particle_system_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_particle_system_definition( particle_system_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'part', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_particle_system_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_particle_system_definition( particle_system_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'part', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_particle_system_definition( def, e );
}





