
#include "core.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 



 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code collection_reference_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// file_tag collection_tag;
	_4byte,

	// short number_of_permutations;
	// short_fixed tint_fraction;
	_2byte,
	_2byte,

	// struct rgb_color colors[MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE][MAXIMUM_HUE_CHANGES_PER_COLLECTION];
	
		_begin_bs_array, MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE,
		
			_begin_bs_array, MAXIMUM_HUE_CHANGES_PER_COLLECTION,
			
			// word red, green, blue;
			// word flags;
			_2byte, _2byte, _2byte,
			_2byte,
			
			_end_bs_array,
		
		_end_bs_array,

	// struct rgb_color tint_color;
	_2byte, _2byte, _2byte, _2byte,

	// short unused[18];
	18*sizeof(short),

	// short collection_index;
	sizeof(short),
	
	// short color_table_indexes[MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERNECE];
	MAXIMUM_PERMUTATIONS_PER_COLLECTION_REFERENCE*sizeof(short),
	
	_end_bs_array
};


// ==============================  EDIT WINDOW  ========================================
void edit_core( tag_entry* e )
{
}


void dependency_core( tag_entry *e )
{
	collection_reference def;
	read_collection_reference( &def, e );
	add_tag_dependency( '.256', def.collection_tag );
}


// ==============================  READ TAG DATA  ========================================
bool read_collection_reference( collection_reference* def, tag_entry* e )
{
	return load_tag_data_into_struct( "core", e, def, SIZEOF_STRUCT_COLLECTION_REFERENCE, 1, collection_reference_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_collection_reference( collection_reference* def, tag_entry* e )
{
	return save_tag_data_from_struct( "core", e, def, SIZEOF_STRUCT_COLLECTION_REFERENCE, 1, collection_reference_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_collection_reference( collection_reference* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'core', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_collection_reference( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_collection_reference( collection_reference* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'core', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_collection_reference( def, e );
}

