// obje.h
#ifndef __obje_h__
#define __obje_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	OBJECT_DEFINITION_GROUP_TAG= 0x6f626a65, // 'obje' (looks like text)
	OBJECT_DEFINITION_VERSION_NUMBER= 1,

	MAXIMUM_OBJECT_DEFINITIONS_PER_MAP= 64
};

/* ---------- definition */

enum // object definition flags
{
	_draw_selection_box_bit,
	_draw_vitality_box_bit,
	_maintains_constant_height_above_ground_bit,
	_ignore_model_and_object_collisions_bit,
	NUMBER_OF_OBJECT_DEFINITION_FLAGS,

	_draw_selection_box_flag= FLAG(_draw_selection_box_bit),
	_draw_vitality_box_flag= FLAG(_draw_vitality_box_bit),
	_maintains_constant_height_above_ground_flag= FLAG(_maintains_constant_height_above_ground_bit),
	_ignore_model_and_object_collisions_flag= FLAG(_ignore_model_and_object_collisions_bit)
};

enum // effect modifiers
{
	_effect_modifier_close_range_physical_damage, // close range
	_effect_modifier_long_range_physical_damage, // long range
	_effect_modifier_explosive_damage,
	_effect_modifier_electrical_damage,
	_effect_modifier_magical_damage,
	_effect_modifier_paralysis,
	_effect_modifier_stone,
	_effect_modifier_gas_damage,
	_effect_modifier_confusion,
	NUMBER_OF_EFFECT_MODIFIERS,
	MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS= 16
};

/* ---------- structures */

#define SIZEOF_STRUCT_OBJECT_DEFINITION 64
struct object_definition
{
	word flags;
	
	short_world_distance gravity; // world_distance/(ticks^2)
	short_fixed elasticity; // how much velocity is preserved after a collision	
	short_world_distance terminal_velocity; // for gravity only

	short_fixed maximum_vitality, maximum_vitality_delta;
	short_fixed scale_lower_bound, scale_delta;
	
	short_fixed effect_modifiers[MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS];
	
	short_fixed minimum_damage; // how much damage must be done before it takes effect

	short unused[7];
};


// =========================================================================================

bool read_object_definition( object_definition* def, tag id );
bool save_object_definition( object_definition* def, tag id );

bool read_object_definition( object_definition* def, tag_entry* e );
bool save_object_definition( object_definition* def, tag_entry* e );



#endif