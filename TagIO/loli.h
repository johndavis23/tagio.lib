
// loli.h
#ifndef __loli_h__
#define __loli_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	LOCAL_LIGHT_DEFINITION_GROUP_TAG = 'loli',	// 'loli'
	LOCAL_LIGHT_DEFINITION_VERSION_NUMBER = 1,
	MAXIMUM_LOCAL_LIGHT_DEFINITIONS_PER_MAP = 64
};


struct loli_definition {
	word flags;
	
	short fade_in_ticks;
	short sustain_ticks;
	short fade_out_ticks;
	
	world_distance radius;
	
	struct rgb_color dim_color;
	struct rgb_color bright_color;
	
	angle flicker_speed_0;
	angle flicker_speed_1;
	word flicker_amplitude_0;
	word flicker_amplitude_1;
	
	angle jitter_speed_x;
	short_world_distance jitter_size_x;
	
	angle jitter_speed_y;
	short_world_distance jitter_size_y;
	
	angle jitter_speed_z;
	short_world_distance jitter_size_z;
};


//=====================================================================================================
bool read_loli_definition( loli_definition* def, tag id );
bool save_loli_definition( loli_definition* def, tag id );

bool read_loli_definition( loli_definition* def, tag_entry* e );
bool save_loli_definition( loli_definition* def, tag_entry* e );


#endif