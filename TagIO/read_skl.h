

#ifndef __read_skl_h__
#define __read_skl_h__

#include <stdio.h>

#ifndef __skelanim_h__
#include "skelanim.h"
#endif

//void read_skl( struct FILE *f );
void compact_mesh();
void compact_bones();
void divide_st();
void set_skl_to_frame( int f, bool normals = true );
void compute_bone_bounds();
void fix_normals( int init_frame = 0 );
void fill_npatch_cracks();
void build_collapse_order();
struct skmd_lod *make_skmd_lod( int min_vertices, int max_vertices, bool use_npatch );
struct skmd_modelheader *make_skmd_modelheader();

const char *get_bone_name( int index );
void make_skan_base( int origin_bone_index, skan_bone_base *out, int frame = 0 );
skan_animation *make_skan_animation( int origin_bone_index,
									const skan_bone_base base, int length,
									const int *offset_settings,
									bool loopable );

void QuatMult( float out[ 4 ], const float q1[ 4 ], const float q2[ 4 ] );
void SlerpQuaternion( float output[ 4 ], const float in_from[ 4 ], const float in_to[ 4 ], float t );
void QuatToMat( float m[ 3 ][ 3 ], const float q[ 4 ] );
void MatToQuat( float destQuat[ 4 ], const float srcMatrix[ 3 ][ 3 ] );
void MatMult( float out[3][3], const float a[3][3], const float b[3][3] );

#endif

