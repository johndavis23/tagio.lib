/*
CRC.C
Sunday, March 5, 1995 6:25:36 PM
*/

void crc_new(unsigned long *crc_reference);
void crc_checksum_buffer(unsigned long *crc_reference, void *buffer, long count);
