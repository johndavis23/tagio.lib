
#include "game.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code game_parameters_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	// 	file_tag tutorial_map;
	_4byte,
	
	// file_tag first_map;
	_4byte,
	
	// file_tag default_netmap;
	_4byte,

	// short unused[10];
	sizeof(short) * 10,
	
	_end_bs_array
};

game_parameters_definition default_game =
{
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

// ==============================  EDIT WINDOW  ========================================
void edit_game( tag_entry* e )
{
}

void defaultnew_game( tag_entry *e )
{
	game_parameters_definition def;
	byte_swap_move_be( "game", &def, &default_game, sizeof(game_parameters_definition), 1, game_parameters_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_game( tag_entry *e )
{
	game_parameters_definition def;
	read_game_parameters_definition( &def, e );
	add_tag_dependency( 'mesh', def.training_map );
	add_tag_dependency( 'mesh', def.first_map );
	add_tag_dependency( 'mesh', def.default_netmap );
}

// ==============================  READ TAG DATA  ========================================
bool read_game_parameters_definition( game_parameters_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "game", e, def, SIZEOF_STRUCT_GAME_PARAMETERS_DEFINITION, 1, game_parameters_definition_byte_swapping_data );
}


// ==============================  SAVE TAG DATA  ========================================
bool save_game_parameters_definition( game_parameters_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "game", e, def, SIZEOF_STRUCT_GAME_PARAMETERS_DEFINITION, 1, game_parameters_definition_byte_swapping_data );
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_game_parameters_definition( game_parameters_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'game', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_game_parameters_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_game_parameters_definition( game_parameters_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'game', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_game_parameters_definition( def, e );
}



