/*
** CTD.H
*/

#ifndef __ctd_h__
#define __ctd_h__

#include "myth_defs.h"
#include "tags.h"

void CollapseTerrainDetails(tag_file* tf,  tag terrain_stack_tag, tag detail_stack_tag );

#endif