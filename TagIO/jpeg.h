
#ifndef __jpeg_h__
#define __jpeg_h__

#ifdef __cplusplus
extern "C" {
#endif
    
    void decompress_jpeg( void *out, const void *in, int in_size,
                         int width, int height, int channels, bool hq );
    
    void compress_jpeg( void *out, int *out_len, const void *in,
                       int width, int height, int channels, int quality );
    
    int estimate_jpeg_quality( const void *jpeg, int jpeg_len,
                              int width, int height, int channels );
    
#ifdef __cplusplus
}
#endif

#endif

