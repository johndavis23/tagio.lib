
#include "meef.h"



#include "tagpick.h"
#include <assert.h>



// ==============================  BYTE SWAPPING DATA  ========================================
byte_swap_code mesh_effect_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,

	// short_world_distance amplitude;
	// short_world_distance cutoff_radius;
	// short_fixed dissipation;
	_2byte,	
	_2byte,
	_2byte,

	// short_world_distance velocity;
	// short_world_distance epicenter_offset;
	_2byte,
	_2byte,

	// short camera_shaking_type;
	_2byte,

	// file_tag collection_reference_tag;
	// short sequence_index;
	_4byte,
	_2byte,

	// short number_of_radial_points;
	_2byte,

	// short_world_distance shockwave_thickness;
	// short_world_distance shockwave_height_offset;
	_2byte,
	_2byte,

	// computed during postprocessing
	
	// short collection_reference_index;
	// short collection_index, color_table_index;
	// short pad;
	4*sizeof(short),
	
	_end_bs_array
};

mesh_effect_definition default_meef =
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	static_cast<file_tag>(-1),
	0,
	0,
	0,
	0,
	0,
	0, 0,
	0
};	


// ==============================  EDIT WINDOW  ========================================
void edit_meef( tag_entry* e )
{

}

void defaultnew_meef( tag_entry *e )
{
	mesh_effect_definition def;
	byte_swap_move_be( "meef", &def, &default_meef, sizeof(mesh_effect_definition), 1, mesh_effect_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_meef( tag_entry *e )
{
	mesh_effect_definition def;
	read_mesh_effect_definition( &def, e );
	add_tag_dependency( 'core', def.collection_reference_tag );
}


// ==============================  READ TAG DATA  ========================================
bool read_mesh_effect_definition( mesh_effect_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "meef", e, def, SIZEOF_STRUCT_MESH_EFFECT_DEFINITION, 1, mesh_effect_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_mesh_effect_definition( mesh_effect_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "meef", e, def, SIZEOF_STRUCT_MESH_EFFECT_DEFINITION, 1, mesh_effect_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_mesh_effect_definition( mesh_effect_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'meef', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_mesh_effect_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_mesh_effect_definition( mesh_effect_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'meef', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_mesh_effect_definition( def, e );
}


