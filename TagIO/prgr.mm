#include "prgr.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 
 
 
 
#include "formatters.h"


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code projectile_group_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	
	// short number_of_parts;
	_2byte,
	
	// word pad;
	sizeof(word),

	// file_tag mesh_effect_tag;
	// file_tag sound_tag;
	// file_tag local_projectile_group_tag;
	_4byte,
	_4byte,
	_4byte,
	
	// short unused[3];
	3*sizeof(short),
	
	// short local_projectile_group_type;
	// short mesh_effect_type;
	// short sound_index;
	3*sizeof(short),
	
	// struct projectile_group_part_definition parts[MAXIMUM_NUMBER_OF_PARTS_PER_PROJECTILE_GROUP];
	
		_begin_bs_array, MAXIMUM_NUMBER_OF_PARTS_PER_PROJECTILE_GROUP,
		
		// file_tag projectile_tag;
		_4byte,

		// unsigned long flags;
		_4byte,
			
		// short count_lower_bound, count_delta;
		// short_fixed position_lower_bound, position_delta;
		_2byte, _2byte,
		_2byte, _2byte,

		// short obsolete[2];
		2*sizeof(short),
		
		// fixed_fraction appearing_fraction; // chance that this part will actually be created
		_2byte,
		
		// short unused[4];
		4*sizeof(short),
		
		// short projectile_type;
		1*sizeof(short),
		
		_end_bs_array,
	
	_end_bs_array
};

projectile_group_definition default_prgr =
{
	0,
	0,
	0,
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	0, 0, 0,
	0,
	0,
	0,
	// parts
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};


// ==============================  EDIT WINDOW  ========================================
void edit_prgr( tag_entry* e )
{

}

void defaultnew_prgr( tag_entry *e )
{
	projectile_group_definition def;
	byte_swap_move_be( "prgr", &def, &default_prgr, sizeof(projectile_group_definition), 1, projectile_group_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}


void dependency_prgr( tag_entry *e )
{
	projectile_group_definition def;
	read_projectile_group_definition( &def, e );
	add_tag_dependency( 'meef', def.mesh_effect_tag );
	add_tag_dependency( 'soun', def.sound_tag );
	add_tag_dependency( 'lpgr', def.local_projectile_group_tag );
	for (int i = 0; i < def.number_of_parts; ++i)
	{
		add_tag_dependency( 'proj', def.parts[i].projectile_tag );
	}
}

// ==============================  READ TAG DATA  ========================================
bool read_projectile_group_definition( projectile_group_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "prgr", e, def, SIZEOF_STRUCT_PROJECTILE_GROUP_DEFINITION, 1, projectile_group_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_projectile_group_definition( projectile_group_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "prgr", e, def, SIZEOF_STRUCT_PROJECTILE_GROUP_DEFINITION, 1, projectile_group_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_projectile_group_definition( projectile_group_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'prgr', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_projectile_group_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_projectile_group_definition( projectile_group_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'prgr', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_projectile_group_definition( def, e );
}

