#ifndef __tagpick_h__
#include "tagpick.h"
#endif
#include <assert.h>
#include <ctype.h>


static int single_file_by_default = true;


class pick_info {
public:
	tag_file *limit_file;
	bool use_single_file;
	
	tag chosen_id;
	tag type;
	
	
};


extern "C" {

static int tag_sorter( const void *a, const void *b )
{
	tag_entry *ea, *eb;
	ea = *(tag_entry **)a;
	eb = *(tag_entry **)b;
	
	const char *ca = get_entry_name(ea);
	const char *cb = get_entry_name(eb);
	
	while (*ca && tolower(*ca) == tolower(*cb)) {
		++ca;
		++cb;
	}
	return tolower(*ca) - tolower(*cb);
}
}


void add_file_tags( pick_info *info, tag_file *f )
{
	tag_group *g = get_group_in_file( f, info->type, false );
	if (!g) return;
	tag_entry *e = 0;
	
}



