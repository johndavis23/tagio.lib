
#include "amso.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code ambient_sound_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	// short_world_distance inner_radius, outer_radius; // of both sounds
	_2byte, _2byte,
	// short period_lower_bound, period_delta; // of random sound
	_2byte, _2byte,
	// file_tag sound_tags[MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS];
	_begin_bs_array, MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS, _4byte, _end_bs_array,
	// short_world_distance random_sound_radius;
	_2byte,
	// short unused[5];
	5 * sizeof(short),
	// short sound_indexes[MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS];
	MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS * sizeof(short),
	// short phase, period;
	2 * sizeof(short),
	
	_end_bs_array
};

ambient_sound_definition default_amso =
{
	0,
	0, 0,
	0, 0,
	static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1),
	0,
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0,
	0, 0
};

// ==============================  EDIT WINDOW  ========================================
void edit_amso( tag_entry* e )
{
	
}

void defaultnew_amso( tag_entry *e )
{
	ambient_sound_definition def;
	byte_swap_move_be( "amso", &def, &default_amso, sizeof(ambient_sound_definition), 1, ambient_sound_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_amso( tag_entry *e )
{
	ambient_sound_definition def;
	read_ambient_sound_definition( &def, e );
	for (int i = 0; i < NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS; ++i)
	{
		add_tag_dependency( 'soun', def.sound_tags[i] );
	}
}

// ==============================  READ TAG DATA  ========================================
bool read_ambient_sound_definition( ambient_sound_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "amso", e, def, SIZEOF_STRUCT_AMBIENT_SOUND_DEFINITION, 1, ambient_sound_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_ambient_sound_definition( ambient_sound_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "amso", e, def, SIZEOF_STRUCT_AMBIENT_SOUND_DEFINITION, 1, ambient_sound_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_ambient_sound_definition( ambient_sound_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'amso', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_ambient_sound_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_ambient_sound_definition( ambient_sound_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'amso', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_ambient_sound_definition( def, e );
}


