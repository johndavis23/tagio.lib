
#ifndef __markers_h__
#define __markers_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif


/* ---------- markers */

enum // marker types
{
	_marker_team, // no tag
	_marker_scenery,
	_marker_unused1,
	_marker_monster, // refers to unit tag
	_marker_unused2,
	_marker_sound_source,
	_marker_model,
	_marker_unused3,
	_marker_unused4,
	_marker_projectile,
	_marker_local_projectile_group,
	_marker_model_animation,
	NUMBER_OF_MARKER_TYPES
};

enum // marker palette flags
{
	_marker_palette_uncontrollable_bit, // has the "ignores player control" bit set
	_marker_palette_may_be_traded_bit, // if the game allows, these units can be traded for other units
	_marker_palette_may_use_veterans_bit, // if veterans exist from the previous level they can be used as replacements for these units
	_marker_palette_must_use_veterans_bit, // veterans must exist from the previous level for these units to be placed
	_marker_palette_ambient_life_bit,
	_marker_palette_hunted_creature_bit,
	
	NUMBER_OF_MARKER_PALETTE_FLAGS,
	
	_marker_palette_uncontrollable_flag= FLAG(_marker_palette_uncontrollable_bit),
	_marker_palette_may_be_traded_flag= FLAG(_marker_palette_may_be_traded_bit),
	_marker_palette_may_use_veterans_flag= FLAG(_marker_palette_may_use_veterans_bit),
	_marker_palette_must_use_veterans_flag= FLAG(_marker_palette_must_use_veterans_bit),
	_marker_palette_ambient_life_flag= FLAG(_marker_palette_ambient_life_bit),
	_marker_palette_hunted_creature_flag= FLAG(_marker_palette_hunted_creature_bit)	
};

enum // marker palette extra flags
{
	_marker_palette_extra_appears_on_netmap_game_type_bit,
	_marker_palette_extra_unused_bit= 17,
	
	NUMBER_OF_MARKER_PALETTE_EXTRA_FLAGS,

	_marker_palette_extra_appears_on_netmap_game_type_flag= FLAG(_marker_palette_extra_appears_on_netmap_game_type_bit)
};

#define SIZEOF_STRUCT_MARKER_PALETTE_ENTRY 32
struct marker_palette_entry
{
	short type; // of marker (types which don't need palettes don't have them)
	word flags;
	
	file_tag tag; // subgroup tag based on type (types which don't need subgroups don't have them)
	
	short team_index; // to establish ownership; only planned to be used for monsters and observers	
	short pad;
	
	unsigned long extra_flags;

	short unused[4];
	
	// load time
	
	struct marker_data *render_chain;
	
	short count; // total references on map; temporary field constructed during palette editing
	short tag_index; // NONE if couldn't load tag
};

enum // marker flags
{
	_marker_is_invisible_bit, // not initially placed
	_marker_is_stone_bit, // stone balls/flags never change owners
	_marker_detonates_immediately_bit,
	_marker_is_invisible_observer_bit,
	_marker_is_netgame_target_bit,
	_marker_unused5_bit,
	_marker_unused6_bit,
	_marker_unused7_bit,
	
	NUMBER_OF_MARKER_FLAGS,
	
	_marker_is_invisible_flag= FLAG(_marker_is_invisible_bit),
	_marker_is_stone_flag= FLAG(_marker_is_stone_bit),
	_marker_detonates_immediately_flag= FLAG(_marker_detonates_immediately_bit),
	_marker_is_invisible_observer_flag= FLAG(_marker_is_invisible_observer_bit),
	_marker_is_netgame_target_flag= FLAG(_marker_is_netgame_target_bit),
	_marker_unused5_flag= FLAG(_marker_unused5_bit),
	_marker_unused6_flag= FLAG(_marker_unused6_bit),
	_marker_unused7_flag= FLAG(_marker_unused7_bit)
};

#define SIZEOF_MARKER_USER_DATA 16
#define SIZEOF_STRUCT_MARKER_DATA 64
struct marker_data
{
	unsigned long flags;
	
	short type; // _marker_observer, _marker_scenery, ...
	short palette_index; // index into global marker palette list

	short identifier; // unique for each marker type
	
	short minimum_difficulty_level;
	
	// expanded from old world_location3d because we added a roll to the struct
	// world_location3d location;
		world_point3d position;
		world_vector3d velocity;
		short_world_distance height; // height above ground (or under media?)
		angle yaw, pitch;


	char user_data[SIZEOF_MARKER_USER_DATA];	// specific to the marker type
	
	angle roll;
	
	short unused[1];

	// load time
	marker_data *render_chain;
	
	short data_index, data_identifier; // usually object_index/object_identifier
};


extern byte_swap_code marker_data_bs_data[];
extern byte_swap_code marker_palette_data_bs_data[];

#endif

