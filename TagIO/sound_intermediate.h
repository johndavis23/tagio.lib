#ifndef __sound_intermediate_h__
#define __sound_intermediate_h__

// phook - ported over from the myth2/wintagedit code, adjustments to UI stuff mostly
enum
{
	_no_compression, // note that 8 bit is like mac SND-> ie 0x80 is silence
	_ima_adpcm_compression,
	_apple_ima_adpcm_compression,
	NUMBER_OF_COMPRESSION_TYPES
};

enum
{
	_frequency_22kHz= 22050,
	_frequency_44kHz= 44100
};

enum
{
	SOUND_MAGIC_COOKIE= 'SOUN'
};

struct intermediate_sound_data
{
	long magic_cookie;
	bool initialized;
	short compression_type;
	short uncompressed_sample_size_in_bits;
	long frequency;
	short number_of_channels;
	long sample_frame_count;
	void *samples;
};

struct intermediate_sound_data *new_intermediate_sound( void );
void dispose_intermediate_sound( intermediate_sound_data *sound );

void initialize_sound_header(
	intermediate_sound_data *sound,
	short compression_type,
	long sample_frame_count,
	short uncompressed_size_in_bits,
	long frequency,
	short number_of_channels
	);
	
void set_sound_samples( intermediate_sound_data *sound, void *samples );

void set_intermediate_sound_compression_type( intermediate_sound_data *sound, short compression_type );
short get_intermediate_sound_compression_type( intermediate_sound_data *sound );

void set_uncompressed_sample_size_in_bits( intermediate_sound_data *sound, short size );
short get_uncompressed_sample_size_in_bits( intermediate_sound_data *sound );

void set_intermediate_sound_frequency( intermediate_sound_data *sound, long frequency );
long get_intermediate_sound_frequency( intermediate_sound_data *sound );

long get_intermediate_sound_sample_count( intermediate_sound_data *sound );

void *get_intermediate_sound_samples( intermediate_sound_data *sound );

long get_samples_size( intermediate_sound_data *sound );

long get_intermediate_sample_count( intermediate_sound_data *sound );

#endif // __sound_intermediate_h__
