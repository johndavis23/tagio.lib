
#include "medi.h"

#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code media_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	
	// file_tag collection_reference_tag;
	_4byte,
	
	// short unused[8];
	8*sizeof(short),

	// file_tag projectile_group_tags[MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS];
	
		_begin_bs_array, MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS,
		
		_4byte,
		
		_end_bs_array,

	// short_fixed reflection_tint_fraction;
	// struct rgb_color reflection_tint_color;
	_2byte,
	_2byte, _2byte, _2byte, _2byte,

	// fixed_fraction surface_effect_density; // percentage of maximum density
	// file_tag surface_effect_local_projectile_group_tag;
	_2byte,
	_4byte,

	// world_vector3d wobble_magnitude;
	// world_vector3d wobble_phase_magnitude;
	_2byte, _2byte, _2byte,
	_2byte, _2byte, _2byte,
	
	// short effects_per_cell;
	// short time_between_building_effects;
	_2byte,
	_2byte,

	// fixed_fraction reflection_transparency;
	_2byte,
	
	// short_fixed min_opacity;
	_2byte,
	
	// short_fixed max_opacity;
	_2byte,
	
	// short_fixed max_opacity_depth;
	_2byte,
	
	// fixed_fraction spec_min, spec_max;
	_2byte, _2byte,
	
	// short_fixed emboss_fraction;
	_2byte,
	
	_begin_bs_array, 4,
		// short_fixed wobble_scale;
		_2byte,
		
		// short_fixed wobble_speed;
		_2byte,
		
		// short_fixed base_scale[2];
		_2byte, _2byte,
		
		// short_fixed base_speed[2];
		_2byte, _2byte,
		
	_end_bs_array,
	
	// short unused2[18];
	18*sizeof(short),
	
	// computed during postprocessing

	// short surface_effect_local_projectile_group;
	// short surface_effect_local_projectile_group_type;
	// short collection_reference_index;
	3*sizeof(short),
	
	// short projectile_group_types[MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS];
	MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS*sizeof(short),

	_end_bs_array
};

media_definition default_medi =
{
	0,
	static_cast<file_tag>(-1),
	0, 0, 0, 0, 0, 0, 0, 0,
	static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1),
	0,
	0, 0, 0, 0,
	0,
	static_cast<file_tag>(-1),
	0, 0, 0,	
	0, 0, 0,
	0,
	0,
	0,
	FLOAT_TO_SHORT_FIXED( 0.1 ),
	FLOAT_TO_SHORT_FIXED( 1.0 ),
	FLOAT_TO_SHORT_FIXED( 2.0 ),
	FLOAT_TO_FIXED( 0.2 ),
	FLOAT_TO_FIXED( 0.8 ),
	FLOAT_TO_SHORT_FIXED( 1.0 ),
	
		FLOAT_TO_SHORT_FIXED( 0.5 ),
		FLOAT_TO_SHORT_FIXED( 1.0 ),
		FLOAT_TO_SHORT_FIXED( 0.4 ),
		FLOAT_TO_SHORT_FIXED( 0.4 ),
		FLOAT_TO_SHORT_FIXED( 0.1 ),
		FLOAT_TO_SHORT_FIXED( 0.0 ),
	
		FLOAT_TO_SHORT_FIXED( 0.2 ),
		FLOAT_TO_SHORT_FIXED( 1.1 ),
		FLOAT_TO_SHORT_FIXED( 0.3 ),
		FLOAT_TO_SHORT_FIXED( 0.3 ),
		FLOAT_TO_SHORT_FIXED( 0.0 ),
		FLOAT_TO_SHORT_FIXED( 0.1 ),
	
		FLOAT_TO_SHORT_FIXED( 0.1 ),
		FLOAT_TO_SHORT_FIXED( 0.2 ),
		FLOAT_TO_SHORT_FIXED( 0.4 ),
		FLOAT_TO_SHORT_FIXED( 0.4 ),
		FLOAT_TO_SHORT_FIXED( 0.0 ),
		FLOAT_TO_SHORT_FIXED( 0.0 ),
	
		FLOAT_TO_SHORT_FIXED( 0.2 ),
		FLOAT_TO_SHORT_FIXED( 0.3 ),
		FLOAT_TO_SHORT_FIXED( 0.3 ),
		FLOAT_TO_SHORT_FIXED( 0.3 ),
		FLOAT_TO_SHORT_FIXED( 0.1 ),
		FLOAT_TO_SHORT_FIXED( 0.1 ),
	
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	
	0,
	0,
	0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};	


// ==============================  EDIT WINDOW  ========================================
void edit_medi( tag_entry* e )
{
}

void dependency_medi( tag_entry *e )
{
	media_definition def;
	read_media_definition( &def, e );
	add_tag_dependency( 'txst', def.collection_reference_tag );
	add_tag_dependency( 'lpgr', def.surface_effect_local_projectile_group_tag );
	for (int i = 0; i < NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS; ++i)
	{
		add_tag_dependency( 'prgr', def.projectile_group_tags[i] );
	}
}

// ==============================  READ TAG DATA  ========================================
bool read_media_definition( media_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "medi", e, def, SIZEOF_STRUCT_MEDIA_DEFINITION, 1, media_definition_byte_swapping_data);
}

void defaultnew_medi( tag_entry *e )
{
	media_definition def;
	byte_swap_move_be( "medi", &def, &default_medi, sizeof(media_definition), 1, media_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

// ==============================  SAVE TAG DATA  ========================================
bool save_media_definition( media_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "medi", e, def, SIZEOF_STRUCT_MEDIA_DEFINITION, 1, media_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_media_definition( media_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'medi', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_media_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_media_definition( media_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'medi', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_media_definition( def, e );
}


