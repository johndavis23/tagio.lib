/*
** FORMATTERS.CPP
*/
 

 
 
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include "myth_defs.h"
#include "tags.h"
#include "formatters.h"

static char formatters_text[256];

// core math
float incremental_round( float value, float increment )
{
	assert( increment );
	
	float float_value = value / increment;
	long trunc_value = float_value;
	float rounded;

	if( float_value > 0.0 )
	{
		if( float_value - (float)trunc_value < 0.5 )
			rounded = trunc_value * increment;
		else
			rounded = (trunc_value + 1) * increment;
	}
	else
	{
		if( float_value - (float)trunc_value > -0.5 )
			rounded = trunc_value * increment;
		else
			rounded = (trunc_value - 1) * increment;
	}
	
	return rounded;
}

char get_delta( char lower, char upper )
{
	if( upper - lower )
	{
		return upper - lower + 1;
	}
	
	return 0;
}

char get_upper_bound( char lower, char delta )
{
	if( delta )
		return lower + delta - 1;
		
	return lower + delta;
}

unsigned char get_delta( unsigned char lower, unsigned char upper )
{
	if( upper - lower )
	{
		return upper - lower + 1;
	}
	
	return 0;
}

unsigned char get_upper_bound( unsigned char lower, unsigned char delta )
{
	if( delta )
		return lower + delta - 1;
		
	return lower + delta;
}

short get_delta( short lower, short upper )
{
	if( upper - lower )
	{
		return upper - lower + 1;
	}
	
	return 0;
}

short get_upper_bound( short lower, short delta )
{
	if( delta )
		return lower + delta - 1;
		
	return lower + delta;
}

unsigned short get_delta( unsigned short lower, unsigned short upper )
{
	if( upper - lower )
	{
		return upper - lower + 1;
	}
	
	return 0;
}

unsigned short get_upper_bound( unsigned short lower, unsigned short delta )
{
	if( delta )
		return lower + delta - 1;
		
	return lower + delta;
}

long get_delta( long lower, long upper )
{
	if( upper - lower )
	{
		return upper - lower + 1;
	}
	
	return 0;
}

long get_upper_bound( long lower, long delta )
{
	if( delta )
		return lower + delta - 1;
		
	return lower + delta;
}

unsigned long get_delta( unsigned long lower, unsigned long upper )
{
	if( upper - lower )
	{
		return upper - lower + 1;
	}
	
	return 0;
}

unsigned long get_upper_bound( unsigned long lower, unsigned long delta )
{
	if( delta )
		return lower + delta - 1;
		
	return lower + delta;
}


float get_delta( float lower, float upper )
{
	if( upper - lower )
	{
		return upper - lower + 1;
	}
	
	return 0;
}

float get_upper_bound( float lower, float delta )
{
	if( delta )
		return lower + delta - 1;
		
	return lower + delta;
}



float short_fixed_to_float( short_fixed val )
{
	return (float)(((double)val) / (double)SHORT_FIXED_ONE);
}

short_fixed float_to_short_fixed( float val )
{
	long whole = (val < 0) ? ceil( val ) : floor( val );
	float decimal = (val < 0) ? (val - whole) * -1: val - whole;

#if 0
	decimal = (decimal * (float)(SHORT_FIXED_ONE + 0.5));
#else
	decimal = round( decimal * (float)SHORT_FIXED_ONE );
#endif
	
	
	if( val < 0 )
		return (short_fixed)((whole * SHORT_FIXED_ONE) - decimal);
		
	return (short_fixed)((whole * SHORT_FIXED_ONE) + decimal);
}


float fixed_to_float( fixed val )
{
	return (float)(((double)val) / (double)FIXED_ONE);
}

fixed float_to_fixed( float val )
{
	long whole = (val < 0) ? ceil( val ) : floor( val );
	float decimal = (val < 0) ? (val - whole) * -1: val - whole;

#if 0
	decimal = (decimal * (float)(FIXED_ONE + 0.5));
#else
	decimal = round( decimal * (float)FIXED_ONE );
#endif

	if( val < 0 )
		return (fixed)((whole * FIXED_ONE) - decimal);
		
	return (fixed)((whole * FIXED_ONE) + decimal);
}


float short_fixed_fraction_to_float( short_fixed_fraction val )
{
	return (float)(((double)val) / SHORT_FIXED_ONE);
}

short_fixed_fraction float_to_short_fixed_fraction( float val )
{
#if 0
	return (short_fixed_fraction)(val * (float)(SHORT_FIXED_ONE + 0.5));
#else
	return (short_fixed_fraction)round( val * (float)SHORT_FIXED_ONE );
#endif
}


float fixed_fraction_to_float( fixed_fraction val )
{
	return (float)(((double)val) / FIXED_ONE);
}

fixed_fraction float_to_fixed_fraction( float val )
{
#if 0
	return (short_fixed_fraction)(val * (float)(FIXED_ONE + 0.5));
#else
	return (short_fixed_fraction)round( val * (float)FIXED_ONE );
#endif
}


float short_percentage_to_float( short_fixed_fraction val )
{
	return ((float)(val)) / (SHORT_FIXED_ONE - 1);
}

short_fixed_fraction float_to_short_percentage( float val )
{
	return (short_fixed_fraction)PIN( incremental_round( val * (float)(SHORT_FIXED_ONE - 1 ), 1.0 ), 0, SHORT_FIXED_ONE - 1 );
}


float percentage_to_float( fixed_fraction val )
{
	return ((float)(val)) / (FIXED_ONE - 1);
}

fixed_fraction float_to_percentage( float val )
{
	return (fixed_fraction)PIN( incremental_round( val * (float)(FIXED_ONE - 1), 1.0), 0, FIXED_ONE - 1 );
}


float short_world_distance_to_float( short_world_distance val )
{
	return (float)(val / (float)WORLD_ONE);
}

short_world_distance float_to_short_world_distance( float val )
{
	return (short_world_distance)incremental_round( (val * (float)WORLD_ONE), 1 );
}


float world_distance_to_float( world_distance val )
{
	return (float)(val / (float)WORLD_ONE);
}

world_distance float_to_world_distance( float val )
{
	return (world_distance)incremental_round( (val * (float)WORLD_ONE), 1);
}


float angle_to_float( angle val )
{
#if 1
	return (float)(((val << (32 - ANGULAR_BITS)) * 360.0) / 4294967296);
#else
	return incremental_round( (((val << (32 - ANGULAR_BITS)) * 360.0) / 4294967296), 1.0 );
#endif
}

angle float_to_angle( float val )
{
	unsigned long temp = floor( ((val * (double)(1 << ANGULAR_BITS)) / 360.0) + 0.5 );
	return temp & (FULL_CIRCLE - 1);
}


//  GUI functions
char *format_float( double value, int precision )
{
	sprintf( formatters_text, "%.*f", precision, value );
	char *s = formatters_text + strlen( formatters_text ) - 1;
	
	while( s > formatters_text && *s == '0' && s[-1] != '.' )
	{
		*s = 0;
		--s;
	}
	
	return formatters_text;
}
