// temp.h
#ifndef __temp_h__
#define __temp_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	TEMPLATE_DEFINITION_GROUP_TAG= 'temp',
	DIALOG_STRING_DEFINITION_GROUP_TAG= 'ditl',
	MAX_TEMPLATE_SIZE= 16384
	// same as 'stli'
};

struct template_definition
{
	long string_length;
	char string_buffer[MAX_TEMPLATE_SIZE];
};

//=====================================================================================================
bool read_template_definition( template_definition* def, tag id );
bool save_template_definition( template_definition* def, tag id );

bool read_template_definition( template_definition* def, tag_entry* e );
bool save_template_definition( template_definition* def, tag_entry* e );


#endif