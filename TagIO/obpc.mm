
#include "obpc.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code observer_physics_constants_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// struct observer_physics_constant	parallel_velocity, orthogonal_velocity;
	_4byte, _4byte, _4byte, _4byte, _4byte, _4byte,
	_4byte, _4byte, _4byte, _4byte, _4byte, _4byte,
	
	// struct observer_physics_constant yaw_velocity, orbit_velocity;
	_4byte, _4byte, _4byte, _4byte, _4byte, _4byte,
	_4byte, _4byte, _4byte, _4byte, _4byte, _4byte,
	
	// struct observer_physics_constant vertical_velocity;
	_4byte, _4byte, _4byte, _4byte, _4byte, _4byte,

	// struct observer_physics_constant pitch_velocity;
	_4byte, _4byte, _4byte, _4byte, _4byte, _4byte,

	// struct observer_physics_constant zoom_velocity;
	_4byte, _4byte, _4byte, _4byte, _4byte, _4byte,

	// short unused[44];
	44*sizeof(short),
	
	_end_bs_array
};

observer_physics_constants default_obpc =
{
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};


// ==============================  EDIT WINDOW  ========================================
void edit_obpc( tag_entry* e )
{

}

void defaultnew_obpc( tag_entry *e )
{
	observer_physics_constants def;
	byte_swap_move_be( "obpc", &def, &default_obpc, sizeof(observer_physics_constants), 1, observer_physics_constants_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_obpc( tag_entry *e )
{
    #pragma unused( e )

#if 0 // no dependencies
	observer_physics_constants def;
	read_observer_physics_constants( &def, e );
#endif
}


// ==============================  READ TAG DATA  ========================================
bool read_observer_physics_constants( observer_physics_constants* def, tag_entry* e )
{
	return load_tag_data_into_struct( "obpc", e, def, SIZEOF_STRUCT_OBSERVER_PHYSICS_CONSTANTS, 1, observer_physics_constants_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_observer_physics_constants( observer_physics_constants* def, tag_entry* e )
{
	return save_tag_data_from_struct( "obpc", e, def, SIZEOF_STRUCT_OBSERVER_PHYSICS_CONSTANTS, 1, observer_physics_constants_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_observer_physics_constants( observer_physics_constants* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'obpc', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_observer_physics_constants( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_observer_physics_constants( observer_physics_constants* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'obpc', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_observer_physics_constants( def, e );
}
