
#include "lpgr.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 
#include "skelmodel.h"

// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code local_projectile_group_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	// unsigned long flags;
	_4byte,
	
	// file_tag collection_reference_tag;
	// file_tag physics_tag;
	_4byte,
	_4byte,

	// short sequence_indexes[MAXIMUM_NUMBER_OF_LOCAL_PROJECTILE_GROUP_SEQUENCES];
	
		_begin_bs_array, MAXIMUM_NUMBER_OF_LOCAL_PROJECTILE_GROUP_SEQUENCES,
		
		_2byte,
		
		_end_bs_array,
	
	// short_fixed scale_lower_bound, scale_delta;
	// short_fixed animation_rate_lower_bound, animation_rate_delta;
	_2byte, _2byte,
	_2byte, _2byte,
	
	// short target_number_lower_bound, target_number_delta;
	// world_distance radius_lower_bound, radius_delta;
	_2byte, _2byte,
	_4byte, _4byte,
	
	// int expiration_time_lower_bound, expiration_time_delta;
	_4byte, _4byte,

	// short transfer_mode;
	_2byte,
	
	// short_world_distance height_offset;
	_2byte,
	
	// short startup_transition_time_lower_bound, startup_transition_time_delta;
	// short ending_transition_time_lower_bound, ending_transition_time_delta;
	// int duration_lower_bound, duration_delta;
	_2byte, _2byte,
	_2byte, _2byte,
	_4byte, _4byte,
	
	_2byte,	// short_world_distance initial_z_velocity
	_2byte, // short_world_distance z_acceleration
	
	// short startup_delay_lower_bound, startup_delay_delta;
	_2byte, _2byte,
	
	// short unused2[4];
	4*sizeof(short),
	
	// file_tag chain_to_lpgr
	_4byte,
	// file_tag local_light_tag
	_4byte,

	// short unused[14];
	14*sizeof(short),

	// computed during postprocessing
	
	// short collection_reference_index;
	// short collection_index, color_table_index;
	// short physics_index;
	// short chain_to_lpgr_index;
	// short local_light_index;
	6*sizeof(short),
	
	_end_bs_array
};

local_projectile_group_definition default_lpgr =
{
	0,
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	0, 0,
	0, 0,
	0, 0,
	0, 0,
	0, 0,
	0, 0,
	0,
	0,
	0, 0,
	0, 0,
	0, 0,
	0,
	0,
	0, 0,
	0, 0, 0, 0,
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	-1,
	-1, -1,
	-1,
	-1,
	-1
};


// ==============================  EDIT WINDOW  ========================================
void edit_lpgr( tag_entry* e )
{

}


void dependency_lpgr( tag_entry *e )
{
	local_projectile_group_definition def;
	read_local_projectile_group_definition( &def, e );
	tag artType = (def.flags & _group_uses_skelmodel_flag) ? 'skmd' : (def.flags&_group_uses_sprite_flag) ? 'spri' : 'core';
	add_tag_dependency( artType, def.collection_reference_tag );
	add_tag_dependency( 'phys', def.physics_tag );
	add_tag_dependency( 'lpgr', def.chain_to_lpgr );
	add_tag_dependency( 'loli', def.local_light_tag );
}


// ==============================  READ TAG DATA  ========================================
bool read_local_projectile_group_definition( local_projectile_group_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "lpgr", e, def, SIZEOF_STRUCT_LOCAL_PROJECTILE_GROUP_DEFINITION, 1, local_projectile_group_definition_byte_swapping_data);
}


void defaultnew_lpgr( tag_entry *e )
{
	local_projectile_group_definition def;
	byte_swap_move_be( "lpgr", &def, &default_lpgr, sizeof(local_projectile_group_definition), 1, local_projectile_group_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}


// ==============================  SAVE TAG DATA  ========================================
bool save_local_projectile_group_definition( local_projectile_group_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "lpgr", e, def, SIZEOF_STRUCT_LOCAL_PROJECTILE_GROUP_DEFINITION, 1, local_projectile_group_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_local_projectile_group_definition( local_projectile_group_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'lpgr', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_local_projectile_group_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_local_projectile_group_definition( local_projectile_group_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'lpgr', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_local_projectile_group_definition( def, e );
}

