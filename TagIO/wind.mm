
#include "wind.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code wind_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,

//	unsigned long flags;
	_4byte,

//	int time_between_adjustments_lower_bound, time_between_adjustments_delta;
//	fixed maximum_adjustment_per_tick;
	_4byte, _4byte,
	_4byte,

//	fixed_vector3d velocity_lower_bound;
//	fixed_vector3d velocity_delta;
	_4byte, _4byte, _4byte,
	_4byte, _4byte, _4byte,

	//------- postprocessed stuff
//	int ticks_until_wind_changes;
//	fixed_vector3d wind_velocity;
//	fixed_vector3d target_wind;
	sizeof(int),
	2*sizeof(fixed_vector3d),

//	short unused[30];
	30*sizeof(short),
	
	_end_bs_array
};

wind_definition default_wind =
{
	0,
	0, 0,
	0,
	0, 0, 0,
	0, 0, 0,
	0,
	0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

// ==============================  EDIT WINDOW  ========================================
void edit_wind( tag_entry* e )
{

}

void defaultnew_wind( tag_entry *e )
{
	wind_definition def;
	byte_swap_move_be( "wind", &def, &default_wind, sizeof(wind_definition), 1, wind_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_wind( tag_entry *e )
{
    #pragma unused( e )

#if 0 // no dependencies
	wind_definition def;
	read_wind_definition( &def, e );
#endif
}

// ==============================  READ TAG DATA  ========================================
bool read_wind_definition( wind_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "wind", e, def, SIZEOF_STRUCT_WIND_DEFINITION, 1, wind_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_wind_definition( wind_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "wind", e, def, SIZEOF_STRUCT_WIND_DEFINITION, 1, wind_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_wind_definition( wind_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'wind', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_wind_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_wind_definition( wind_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'wind', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_wind_definition( def, e );
}
