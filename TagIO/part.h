// part.h
#ifndef __part_h__
#define __part_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif


#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	PARTICLE_SYSTEMS_GROUP_TAG= 0x70617274, // 'part' (looks like text)
	PARTICLE_SYSTEMS_VERSION_NUMBER= 1
};

/* ---------- structures */

enum // possible snowing states
{
	_not_snowing,
	_snowing,
	NUMBER_OF_SNOWING_STATES
};

enum // particle system flags
{
	_particle_system_states_last_indefinitely_bit,
	_particle_system_does_not_affect_projectiles_bit,
	_particle_system_transparency_varies_with_distance_bit,
	_particle_system_aligns_to_direction_bit,
	NUMBER_OF_PARTICLE_SYSTEM_DEFINITION_FLAGS,
	
	_particle_system_states_last_indefinitely_flag= FLAG(_particle_system_states_last_indefinitely_bit),
	_particle_system_does_not_affect_projectiles_flag= FLAG(_particle_system_does_not_affect_projectiles_bit),
	_particle_system_transparency_varies_with_distance_flag= FLAG(_particle_system_transparency_varies_with_distance_bit),
	_particle_system_aligns_to_direction_flag = FLAG(_particle_system_aligns_to_direction_bit)
};


struct state_variables
{
	short duration_lower_bound, duration_delta;
	fixed_fraction value_lower_bound, value_delta;
	short transition_lower_bound, transition_delta;
};

#define SIZEOF_STRUCT_PARTICLE_SYSTEM 128
struct particle_system_definition
{
	unsigned long flags;

	file_tag sprite_tag;

	world_distance minimum_view_distance; // The closest that particles can get to observer

	world_distance transparency_rolloff_point; // The distance at which the particle starts fading out
	world_distance transparency_cutoff_point; // The distance at which the particle is 100% transparent
	
	short sequence_index;

	short number_of_particles; // Largest number of particles possible on screen
	short maximum_particle_number_delta; // NOT CURRENTLY USED-- Largest change in # of particles possible per tick

	short_fixed scale;
	
	fixed_vector3d velocity_lower_bound;
	fixed_vector3d velocity_delta;
	
	short_fixed x0_particle_number;
	short_fixed x1_particle_number; 
	short_fixed y0_particle_number;
	short_fixed y1_particle_number;
	
	struct state_variables state[NUMBER_OF_SNOWING_STATES];

	file_tag ambient_sound_tag;
	file_tag splash_local_projectile_group_tag;
	
	short box_width;
	short_world_distance box_top_height;
	short_world_distance box_bottom_height;
	
	short max_splashes_per_cell;
	short time_between_building_effects;

	short unused[10];
	
	//------- postprocessed stuff
	short sprite_definition_index;
	short ambient_sound_index;
	short splash_local_projectile_group_type;
};


//=====================================================================================================
bool read_particle_system_definition( particle_system_definition* def, tag id );
bool save_particle_system_definition( particle_system_definition* def, tag id );

bool read_particle_system_definition( particle_system_definition* def, tag_entry* e );
bool save_particle_system_definition( particle_system_definition* def, tag_entry* e );


#endif