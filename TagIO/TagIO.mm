//
//  TagIO.m
//  TagIO
//
//  Created by John Davis on 9/1/15.
//  Copyright (c) 2015 Good Lamb. All rights reserved.
//  This is half a trie and half a NSMutableArray.
//

#import "TagIO.h"

@implementation TagIO
- (tag_entry *)fetchTag:(const char *)type withName:(const char *)name // a mix between a Burst Trie and NSMDicts.
{
    
    
    int current_character = 0;
    tag_type_indices * cursor= self.search_index->narrow[type[current_character]];
    
    while(cursor){
            cursor = cursor->narrow[type[current_character]];
            current_character++;
    }
    NSString *nsname = [NSString stringWithCString:name encoding:NSASCIIStringEncoding];
    if(current_character < 3)// if we don't find a type match
    {
        while(current_character<3)//dig in and add indices
        {
            //cursor = cursor->narrow[type[current_character]];
            cursor = cursor->narrow[type[current_character]] = new tag_type_indices;
            current_character++;
        }
        //then add the bounty and start up dictionary
        cursor->narrow[type[current_character]]->tags_of_type = [[NSMutableDictionary alloc] init];
        [cursor->narrow[type[current_character]]->tags_of_type setObject: (__bridge id)get_entry(*type, *name) forKey:nsname];
    }
    return (__bridge tag_entry *)[cursor->narrow[type[current_character]]->tags_of_type objectForKey: nsname];
    
    
}
//TODO: FREE ALL THIS MEMORY FOOL!
@end
