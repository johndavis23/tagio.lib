// prgr.h
#ifndef __prgr_h__
#define __prgr_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

 


/* ---------- constants */

enum
{
	PROJECTILE_GROUP_DEFINITION_GROUP_TAG= 0x70726772, // 'prgr' (looks like text)
	PROJECTILE_GROUP_DEFINITION_VERSION_NUMBER= 1,

	MAXIMUM_PROJECTILE_GROUP_DEFINITIONS_PER_MAP= 100,

	MAXIMUM_NUMBER_OF_PARTS_PER_PROJECTILE_GROUP= 15
};

/* ---------- projectile group part */

enum // projectile group part definition flags
{
	_projectile_group_part_is_central_bit, // this part is a 1:1 replacement for the original object
	_projectile_group_part_is_on_ground_bit, // this part always begins on the ground, but still chooses a random radius
	
	NUMBER_OF_PROJECTILE_GROUP_PART_DEFINITION_FLAGS,

	_projectile_group_part_is_central_flag= FLAG(_projectile_group_part_is_central_bit),
	_projectile_group_part_is_on_ground_flag= FLAG(_projectile_group_part_is_on_ground_bit)
};

#define SIZEOF_STRUCT_PROJECTILE_GROUP_PART_DEFINITION 32
struct projectile_group_part_definition
{
	file_tag projectile_tag;

	unsigned long flags;
		
	short count_lower_bound, count_delta;
	short_fixed position_lower_bound, position_delta;

	short obsolete[2];
	
	fixed_fraction appearing_fraction; // chance that this part will actually be created
	
	short unused[4];
	
	short projectile_type;
};

/* ---------- projectile group */

enum // projectile group definition flags
{
	NUMBER_OF_PROJECTILE_GROUP_DEFINITION_FLAGS
};

#define SIZEOF_STRUCT_PROJECTILE_GROUP_DEFINITION 512
struct projectile_group_definition
{
	unsigned long flags;
	
	short number_of_parts;
	
	word pad;
	file_tag mesh_effect_tag;
	file_tag sound_tag;
	file_tag local_projectile_group_tag;

	short unused[3];
	
	short local_projectile_group_type;
	short mesh_effect_type;
	short sound_index;
	
	struct projectile_group_part_definition parts[MAXIMUM_NUMBER_OF_PARTS_PER_PROJECTILE_GROUP];
};


//=====================================================================================================
bool read_projectile_group_definition( projectile_group_definition* def, tag id );
bool save_projectile_group_definition( projectile_group_definition* def, tag id );

bool read_projectile_group_definition( projectile_group_definition* def, tag_entry* e );
bool save_projectile_group_definition( projectile_group_definition* def, tag_entry* e );


#endif