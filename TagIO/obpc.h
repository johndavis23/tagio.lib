// obpc.h
#ifndef __obpc_h__
#define __obpc_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	OBSERVER_PHYSICS_CONSTANTS_GROUP_TAG= 0x6f627063, // 'obpc' (looks like text)
	OBSERVER_PHYSICS_CONSTANTS_VERSION= 1
};

/* ---------- structures */

struct half_observer_physics_constant
{
	fixed maximum_velocity, acceleration, deceleration;
};

struct observer_physics_constant
{
	struct half_observer_physics_constant negative, positive;
};

#define SIZEOF_STRUCT_OBSERVER_PHYSICS_CONSTANTS 256
struct observer_physics_constants
{
	struct observer_physics_constant parallel_velocity, orthogonal_velocity;
	struct observer_physics_constant yaw_velocity, orbit_velocity;
	struct observer_physics_constant vertical_velocity;
	struct observer_physics_constant pitch_velocity;
	struct observer_physics_constant zoom_velocity;
	
	short unused[44];
};


//=====================================================================================================
bool read_observer_physics_constants( observer_physics_constants* def, tag id );
bool save_observer_physics_constants( observer_physics_constants* def, tag id );

bool read_observer_physics_constants( observer_physics_constants* def, tag_entry* e );
bool save_observer_physics_constants( observer_physics_constants* def, tag_entry* e );

#endif