#include "tags.h"
#include "tagprivate.h"

#include "text.h"
 
#include "tagpick.h"
#include <assert.h>
#include <string.h>



// ==============================  BYTE SWAPPING DATA  ========================================


// ==============================  EDIT WINDOW  ========================================
void edit_text( tag_entry* e )
{
}

void defaultnew_text( tag_entry *e )
{
    #pragma unused( e )

	// nop
}


// ==============================  READ TAG DATA  ========================================
bool read_string_definition( string_definition* def, tag_entry* e )
{
	// text data is saved in Mac format (\r only), yaaf likes \n for it's text box display
	const void *data;
	int ii;
	
	if( e->length )
	{
		if( get_entry_data( e, &data, (unsigned *)&def->string_length ) )
		{
			memcpy( def->string_buffer, data, def->string_length );
			def->string_buffer[def->string_length] = '\0'; 
		
			// happy fun text formatting
			for( ii = 0; ii < def->string_length; ii++ )
			{
				if( def->string_buffer[ii] == '\r' )
					def->string_buffer[ii] = '\n';
			}
			
			return TRUE;
		}
	}
	else
	{
		def->string_length = 0;
		def->string_buffer[0] = '\0';
		return TRUE;
	}
	
	return FALSE;
}


// ==============================  SAVE TAG DATA  ========================================
bool save_string_definition( string_definition* def, tag_entry* e )
{
	// text data is saved in Mac format (\r only), yaaf likes \n for it's text box display
	int ii;
	
	// happy fun text formatting
	for( ii = 0; ii < def->string_length; ii++ )
	{
		if( def->string_buffer[ii] == '\n' )
			def->string_buffer[ii] = '\r';
	}
	
	set_entry_data( e, def->string_buffer, def->string_length, FALSE );

	// I have to assume that set_entry_data worked	
	return TRUE;
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_string_definition( string_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'text', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_string_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_string_definition( string_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'text', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_string_definition( def, e );
}



