//
//  TagIO.h
//  TagIO
//
//  Created by John Davis on 9/1/15.
//  Copyright (c) 2015 Good Lamb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "tags.h"
enum tag_index_types
{ mesh_index, txst_index, count_tag_index_types };

struct binary_node
{
    void * left;
    void * right;
    tag_entry * stash;
};
struct tag_type_indices
{
    tag_type_indices * narrow[sizeof(char)];
    NSMutableDictionary * tags_of_type;
};
@interface TagIO : NSObject
@property (nonatomic) tag_type_indices* search_index;
- (tag_entry *)fetchTag:(const char *)type withName:(const char *)name;
@end

