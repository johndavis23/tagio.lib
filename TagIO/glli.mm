
#include "glli.h"


#include <assert.h>
#include <math.h>

// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code glli_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	//	struct rgb_color ambient_color;
	_2byte, _2byte, _2byte, _2byte,
	
	//	struct rgb_color sky_color;
	_2byte, _2byte, _2byte, _2byte,
	
	//	struct rgb_color key_color;
	_2byte, _2byte, _2byte, _2byte,
	
	//	struct rgb_color fill_color;
	_2byte, _2byte, _2byte, _2byte,
	
	//	fixed_vector3d key_incident_direction;
	_4byte, _4byte, _4byte,
	
	//	fixed_vector3d key_incident_direction;
	_4byte, _4byte, _4byte,
	
	_end_bs_array
};


// ==============================  EDIT WINDOW  ========================================
void edit_glli( tag_entry* e )
{
}


static glli_definition default_glli =
{
	{ 0x4000, 0x4000, 0x4000, 0 },
	{ 0x2000, 0x2000, 0x4000, 0 },
	{ 0x9fff, 0x9fff, 0x5fff, 0 },
	{ 0x4000, 0x3000, 0x2000, 0 },
	
	{ FLOAT_TO_FIXED(0.3), FLOAT_TO_FIXED(0.1), FLOAT_TO_FIXED(-0.4) },
	{ FLOAT_TO_FIXED(-1.0f), FLOAT_TO_FIXED(1.0f), FLOAT_TO_FIXED(-0.2f) }
};


void defaultnew_glli( tag_entry* e )
{
	glli_definition def;
	byte_swap_move_be( "glli", &def, &default_glli, sizeof(glli_definition), 1, glli_definition_byte_swapping_data);
	set_entry_data( e, &def, sizeof(def), false );
}


void dependency_glli( tag_entry *e )
{
    #pragma unused( e )

#if 0 // no dependencies
	glli_definition def;
	read_glli_definition( &def, e );
#endif
}


// ==============================  READ TAG DATA  ========================================
bool read_glli_definition( glli_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "glli", e, def, sizeof(glli_definition), 1, glli_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_glli_definition( glli_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "glli", e, def, sizeof(glli_definition), 1, glli_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_glli_definition( glli_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'glli', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_glli_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_glli_definition( glli_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'glli', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_glli_definition( def, e );
}



