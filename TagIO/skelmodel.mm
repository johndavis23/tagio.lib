

#ifndef __skelmodel_h__
#include "skelmodel.h"
#endif

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __read_skl_h__
#include "read_skl.h"
#endif

#ifndef __fvec_c1_h__
#include "vec/fvec_c1.h"
#endif

#include "memalign.h"
#include "tagpick.h"
#include "formatters.h"

#include <ctype.h>
#include "vec3.h"

#include <stdlib.h>
#include <string.h>

template<class A> struct staticInit { operator A*() { return (A*)malloc( sizeof( A ) );  }; }; // Build 100 - PAB - Meggs didn't have this ; I don't know the history here
#if 1

#define assert(A) ((void)0)

#else
#define enter_debugger() __asm {int 3}
#define assert(expr) if (!(expr)) { enter_debugger() }

#endif


byte_swap_code bs_skmd_tag_header[] =
{
	_begin_bs_array, 1,
	
	// tag animation
	_4byte,
	
	// unsigned animation_checksum
	_4byte,
	
	// word modelcount
	_2byte,
	
	// word flags
	_2byte,
	
	_begin_bs_array, MAX_SKMD_MODELS,
		
		// unsigned offset
		_4byte,
		
		// unsigned length
		_4byte,
		
		// word flags
		_2byte,
		
		// word probability
		_2byte,
		
		// model pointer
		4,
		
	_end_bs_array,
	
	_end_bs_array
};


byte_swap_code bs_skmd_lodref[] =	// Build 100 - PAB - Import from Meggs
{
	_begin_bs_array, 1,
		_2byte,		// word min_vertices;
		_2byte,		// word max_vertices;
		_4byte,		// unsigned offset;	// from skmd_modelheader start
		_4byte,		// unsigned length;
		4,			// const struct skmd_lod *data;	// used when loaded
	_end_bs_array
};	// Build 100 - PAB - End



byte_swap_code bs_skmd_modelheader[] =
{
	_begin_bs_array, 1,
	
	MAX_SKMD_MODELNAME+1,	//	char name[MAX_SKMD_MODELNAME+1];
	_4byte,	//	tag texture_stack;
	_2byte, //	word hard_min_vertices;
	_2byte, //	word user_min_vertices;
	_2byte, //	word max_vertices;
	_2byte, //	word lod_count;
	_2byte, //	word bone_count;
	_2byte, //	word bone_fixed_shift;
	_2byte, //	word anim_offset_scale;
	_2byte, //	word anim_offset_shift;
	_4byte, //	float model_scale;
	_4byte, //	world_distance collision_radius;
	_4byte, //	world_distance collision_height0;
	_4byte, //	world_distance collision_height1;
	_4byte, //	world_distance pick_radius;
	_4byte, //	world_distance pick_height0;
	_4byte, //	world_distance pick_height1;
	_4byte, //	world_distance hilight_radius;
	_4byte, //	world_distance hilight_height0;
	_4byte, //	world_distance hilight_height1;
	_2byte, //	word complex_collision_tri_count;
	_2byte, //	word complex_collision_box_count;
	
	62, //	byte padding[62];
	
	2, // short loaded_texturestacks_index;
	
	4, //	float (*uv)[2];	// internal pointer used when loaded
	4, //	struct skmd_complex_collision_box *complex_collision_boxes;	// internal pointer used when loaded
	
	// Build 100 - PAB - Import from Meggs
	// This bs data is missing the following two items:	
	// 4, // struct skmd_complex_collision_tri *complex_collision_tris;
	// 4, // word *real_vertex_counts;
	// Sadly, all our model files were saved on the PC using this incorrect byte swapping.
	// We have to do work in import_skmd_modelheader to fix it on the Mac.
	
	//	skmd_lodref lods[MAX_SKMD_PREMADELOD];
	_begin_bs_array, MAX_SKMD_PREMADELOD,		// skmd_lodref lods[MAX_SKMD_PREMADELOD];
		_2byte,		// word min_vertices;
		_2byte,		// word max_vertices;
		_4byte,		// unsigned offset;	// from skmd_modelheader start
		_4byte,		// unsigned length;
		4,			// const struct skmd_lod *data;	// used when loaded
	_end_bs_array,
	
	// then the bones
	// align-to-16
	// then the uv
	// then the collision
	_end_bs_array
};

byte_swap_code bs_skmd_bone[] = 
{
	_begin_bs_array, 1,
	
	MAX_SKMD_BONENAME+1,	//	char name[MAX_SKMD_BONENAME+1];
	_4byte,	//	unsigned flags;
		
	_2byte,	//	signed short parent;	// can be -1 == no parent
	2,		//	signed short anim_bone; // can be -1 == none; run-time
	
	_4byte, _4byte, _4byte, _4byte,	//	float float_orient[4];
	_4byte, _4byte, _4byte,	//	float float_offset[3];
		
	_2byte, _2byte, _2byte, _2byte, //	signed short fixed_orient[4];
	_2byte, _2byte, _2byte,	//	signed short fixed_offset[3];
	
	_2byte,	//	signed short attachment_index; // run-time
	_4byte,	//	tag attachment_id;
	
	_begin_bs_array, 3*2,	// float bounds[3][2]
		_4byte,
	_end_bs_array,
	
	_4byte, //	float shadow_near;
	_4byte, //	float shadow_far;
	_4byte, //	float shadow_width;
		
	_4byte, //	tag custom_shadow_tag;
	_byte,	//	byte custom_shadow_stack;
	_byte,	//	byte custom_shadow_level;
	2,		//	short shadow_texture_entry_index;
		
	_4byte, //	float tree_mass;
	_4byte,	//	float tree_resistance;
	_4byte,	//	float tree_dampening;
	
	_end_bs_array
};

byte_swap_code bs_skmd_complex_collision_tri[] =
{
	_begin_bs_array, 1,
	
	_2byte, _2byte, _2byte,
	_2byte, _2byte, _2byte,
	_2byte, _2byte, _2byte,
	_2byte, _2byte, _2byte,
	
	_4byte,
	_4byte,
	_4byte,
	_4byte,
	
	_end_bs_array
};

byte_swap_code bs_skmd_complex_collision_box[] =
{
	_begin_bs_array, 1,
	
	//	short_world_rectangle3d bounds;
 	_2byte, _2byte, _2byte, _2byte, _2byte, _2byte,
 	
	//	short sub_box_first_index, sub_box_count;
	_2byte, _2byte,
	
	//	short triangle_first_index, triangle_count;
	_2byte, _2byte,
	
	_end_bs_array
};

#define ALIGN16(p) ((((size_t)(p)) + 15) & ~15)


inline void MatMult( float out[3][3], const float a[3][3], const float b[3][3] )
{
	float temp[3][3];
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			temp[i][j] = a[i][0] * b[0][j] + a[i][1] * b[1][j] + a[i][2] * b[2][j];
		}
	}
	memcpy( out, temp, sizeof(temp) );
}


unsigned setup_skmd_modelheader_ptrs( skmd_modelheader *mh )
{
	assert( mh == (skmd_modelheader *)ALIGN16(mh) );
	for (int i = 0; i < mh->lod_count; ++i) {
		mh->lods[i].data = (skmd_lod *)((byte *)mh + mh->lods[i].offset);
	}
	mh->uv = (float (*)[2])(ALIGN16( &mh->bones[mh->bone_count] ));
	mh->complex_collision_boxes = (struct skmd_complex_collision_box *)(mh->uv + mh->max_vertices);
	mh->complex_collision_tris = (struct skmd_complex_collision_tri *)(mh->complex_collision_boxes + mh->complex_collision_box_count);
	mh->real_vertex_counts = (word *)(mh->complex_collision_tris + mh->complex_collision_tri_count) - mh->hard_min_vertices;
	return ((size_t)(mh->real_vertex_counts + mh->max_vertices+1) - (size_t)mh);
}


unsigned import_skmd_modelheader( skmd_modelheader *mh )	// returns length
{
	assert( mh == (skmd_modelheader *)ALIGN16(mh) );
	
	byte_swap_data_be( "skmd_modelheader", mh, sizeof(mh) - sizeof(skmd_bone), 1, bs_skmd_modelheader );
	// Build 100 - PAB - Import from Meggs
	// now we clean up the wrongness with the following two swaps....see note in bs_skmd_modelheader struct
	byte_swap_data_le( "skmd_lodref fixup 1", ((byte *)(mh->lods)) - 8, sizeof(skmd_lodref), MAX_SKMD_PREMADELOD, bs_skmd_lodref );
	byte_swap_data_le( "skmd_lodref fixup 2", mh->lods, sizeof(skmd_lodref), MAX_SKMD_PREMADELOD, bs_skmd_lodref );
	// Build 100 - PAB - End
	byte_swap_data_be( "skmd_bone", mh->bones, sizeof(skmd_bone), mh->bone_count, bs_skmd_bone );
	unsigned len = setup_skmd_modelheader_ptrs(mh);
	byte_swap_memory_be( "uv coords", (void *)mh->uv, mh->max_vertices * 2, _4byte );
	byte_swap_data_be( "complex collision boxes", mh->complex_collision_boxes, sizeof(skmd_complex_collision_box), mh->complex_collision_box_count, bs_skmd_complex_collision_box );
	byte_swap_data_be( "complex collision tris", mh->complex_collision_tris, sizeof(skmd_complex_collision_tri), mh->complex_collision_tri_count, bs_skmd_complex_collision_tri );
	byte_swap_memory_be( "real vertex count", mh->real_vertex_counts + mh->hard_min_vertices, mh->max_vertices+1 - mh->hard_min_vertices, _2byte );
	return len;
}


unsigned export_skmd_modelheader( skmd_modelheader *mh )
{
	int i, j;
	
	assert( mh == (skmd_modelheader *)ALIGN16(mh) );
	setup_skmd_modelheader_ptrs( mh );
	unsigned size = ((size_t)(mh->real_vertex_counts + mh->max_vertices+1 - mh->hard_min_vertices)) - ((size_t)mh);
	
	byte_swap_memory_be( "uv coords", (void *)mh->uv, mh->max_vertices * 2, _4byte );
	mh->uv = 0;
	
	byte_swap_data_be( "complex collision boxes", mh->complex_collision_boxes, sizeof(skmd_complex_collision_box), mh->complex_collision_box_count, bs_skmd_complex_collision_box );
	mh->complex_collision_boxes = 0;
	
	byte_swap_data_be( "complex collision tris", mh->complex_collision_tris, sizeof(skmd_complex_collision_tri), mh->complex_collision_tri_count, bs_skmd_complex_collision_tri );
	mh->complex_collision_tris = 0;
	
	byte_swap_memory_be( "real vertex count", mh->real_vertex_counts + mh->hard_min_vertices, mh->max_vertices+1 - mh->hard_min_vertices, _2byte );
	mh->real_vertex_counts = 0;
	
	for (i = 0; i < mh->lod_count; ++i) {
		mh->lods[i].data = 0;
	}
	
	float biggest = 0.00001f;
	for (i = 0; i < mh->bone_count; ++i)
	{
		for (j = 0; j < 3; ++j)
		{
			float offset = fabs(mh->bones[i].float_offset[j] * mh->model_scale);
			if (offset > biggest) biggest = offset;
		}
	}
	mh->bone_fixed_shift = 0;
	float scale = 1.0f;
	while (biggest * scale < 16383.0)
	{
		scale *= 2.0f;
		mh->bone_fixed_shift += 1;
	}
	for (i = 0; i < mh->bone_count; ++i)
	{
		for (j = 0; j < 3; ++j)
		{
			mh->bones[i].fixed_offset[j] = float_to_clamped_short( mh->model_scale * mh->bones[i].float_offset[j] * scale );
		}
	}
	
	scale = mh->model_scale;
	mh->anim_offset_shift = 0;
	while (fabs(scale) < 16384.0)
	{
		scale *= 2.0f;
		mh->anim_offset_shift += 1;
	}
	mh->anim_offset_scale = float_to_clamped_short( scale );
	
	byte_swap_data_be( "skmd_bone", mh->bones, sizeof(skmd_bone), mh->bone_count, bs_skmd_bone );
	byte_swap_data_be( "skmd_modelheader", mh, sizeof(mh) - sizeof(skmd_bone), 1, bs_skmd_modelheader );
	
	return size;
}


unsigned setup_skmd_lod_ptrs( int bone_count, int min_vertex, int max_vertex, skmd_lod *lod )
{
	assert( lod == (skmd_lod *)ALIGN16(lod) );
	
	word *weights_per_bone = (word *)(&lod->triangles[lod->max_triangles]);
	word *edge_collapse = (word *)(&weights_per_bone[bone_count]) - min_vertex;
	word *min_vertices_for_tri = (word *)(&edge_collapse[max_vertex]);
	word *weight_refs = (word *)(&min_vertices_for_tri[lod->max_triangles]);
	byte *weights_per_vertex = (byte *)(&weight_refs[lod->weight_ref_count]);
	float (*weights)[4] = (float (*)[4])ALIGN16(&weights_per_vertex[max_vertex]);
	
	lod->weights_per_bone = weights_per_bone;
	lod->edge_collapse = edge_collapse;
	lod->min_vertices_for_tri = min_vertices_for_tri;
	lod->weight_refs = weight_refs;
	lod->weights_per_vertex = weights_per_vertex;
	lod->weights = weights;
	
	return (size_t)(&weights[lod->weighted_vert_count]) - (size_t)lod;
}


unsigned import_skmd_lod( int bone_count, int min_vertex, int max_vertex, skmd_lod *lod )
{
	assert( lod == (skmd_lod *)ALIGN16(lod) );
	
	byte_swap_memory_be( "lod_header", lod, 6, _2byte );
	unsigned size = setup_skmd_lod_ptrs( bone_count, min_vertex, max_vertex, lod );
	
	byte_swap_memory_be( "weights_per_bone", (void *)lod->weights_per_bone, bone_count, _2byte );
	byte_swap_memory_be( "edge_collapse", (void *)(lod->edge_collapse + min_vertex), max_vertex - min_vertex, _2byte );
	byte_swap_memory_be( "min_vertices_for_tri", (void *)(lod->min_vertices_for_tri), lod->max_triangles, _2byte );
	byte_swap_memory_be( "weight_refs", (void *)lod->weight_refs, lod->weight_ref_count, _2byte );
	// don't swap single-byte weight_count
	byte_swap_memory_be( "skmd_quadvert", (void *)lod->weights, lod->weighted_vert_count * 4, _4byte );
	byte_swap_memory_be( "triangles", (void *)lod->triangles, lod->max_triangles * 3, _2byte );
	
	return size;
}


unsigned export_skmd_lod( int bone_count, int min_vertex, int max_vertex, skmd_lod *lod )
{
	assert( lod == (skmd_lod *)ALIGN16(lod) );
	setup_skmd_lod_ptrs( bone_count, min_vertex, max_vertex, lod );
	
	unsigned size = (size_t)(lod->weights + lod->weighted_vert_count) - (size_t)lod;
	
	byte_swap_memory_be( "weights_per_bone", (void *)lod->weights_per_bone, bone_count, _2byte );
	byte_swap_memory_be( "edge_collapse", (void *)(lod->edge_collapse + min_vertex), max_vertex - min_vertex, _2byte );
	byte_swap_memory_be( "min_vertices_for_tri", (void *)(lod->min_vertices_for_tri), lod->max_triangles, _2byte );
	byte_swap_memory_be( "weight_refs", (void *)lod->weight_refs, lod->weight_ref_count, _2byte );
	// don't swap single-byte weight_count
	byte_swap_memory_be( "skmd_quadvert", (void *)lod->weights, lod->weighted_vert_count * 4, _4byte );
	byte_swap_memory_be( "triangles", (void *)lod->triangles, lod->max_triangles * 3, _2byte );
	
	lod->weights_per_bone = 0;
	lod->edge_collapse = 0;
	lod->min_vertices_for_tri = 0;
	lod->weight_refs = 0;
	lod->weights_per_vertex = 0;
	lod->weights = 0;
	
	byte_swap_memory_be( "lod_header", lod, 6, _2byte );
	
	return size;
}


bool bounds_to_shadow( skmd_bone &b, const float bounds[3][2] )
{
	b.flags &= ~_skmd_bone_shadow_dimension_mask;
	b.flags |= _skmd_bone_shadow_dimension_none << _skmd_bone_shadow_dimension_bit_0;
	if (bounds[0][0] >= bounds[0][1]) return false;
	if (bounds[1][0] >= bounds[1][1]) return false;
	if (bounds[2][0] >= bounds[2][1]) return false;
	
	int best_dim = -1;
	float best_rad;
	float low_err = 100000000000.0f;
	for (int j = 0; j < 3; ++j) {
		int d1 = j + 1;
		if (d1 >= 3) d1 -= 3;
		int d2 = d1 + 1;
		if (d2 >= 3) d2 -= 3;
		float rad = 0.25f * (bounds[d1][1] - bounds[d1][0] +
							bounds[d2][1] - bounds[d2][0]);
		float err = (fabs(bounds[d1][1] - rad) +
					fabs(bounds[d1][0] + rad) +
					fabs(bounds[d2][1] - rad) +
					fabs(bounds[d2][0] + rad)) / rad;
		if (err < low_err) {
			low_err = err;
			best_dim = j;
		#if 0
			float test;
			rad = fabs(bounds[d1][1]);
			test = fabs(bounds[d1][0]);
			if (test > rad) rad = test;
			test = fabs(bounds[d2][1]);
			if (test > rad) rad = test;
			test = fabs(bounds[d2][0]);
			if (test > rad) rad = test;
		#endif
			best_rad = rad;
		}
	}
	if (best_dim >= 0) {
		b.flags &= ~_skmd_bone_shadow_dimension_mask;
		b.flags |= best_dim << _skmd_bone_shadow_dimension_bit_0;
		float length = 0.6f * (bounds[best_dim][1] - bounds[best_dim][0]);
		float mid_length = 0.5f * (bounds[best_dim][0] + bounds[best_dim][1]);
		b.shadow_near = mid_length - length;//build_info.bounds[best_dim][0];
		b.shadow_far = mid_length + length;//build_info.bounds[best_dim][1];
		b.shadow_width = best_rad;
	}
	
	return true;
}


void add_default_biped_shadows( skmd_modelheader *mh )
{
	static const char *names[] = {
		"Spine",
		"Neck",
		"UpperArm",
		"Forearm",
		"Thigh",
		"Calf",
		"Foot",
		"Head",
		"Hand",
		0
	};
	
	for (int i = 0; i < mh->bone_count; ++i) {
		struct skmd_bone &b = mh->bones[i];
		int j = 0;
		while (names[j]) {
			if (strstr( b.name, names[j] )) {
				if (bounds_to_shadow( b, b.bounds ))
				{
					b.flags |= _skmd_bone_shadowgen_enabled_flag;
				}
				break;
			}
			++j;
		}
	}
}




#pragma mark -


const skmd_lodref *pick_skmd_lod( const skmd_modelheader *m, int *vertices )
{
	int v = *vertices;
	if (v > m->max_vertices) v = m->max_vertices; else if (v < m->hard_min_vertices) v = m->hard_min_vertices;
	v = m->real_vertex_counts[v];
	const skmd_lodref *bestlod = 0;
	int besterror = 2000000;
	int bestv = v;
	for (int i = 0; i < m->lod_count; ++i) {
		int error;
		if (v > m->lods[i].max_vertices) {
			error = 1000000 + (v - m->lods[i].max_vertices);
			if (error < besterror) {
				bestlod = m->lods + i;
				besterror = error;
				bestv = m->lods[i].max_vertices;
			}
		} else if (v < m->lods[i].min_vertices) {
			error = 1000000 + (m->lods[i].min_vertices - v);
			if (error < besterror) {
				bestlod = m->lods + i;
				besterror = error;
				bestv = m->lods[i].min_vertices;
			}
		} else {
			error = m->lods[i].max_vertices - v;
			if (error < besterror) {
				bestlod = m->lods + i;
				besterror = error;
				bestv = v;
			}
		}
	}
	
	*vertices = bestv;
	return bestlod;
}


void generate_skel_mesh( const object_bone *bones, int bone_count,
						const skmd_lodref *ref, int target_vert_count,
						void *out_vert, void *out_norm,
						unsigned short *out_tris, int *out_tri_count )
{
	int i, j, k;
	static fvec vert_temp[3][8000/FVEC_WIDTH];
	static fvec vert2_temp[8000 << (2 - FVEC_WIDTH_SHIFT)];
	
	const skmd_lod *lod = ref->data;
	
	// transform the offsets and normals and save in SoA format
	int qv_pos = 0;
	for (i = 0; i < bone_count; ++i) {
		int bone_vert = lod->weights_per_bone[i];
		if (!bone_vert) continue;
		int fv_rounded_verts = (bone_vert + FVEC_WIDTH - 1) >> FVEC_WIDTH_SHIFT;
		int qv_rounded_verts = (bone_vert + 3) & ~3;
		for (j = 0; j < 3; ++j) {
			fvec off = fv_loadf(bones[i].world_pos[j]);
			fvec mx = fv_loadf(bones[i].m[j][0]);
			fvec my = fv_loadf(bones[i].m[j][1]);
			fvec mz = fv_loadf(bones[i].m[j][2]);
			fvec *v_out = vert_temp[j] + qv_pos;
			const fvec *in = (const fvec *)(lod->weights[qv_pos]);
			for (k = 0; k < fv_rounded_verts; ++k) {
				fvec vert = fv_mul( in[0], mx );
				vert = fv_muladd( vert, in[1], my );
				vert = fv_muladd( vert, in[2], mz );
				*v_out++ = fv_muladd( vert, in[3], off );
				in += 4;
			}
		}
		qv_pos += qv_rounded_verts;
	}
	
	// put the offsets into AoS format
	for (i = 0; i < (lod->weighted_vert_count >> FVEC_WIDTH_SHIFT); ++i) {
		fv_merge3pad_tomem( (float *)(vert2_temp + (i << 2)), vert_temp[0][i], vert_temp[1][i], vert_temp[2][i] );
	}
	
	// sum the vertex weights
	int w_step = 0;
	fvec *out_pos = (fvec *)(out_vert);
	fvec *out_n = (fvec *)(out_norm);
	for (i = 0; i < target_vert_count; ++i) {
		int w_count = lod->weights_per_vertex[i];
		{
			int v_pos = (lod->weight_refs[w_step]) << (2 - FVEC_WIDTH_SHIFT);
			fvec v0 = vert2_temp[v_pos];
			#if FVEC_WIDTH < 4
			fvec v1 = vert2_temp[v_pos+1];
			#endif
			#if FVEC_WIDTH < 2
			fvec v2 = vert2_temp[v_pos+2];
			#endif
			
			++w_step;
			for (j = 1; j < w_count; ++j) {
				v_pos = (lod->weight_refs[w_step]) << (2 - FVEC_WIDTH_SHIFT);
				v0 = fv_add( v0, vert2_temp[v_pos] );
				#if FVEC_WIDTH < 4
				v1 = fv_add( v1, vert2_temp[v_pos+1] );
				#endif
				#if FVEC_WIDTH < 2
				v2 = fv_add( v2, vert2_temp[v_pos+2] );
				#endif
				
				++w_step;
			}
			out_pos[0] = v0;
			#if FVEC_WIDTH < 4
			out_pos[1] = v1;
			#endif
			#if FVEC_WIDTH < 2
			out_pos[2] = v2;
			#endif
			out_pos += 4 >> FVEC_WIDTH_SHIFT;
		}
		{
			int n_pos = (lod->weight_refs[w_step]) << (2 - FVEC_WIDTH_SHIFT);
			fvec n0 = vert2_temp[n_pos];
			#if FVEC_WIDTH < 4
			fvec n1 = vert2_temp[n_pos+1];
			#endif
			#if FVEC_WIDTH < 2
			fvec n2 = vert2_temp[n_pos+2];
			#endif
			
			++w_step;
			for (j = 1; j < w_count; ++j) {
				n_pos = (lod->weight_refs[w_step]) << (2 - FVEC_WIDTH_SHIFT);
				n0 = fv_add( n0, vert2_temp[n_pos] );
				#if FVEC_WIDTH < 4
				n1 = fv_add( n1, vert2_temp[n_pos+1] );
				#endif
				#if FVEC_WIDTH < 2
				n2 = fv_add( n2, vert2_temp[n_pos+2] );
				#endif
				
				++w_step;
			}
			out_n[0] = n0;
			#if FVEC_WIDTH < 4
			out_n[1] = n1;
			#endif
			#if FVEC_WIDTH < 2
			out_n[2] = n2;
			#endif
			out_n += 4 >> FVEC_WIDTH_SHIFT;
		}
	}
	
	// output triangles
	static unsigned short collapse[4000];
	for (i = 0; i < target_vert_count; ++i) {
		collapse[i] = i;
	}
	for (i = target_vert_count; i < ref->max_vertices; ++i) {
		collapse[i] = collapse[lod->edge_collapse[i]];
	}
	int max_tri_count = lod->max_triangles;
	int tri_count = 0;
	for (i = 0; i < max_tri_count; ++i) {
		if (lod->min_vertices_for_tri[i] > target_vert_count) continue;
		out_tris[0] = collapse[lod->triangles[i][0]];
		out_tris[1] = collapse[lod->triangles[i][1]];
		out_tris[2] = collapse[lod->triangles[i][2]];
		out_tris += 3;
		++tri_count;
	}
	*out_tri_count = tri_count;
}


static float get_tri_vert_list_bounds( const vec3 (*v)[3], int count, float out[3][2] )
{
	int i, j, k;
	if (count < 1)
	{
		for (i = 0; i < 3; ++i) {
			for (j = 0; j < 2; ++j) {
				out[i][j] = 0.0f;
			}
		}
		return - 1.0f;
	}
	
	for (i = 0; i < 3; ++i) {
		for (j = 0; j < 2; ++j) {
			out[i][j] = v[0][0][i];
		}
	}
	
	for (i = 0; i < count; ++i)
	{
		for (j = 0; j < 3; ++j)
		{
			const vec3 &p = v[i][j];
			for (k = 0; k < 3; ++k)
			{
				if (p[k] < out[k][0]) out[k][0] = p[k];
				if (p[k] > out[k][1]) out[k][1] = p[k];
			}
		}
	}
	
	return (out[0][1] - out[0][0]) * (out[1][1] - out[1][0]) * (out[2][1] - out[2][0]);
}


static void split_tri_vert_list( const vec3 (*in)[3], int in_count,
								vec3 (**out_a)[3], int *out_count_a,
								vec3 (**out_b)[3], int *out_count_b,
								vec3 plane_normal, float plane_const )
{
	int i;
	
	vec3 (*a)[3] = new vec3[in_count*2][3];
	int a_count = 0;
	vec3 (*b)[3] = new vec3[in_count*2][3];
	int b_count = 0;
	
	for (i = 0; i < in_count; ++i)
	{
		vec3 pt = (in[i][0] + in[i][1] + in[i][2]) / 3.0f;
		if (dot( plane_normal, pt ) + plane_const < 0.0f)
		{
			a[a_count][0] = in[i][0];
			a[a_count][1] = in[i][1];
			a[a_count][2] = in[i][2];
			++a_count;
		}
		else
		{
			b[b_count][0] = in[i][0];
			b[b_count][1] = in[i][1];
			b[b_count][2] = in[i][2];
			++b_count;
		}
	}
	
	*out_a = a;
	*out_b = b;
	*out_count_a = a_count;
	*out_count_b = b_count;
}

#define MAX_COLLISION_BOXES 4000
#define MAX_COLLISION_TRIS 4000

static int collision_box_count;
//static skmd_complex_collision_box collision_boxes[MAX_COLLISION_BOXES];	// Build 100 - PAB - Meggs used this version; I don't know the history here
typedef skmd_complex_collision_box Tcollision_boxes[MAX_COLLISION_BOXES];
static Tcollision_boxes& collision_boxes = *staticInit<Tcollision_boxes>();

static int collision_tri_count;

//static skmd_complex_collision_tri collision_tris[MAX_COLLISION_TRIS];	// Build 100 - PAB - Meggs used this version; I don't know the history here
typedef skmd_complex_collision_tri Tcollision_tris[MAX_COLLISION_TRIS];
static Tcollision_tris& collision_tris = *staticInit<Tcollision_tris>();


#define MINIMUM_COLLISION_BOX_SIZE 0.25f
#define MINIMUM_TRIS_IN_COLLISION_BOX 2

static void out_plane( skmd_complex_collision_tri *tri, int plane, vec3 norm, vec3 pointa, vec3 pointb, vec3 pointc, const vec3 *assert_front )
{
	norm = normalized( norm ) * TRIG_MAGNITUDE;
	tri->normals[plane][0] = float_to_clamped_short( norm.x );
	tri->normals[plane][1] = float_to_clamped_short( norm.y );
	tri->normals[plane][2] = float_to_clamped_short( norm.z );
	double d0 = tri->normals[plane][0] * pointa.x + tri->normals[plane][1] * pointa.y + tri->normals[plane][2] * pointa.z;
	double d1 = tri->normals[plane][0] * pointb.x + tri->normals[plane][1] * pointb.y + tri->normals[plane][2] * pointb.z;
	double d2 = tri->normals[plane][0] * pointc.x + tri->normals[plane][1] * pointc.y + tri->normals[plane][2] * pointc.z;
	double d = d0;
	if (d1 < d) d = d1;
	if (d2 < d) d = d2;
	tri->constants[plane] = (int)floor( WORLD_ONE * -d );
	if (assert_front)
	{
		int x = FLOAT_TO_WORLD( assert_front->x );
		int y = FLOAT_TO_WORLD( assert_front->y );
		int z = FLOAT_TO_WORLD( assert_front->z );
		int test = tri->normals[plane][0] * x + tri->normals[plane][1] * y + tri->normals[plane][2] * z;
		assert( test + tri->constants[plane] > 0 );
	}
}

void recursive_build_skelmodel_collision( int box_index, const vec3 (*in)[3], int in_count, const float bounds[3][2], float volume )
{
	skmd_complex_collision_box &box = collision_boxes[box_index];
	box.bounds.minimum_corner.x = (int)floor(bounds[0][0] * WORLD_ONE);
	box.bounds.minimum_corner.y = (int)floor(bounds[1][0] * WORLD_ONE);
	box.bounds.minimum_corner.z = (int)floor(bounds[2][0] * WORLD_ONE);
	box.bounds.maximum_corner.x = (int)ceil(bounds[0][1] * WORLD_ONE);
	box.bounds.maximum_corner.y = (int)ceil(bounds[1][1] * WORLD_ONE);
	box.bounds.maximum_corner.z = (int)ceil(bounds[2][1] * WORLD_ONE);
	
	int best_axis = -1;
	vec3 best_plane_normal;
	float best_plane_const;
	int best_count = (in_count - MINIMUM_TRIS_IN_COLLISION_BOX) * (in_count - MINIMUM_TRIS_IN_COLLISION_BOX) + MINIMUM_TRIS_IN_COLLISION_BOX * MINIMUM_TRIS_IN_COLLISION_BOX;
	vec3 (*a_list)[3], (*b_list)[3];
	int a_count, b_count;
	float a_vol, b_vol, a_bounds[3][2], b_bounds[3][2];
	if (in_count > MINIMUM_TRIS_IN_COLLISION_BOX)
	{
		for (int test_axis = 0; test_axis < 3; ++test_axis)
		{
			float len = bounds[test_axis][1] - bounds[test_axis][0];
			int subdivisions = (int)floor(len / MINIMUM_COLLISION_BOX_SIZE);
			if (subdivisions > 10) subdivisions = 10;
			vec3 plane_norm(0,0,0);
			plane_norm[test_axis] = -1.0f;
			for (int split = 1; split < subdivisions; ++split)
			{
				float plane_const = bounds[test_axis][0] + split * len / subdivisions;
				split_tri_vert_list( in, in_count, &a_list, &a_count, &b_list, &b_count, plane_norm, plane_const );
				if (a_count < MINIMUM_TRIS_IN_COLLISION_BOX || b_count < MINIMUM_TRIS_IN_COLLISION_BOX) continue;
				a_vol = get_tri_vert_list_bounds( a_list, a_count, a_bounds );
				b_vol = get_tri_vert_list_bounds( b_list, b_count, b_bounds );
				if (a_count * a_count + b_count * b_count <= best_count && a_vol + b_vol < 1.5f * volume)
				{
					best_axis = test_axis;
					best_plane_normal = plane_norm;
					best_plane_const = plane_const;
					best_count = a_count * a_count + b_count * b_count;
				}
				
				delete [] a_list;
				delete [] b_list;
			}
		}
	}
	
	if (best_axis == -1)
	{
		box.sub_box_first_index = -1;
		box.sub_box_count = 0;
		box.triangle_first_index = collision_tri_count;
		box.triangle_count = in_count;
		
		for (int t = 0; t < in_count; ++t)
		{
			skmd_complex_collision_tri &tri = collision_tris[collision_tri_count++];
			vec3 face_normal = cross( in[t][0] - in[t][1], in[t][2] - in[t][1] );
			out_plane( &tri, 0, face_normal, in[t][0], in[t][1], in[t][2], 0 );
			out_plane( &tri, 1, cross( in[t][1] - in[t][0], face_normal ), in[t][1], in[t][0], in[t][0], &in[t][2] );
			out_plane( &tri, 2, cross( in[t][2] - in[t][1], face_normal ), in[t][2], in[t][1], in[t][1], &in[t][0] );
			out_plane( &tri, 3, cross( in[t][0] - in[t][2], face_normal ), in[t][0], in[t][2], in[t][2], &in[t][1] );
		}
	}
	else
	{
		split_tri_vert_list( in, in_count, &a_list, &a_count, &b_list, &b_count, best_plane_normal, best_plane_const );
		box.sub_box_first_index = collision_box_count;
		box.sub_box_count = 2;
		box.triangle_first_index = -1;
		box.triangle_count = 0;
		collision_box_count += 2;
		a_vol = get_tri_vert_list_bounds( a_list, a_count, a_bounds );
		b_vol = get_tri_vert_list_bounds( b_list, b_count, b_bounds );
		recursive_build_skelmodel_collision( box.sub_box_first_index, a_list, a_count, a_bounds, a_vol ); 
		recursive_build_skelmodel_collision( box.sub_box_first_index+1, b_list, b_count, b_bounds, b_vol ); 
		delete [] a_list;
		delete [] b_list;
	}
}

void clear_complex_skelmodel_collision( skmd_modelheader *mh )
{
	if (mh->complex_collision_boxes)
	{
		delete[] mh->complex_collision_boxes;
		mh->complex_collision_boxes = 0;
	}
	mh->complex_collision_box_count = 0;
	
	if (mh->complex_collision_tris)
	{
		delete[] mh->complex_collision_tris;
		mh->complex_collision_tris = 0;
	}
	mh->complex_collision_tri_count = 0;
}
	
void build_complex_skelmodel_collision( skmd_modelheader *mh )
{
	clear_complex_skelmodel_collision( mh );
	
	int vcount = mh->max_vertices;
	int i, j;
	const skmd_lodref *lodref = pick_skmd_lod( mh, &vcount );
	
	float (*pos)[4] = new float[vcount][4];
	float (*norm)[4] = new float[vcount][4];
	unsigned short (*tri)[3] = new unsigned short[lodref->data->max_triangles][3];
	object_bone *bone = new object_bone[mh->bone_count + 1];
	
	for (i = 0; i < 3; ++i) {
		bone->world_pos[i] = 0.0f;
		for (j = 0; j <3; ++j) {
			bone->m[i][j] = (i == j ? mh->model_scale : 0.0f);
		}
	}
	
	++bone;
	
	for (i = 0; i < mh->bone_count; ++i) {
		int parent = mh->bones[i].parent;
		QuatToMat( bone[i].m, mh->bones[i].float_orient );
		MatMult( bone[i].m, bone[parent].m, bone[i].m );
		for (j = 0; j < 3; ++j) {
			const float (*m)[3] = bone[parent].m;
			bone[i].world_pos[j] =  m[j][0] * mh->bones[i].float_offset[0] +
									m[j][1] * mh->bones[i].float_offset[1] +
									m[j][2] * mh->bones[i].float_offset[2] +
									bone[parent].world_pos[j];
		}
	}
	
	int tcount = 0;
	generate_skel_mesh( bone, mh->bone_count, lodref, vcount, pos, norm, tri[0], &tcount );
	
	vec3 (*tri_verts)[3] = new vec3[tcount][3];
	for (i = 0; i < tcount; ++i)
	{
		for (j = 0; j < 3; ++j)
		{
			int v = tri[i][j];
			tri_verts[i][j] = *(vec3 *)(pos[v]);
		}
	}
	
	--bone;
	delete [] bone;
	delete [] tri;
	delete [] norm;
	delete [] pos;
	
	float bounds[3][2];
	
	float volume = get_tri_vert_list_bounds( tri_verts, tcount, bounds );
	collision_box_count = 1;
	collision_tri_count = 0;
	recursive_build_skelmodel_collision( 0, tri_verts, tcount, bounds, volume );
	
	delete [] tri_verts;
	
	mh->complex_collision_box_count = collision_box_count;
	mh->complex_collision_boxes = new skmd_complex_collision_box[collision_box_count];
	memcpy( mh->complex_collision_boxes, collision_boxes, sizeof(skmd_complex_collision_box) * collision_box_count );
	
	mh->complex_collision_tri_count = collision_tri_count;
	mh->complex_collision_tris = new skmd_complex_collision_tri[collision_tri_count];
	memcpy( mh->complex_collision_tris, collision_tris, sizeof(skmd_complex_collision_tri) * collision_tri_count );
}

#pragma mark -


void defaultnew_skelmodel( tag_entry *e )
{
	skmd_tag_header head;
	memset( &head, 0, sizeof(head) );
	byte_swap_data_be( "skmd header", &head, sizeof(head), 1, bs_skmd_tag_header );
	set_entry_data( e, &head, sizeof(head), false );
}


void edit_skelmodel( tag_entry *entry )
{
	
}


bool read_skel_model( skmd_tag_header *models, tag_entry *e )
{
	const void *data;
	unsigned length;

	if (!get_entry_data( e, &data, &length ))
		return false;
	
	byte_swap_move_be( "skmd_header", models, (void*)data, sizeof(*models), 1, bs_skmd_tag_header );
	for (int i = 0; i < models->model_count; ++i)
	{
		skmd_modelheader *buf = (skmd_modelheader *)alloc_align( models->models[i].length, 16 );
		memcpy( buf, (byte *)data + models->models[i].offset, models->models[i].length );
		unsigned hlen = import_skmd_modelheader( buf );
		skmd_modelheader *savemodel = (skmd_modelheader *)alloc_align( hlen, 16 );
		memcpy( savemodel, buf, hlen );
		setup_skmd_modelheader_ptrs( savemodel );
		for (int j = 0; j < buf->lod_count; ++j) {
			skmd_lod *lod = (skmd_lod *)alloc_align( buf->lods[j].length, 16 );
			memcpy( lod, buf->lods[j].data, buf->lods[j].length );
			int lodlen;
			lodlen = import_skmd_lod( buf->bone_count, buf->lods[j].min_vertices, buf->lods[j].max_vertices, lod );
			assert( lodlen == buf->lods[j].length );
			if (lodlen != buf->lods[j].length) {
				free_align( buf );
				models->model_count = i;	// we may leak a little memory when a file is corrupt...
				return false;
			}
			savemodel->lods[j].data = lod;
		}
		free_align( buf );
		models->models[i].model = savemodel;
	}
	
	return true;
}

void dependency_skelmodel( tag_entry *e )
{
	skmd_tag_header def;
	if( !read_skel_model( &def, e ) )
		return;
		
	add_tag_dependency( 'skan', def.animation );
	for( int i = 0; i < def.model_count; ++i )
	{
		add_tag_dependency( 'txst', def.models[i].model->texture_stack );
		
		// add attachments
		for( int j = 0; j < def.models[i].model->bone_count; j++ )
		{
			tag type = -1;
			switch ((def.models[i].model->bones[j].flags & _skmd_bone_attach_type_mask) >> _skmd_bone_attach_type_bit_0)
			{
				case _skmd_bone_attach_type_is_skelmodel:
					type = 'skmd';
					break;
				
				case _skmd_bone_attach_type_is_light:
					type = 'loli';
					break;
				
				case _skmd_bone_attach_type_is_local_projectiles:
					type = 'lpgr';
					break;
			}
			
			if (type != -1)
			{
				add_tag_dependency( type, def.models[i].model->bones[j].attachment_id );
			}
		}
		
		for( int k = 0; k < def.models[i].model->lod_count; ++k )
		{
			free_align( (void *)(def.models[i].model->lods[k].data) );
		}
		
		free_align( (void *)(def.models[i].model) );
	}
}

bool save_skel_model( skmd_tag_header *models, tag_entry *e )
{
	int i, j;
	
	int total_size = sizeof( skmd_tag_header );
	int model_size;
	int lod_size;
	int out_size;
	skmd_modelheader *m;
	skmd_lod *lod;
	
	for (i = 0; i < models->model_count; ++i) {
		total_size = (total_size + 15) & ~15;
		models->models[i].offset = total_size;
		
		m = const_cast<skmd_modelheader *>(models->models[i].model);
		
		model_size = sizeof(skmd_modelheader);
		model_size += (m->bone_count - 1) * sizeof(skmd_bone);
		model_size = (model_size + 15) & ~15;
		model_size += 2 * sizeof(float) * m->max_vertices;
		model_size += m->complex_collision_box_count * sizeof(skmd_complex_collision_box);
		model_size += m->complex_collision_tri_count * sizeof(skmd_complex_collision_tri);
		model_size += (m->max_vertices + 1 - m->hard_min_vertices) * sizeof(word);
		
		for (j = 0; j < m->lod_count; ++j) {
			model_size = (model_size + 15) & ~15;
			m->lods[j].offset = model_size;
			
			lod = const_cast<skmd_lod *>(m->lods[j].data);
			lod_size = ((size_t)(lod->weights + lod->weighted_vert_count)) - ((size_t)lod);
			m->lods[j].length = lod_size;
			model_size += lod_size;
		}
		
		models->models[i].length = model_size;
		total_size += model_size;
	}
	
	byte *tagdata = (byte *)alloc_align( total_size, 16 );
	memset( tagdata, 0, total_size );
	
	byte_swap_move_be( "skmd_header", tagdata, models, sizeof(*models), 1, bs_skmd_tag_header );
	for (i = 0; i < models->model_count; ++i) {
		m = const_cast<skmd_modelheader *>(models->models[i].model);
		model_size = ((size_t)(&m->uv[m->max_vertices])) - ((size_t)(m));
		memcpy( tagdata + models->models[i].offset, m, model_size );
		
		int collision_box_size = m->complex_collision_box_count * sizeof(skmd_complex_collision_box);
		((skmd_modelheader *)(tagdata + models->models[i].offset))->complex_collision_boxes = (skmd_complex_collision_box *)(tagdata + models->models[i].offset + model_size);
		memcpy( tagdata + models->models[i].offset + model_size, m->complex_collision_boxes, collision_box_size );
		model_size += collision_box_size;
		
		int collision_tri_size = m->complex_collision_tri_count * sizeof(skmd_complex_collision_tri);
		((skmd_modelheader *)(tagdata + models->models[i].offset))->complex_collision_tris = (skmd_complex_collision_tri *)(tagdata + models->models[i].offset + model_size);
		memcpy( tagdata + models->models[i].offset + model_size, m->complex_collision_tris, collision_tri_size );
		model_size += collision_tri_size;
		
		int real_vert_count_size = (m->max_vertices + 1 - m->hard_min_vertices) * sizeof(word);
		((skmd_modelheader *)(tagdata + models->models[i].offset))->real_vertex_counts = (word *)(tagdata + models->models[i].offset + model_size);
		memcpy( tagdata + models->models[i].offset + model_size, m->real_vertex_counts + m->hard_min_vertices, real_vert_count_size );
		model_size += real_vert_count_size;
		
		out_size = export_skmd_modelheader( (skmd_modelheader *)(tagdata + models->models[i].offset) );
		assert( out_size == model_size );
		
		for (j = 0; j < m->lod_count; ++j) {
			lod = const_cast<skmd_lod *>(m->lods[j].data);
			memcpy( tagdata + models->models[i].offset + m->lods[j].offset, m->lods[j].data, m->lods[j].length );
			out_size = export_skmd_lod( m->bone_count, m->lods[j].min_vertices, m->lods[j].max_vertices,
										(skmd_lod *)(tagdata + models->models[i].offset + m->lods[j].offset) );
			assert( out_size == m->lods[j].length );
		}
	}
	
	set_entry_data( e, tagdata, total_size, false );
	free_align( tagdata );
	
	return true;
}




struct attach_data {
	int type;
	tag id;
};

struct attach_dialog_data {
	tag_file *source;
	skmd_modelheader *model;
	attach_data *data;
};


struct shadow_data {
	int axis;
	float near_dist, far_dist, width;
	vec3 pos;
	float m[3][3];
};

struct shadow_dialog_data {
	tag_file *source;
	skmd_modelheader *model;
	shadow_data *data;
};


