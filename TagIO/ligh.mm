
#include "ligh.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code lightning_byte_swapping_data[]=
{
	_begin_bs_array, 1,

//	unsigned long flags;
	_4byte,

//	file_tag collection_reference_tag;
	_4byte,

//	long shock_duration; // in ticks
//	long fade_duration;
	_4byte,
	_4byte,

//	world_distance bolt_length;
	_4byte,

// short sequence_indexes[MAXIMUM_NUMBER_OF_LIGHTNING_DEFINITION_SEQUENCES];
	
	_begin_bs_array, MAXIMUM_NUMBER_OF_LIGHTNING_DEFINITION_SEQUENCES,
		
		_2byte,
		
	_end_bs_array,
	
// 	short_world_distance velocity;
//	short_fixed scale;
	_2byte,
	_2byte,	

// 	short fork_segment_minimum;
//	short fork_segment_delta;
	_2byte,
	_2byte,	

//	short fork_overlap_amount;
//	short main_bolt_overlap_amount;
	_2byte,
	_2byte,	

//	angle angular_limit;
	_2byte,	

// struct damage_definition damage;

	// short type;
	// word flags;
	_2byte,
	_2byte,
	
	// short damage_lower_bound, damage_delta;
	_2byte, _2byte,

	// short_world_distance radius_lower_bound, radius_delta;
	// short_world_distance rate_of_expansion; // world units per tick
	_2byte, _2byte,
	_2byte,

	// short_fixed damage_to_velocity;
	_2byte,

	//	short apparent_number_of_bolts;
	_2byte,

	//------- postprocessed stuff
//	short collection_reference_index;
//	short collection_index;
//	short color_table_index;
	3*sizeof(short),
	
	_end_bs_array
};

lightning_definition default_ligh =
{
	0,
	static_cast<file_tag>(-1),
	0,
	0,
	0,
	0, 0, 0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	// damage
	0,
	0,
	0, 0,
	0, 0,
	0,
	0,
	// end damage
	0,
	0,
	0,
	0
};

// ==============================  EDIT WINDOW  ========================================
void edit_ligh( tag_entry* e )
{

}

void defaultnew_ligh( tag_entry *e )
{
	lightning_definition def;
	byte_swap_move_be( "ligh", &def, &default_ligh, sizeof(lightning_definition), 1, lightning_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_ligh( tag_entry *e )
{
	lightning_definition def;
	read_lightning_definition( &def, e );
	add_tag_dependency( 'txst', def.collection_reference_tag );
}

// ==============================  READ TAG DATA  ========================================
bool read_lightning_definition( lightning_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "ligh", e, def, SIZEOF_STRUCT_LIGHTNING_DEFINITION, 1, lightning_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_lightning_definition( lightning_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "ligh", e, def, SIZEOF_STRUCT_LIGHTNING_DEFINITION, 1, lightning_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_lightning_definition( lightning_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'ligh', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_lightning_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_lightning_definition( lightning_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'ligh', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_lightning_definition( def, e );
}


