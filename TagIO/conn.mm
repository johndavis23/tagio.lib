
#include "conn.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code connector_byte_swapping_data[]=
{
	_begin_bs_array, 1,

	//	unsigned long flags;
	_4byte,

	//	file_tag collection_reference_tag;
	_4byte,

	//	short normal_sequence_index;
	_2byte,
	
	// short_fixed origin_object_height_fraction;
	_2byte,	

	// short_world_distance distance_between_interpolants;
	_2byte,	

	//	short damaged_sequence_index;
	_2byte,

	//------- postprocessed stuff
	//	short collection_reference_index;
	//	short collection_index;
	//	short color_table_index;
	3*sizeof(short),
	
	// short unused[5];
	5*sizeof(short),

	_end_bs_array
};

connector_definition default_conn =
{
	0,
	static_cast<file_tag>(-1),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0, 0, 0, 0, 0
};

// ==============================  EDIT WINDOW  ========================================
void edit_conn( tag_entry* e )
{

}

void defaultnew_conn( tag_entry *e )
{
	connector_definition def;
	
	byte_swap_move_be( "conn", &def, &default_conn, sizeof(connector_definition), 1, connector_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_conn( tag_entry *e )
{
	connector_definition def;
	read_connector_definition( &def, e );
	add_tag_dependency( 'core', def.collection_reference_tag );
}


// ==============================  READ TAG DATA  ========================================
bool read_connector_definition( connector_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "conn", e, def, SIZEOF_STRUCT_CONNECTOR_DEFINITION, 1, connector_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_connector_definition( connector_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "conn", e, def, SIZEOF_STRUCT_CONNECTOR_DEFINITION, 1, connector_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_connector_definition( connector_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'conn', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_connector_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_connector_definition( connector_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'conn', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_connector_definition( def, e );
}


