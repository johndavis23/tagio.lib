// stli.h
#ifndef __stli_h__
#define __stli_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	STRING_LIST_DEFINITION_GROUP_TAG= 0x73746c69, // 'stli' (looks like text)
	STRING_LIST_DEFINITION_VERSION_NUMBER= 1,
	MAXIMUM_STRING_LIST_DEFINITIONS_PER_MAP= 128,
	MAX_STRINGLIST_SIZE= 16384
};

struct string_list_definition
{
	long string_length;
	char string_buffer[MAX_STRINGLIST_SIZE];
};

int count_strings_in_list( tag_entry * );
int get_string_at_index( tag_entry *, int index, char *string, int max_length );

//=====================================================================================================
bool read_string_list_definition( string_list_definition* def, tag id );
bool save_string_list_definition( string_list_definition* def, tag id );

bool read_string_list_definition( string_list_definition* def, tag_entry* e );
bool save_string_list_definition( string_list_definition* def, tag_entry* e );



#endif