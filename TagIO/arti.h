// arti.h
#ifndef __arti_h__
#define __arti_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

#include "obje.h"
#include "mons.h"

/* ---------- constants */

enum
{
	ARTIFACT_DEFINITION_GROUP_TAG= 'arti',
	ARTIFACT_DEFINITION_VERSION_NUMBER= 1,

	MAXIMUM_ARTIFACT_DEFINITIONS_PER_MAP= 16
};

/* ---------- definition */

enum // flags
{
	_artifact_overrides_owner_attack_bit,
	_artifact_vanishes_after_last_charge_bit,
	
	NUMBER_OF_ARTIFACT_FLAGS,
	
	_artifact_overrides_owner_attack_flag= FLAG(_artifact_overrides_owner_attack_bit),
	_artifact_vanishes_after_last_charge_flag= FLAG(_artifact_vanishes_after_last_charge_bit)
};

enum // projectiles
{
	_artifact_projectile_tag,
	NUMBER_OF_ARTIFACT_PROJECTILES,
	MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES= 4
};

enum // sequences
{
	_artifact_sequence_inventory,
	NUMBER_OF_ARTIFACT_SEQUENCES,
	MAXIMUM_NUMBER_OF_ARTIFACT_SEQUENCES= 4
};

#define SIZEOF_STRUCT_ARTIFACT_DEFINITION 168
struct artifact_definition
{
	unsigned long flags;
	
	file_tag monster_restriction_tag; // or NONE
	short specialization_restriction; // bow, sword, etc ... or NONE
	
	short initial_charges_lower_bound, initial_charges_delta; // not displayed if zero
	word pad;
	
	file_tag collection_tag;
	short sequence_indexes[MAXIMUM_NUMBER_OF_ARTIFACT_SEQUENCES];
	
	file_tag projectile_tags[MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES];
	
	unsigned long bonus_monster_flags;
	unsigned long bonus_terrain_flags;
	
	short_fixed bonus_effect_modifiers[MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS];
	
	struct monster_attack_definition override_attack;
	
	file_tag special_ability_string_list_tag;
	
	short unused[2];
	
	short collection_index;
	short special_ability_string_list_index;
	short projectile_types[MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES];
};


// =========================================================================================

bool read_artifact_definition( artifact_definition* def, tag id );
bool save_artifact_definition( artifact_definition* def, tag id );

bool read_artifact_definition( artifact_definition* def, tag_entry* e );
bool save_artifact_definition( artifact_definition* def, tag_entry* e );


#endif