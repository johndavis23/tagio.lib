
#include "scen.h"

#include "tagpick.h"
#include <assert.h>
#include "skelmodel.h"


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code scenery_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	
	// file_tag collection_reference_tag;
	_4byte,
	
	// file_tag object_tag;
	// file_tag projectile_tag; // if not NONE, this piece of scenery is a dormant projectile (.sequence_index will be ignored)
	_4byte,
	_4byte,
	
	// short netgame_scoring_type; // invalid if not a flag
	// short flag_number; // invalid if not a flag
	_2byte,
	_2byte,

	// short unused[2];
	2*sizeof(short),
	
	// file_tag projectile_group_tags[MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION];
	_begin_bs_array, MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION, _4byte, _end_bs_array,
	
	// short sequence_indexes[MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION];
	_begin_bs_array, MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION, _2byte, _end_bs_array,
	
	3*sizeof(short),

	// short model_permutation_delta;
	_2byte,
	
	1*sizeof(short),
	
	// file_tag projectile_group_tags[MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION];
	_begin_bs_array, MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION, _4byte, _end_bs_array,
	
	// short unused2[29];
	19*sizeof(short),
	
	// short projectile_group_types[MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION];
	// short collection_reference_index;
	// short impact_projectile_group_type;
	// short object_type;
	// short projectile_type;
	MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION*sizeof(short),
	MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION*sizeof(short),
	4*sizeof(short),
	
	_end_bs_array
};

scenery_definition default_scen =
{
	0,
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	static_cast<file_tag>(-1),
	0,
	0,
	0, 0,
	static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1),
	0, 0, 0,
	0, 0, 0,
	0,
	0,
	static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1),
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0,
	0,
	0,
	0,
	0,
};


// ==============================  EDIT WINDOW  ========================================


void defaultnew_scen( tag_entry *e )
{
	scenery_definition def;
	byte_swap_move_be( "scen", &def, &default_scen, sizeof(scenery_definition), 1, scenery_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}


void update_scen( tag_entry *e )
{	
	for (;;)
	{
		int version = get_entry_version( e );
		switch (version)
		{
			case 1: {
					set_entry_version( e, 2 ); // no changes!
				} break;
				
			case 2:
				return;
			default:
				assert(0);
				return;
		}
	}
}


void dependency_scen( tag_entry *e )
{
	scenery_definition def;
	read_scenery_definition( &def, e );
	int i;
	bool skel = def.flags & _scenery_is_skelmodel_flag;
	add_tag_dependency( 'obje', def.object_tag );
	add_tag_dependency( 'proj', def.projectile_tag );
	for (i = 0; i < NUMBER_OF_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION; ++i)
	{
		add_tag_dependency( 'prgr', def.projectile_group_tags[i] );
	}
	if (skel)
	{
		for (i = 0; i < NUMBER_OF_SEQUENCES_PER_SCENERY_DEFINITION; ++i)
		{
			add_tag_dependency( 'skmd', def.skelmodel_tags[i] );
		}
	}
	else
	{
		add_tag_dependency( skel ? 'skmd' : 'core', def.collection_reference_tag );
	}
}


// ==============================  READ TAG DATA  ========================================
bool read_scenery_definition( scenery_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "scen", e, def, SIZEOF_STRUCT_SCENERY_DEFINITION, 1, scenery_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_scenery_definition( scenery_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "scen", e, def, SIZEOF_STRUCT_SCENERY_DEFINITION, 1, scenery_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_scenery_definition( scenery_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'scen', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_scenery_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_scenery_definition( scenery_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'scen', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_scenery_definition( def, e );
}

