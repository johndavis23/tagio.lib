
#include "texturestack.h"
#include "byte_swapping.h"
#include "psd_read.h"
 
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "jpeg.h"

enum { x_size = 144, y_size = 200, x_offset = 8, y_offset = 8 };

struct recompress_texture_data {
	stacked_texture *tex;
	void *raw;
	int max_quality;
	bool more_coming;
	bool use_for_all;
	unsigned char out_format;
	int out_jpeg_quality;
};

inline void SetRGBPoint( byte *rgb, int width, int height, int x, int y, int r, int g, int b )
{
	x = x & (width - 1);
	y = y & (height - 1);
	rgb += (y * width + x) * 3;
	rgb[0] = r;
	rgb[1] = g;
	rgb[2] = b;
}

void DrawRGBLine( byte *rgb, int width, int height, int x1, int y1, int x2, int y2, int r, int g, int b )
{
	int dx = x2 - x1;
	int dy = y2 - y1;
	int x, y;
	
	if (abs(dx) > abs(dy)) {
		if (dx > 0) {
			for (x = x1; x <= x2; ++x) {
				y = y1 + ((x - x1) * dy) / dx;
				SetRGBPoint( rgb, width, height, x, y, r, g, b );
			}
		} else {
			for (x = x1; x >= x2; --x) {
				y = y1 + ((x - x1) * dy) / dx;
				SetRGBPoint( rgb, width, height, x, y, r, g, b );
			}
		}
	} else {
		if (dy > 0) {
			for (y = y1; y <= y2; ++y) {
				x = x1 + ((y - y1) * dx) / dy;
				SetRGBPoint( rgb, width, height, x, y, r, g, b );
			}
		} else if (dy < 0) {
			for (y = y1; y >= y2; --y) {
				x = x1 + ((y - y1) * dx) / dy;
				SetRGBPoint( rgb, width, height, x, y, r, g, b );
			}
		} else {
			SetRGBPoint( rgb, width, height, x1, y1, r, g, b );
		}
	}
}

static char *TextFloat( double value, int precision = 3 )
{
	static char buffer[256];
	
	sprintf( buffer, "%.*f", precision, value );
	char *s = buffer + strlen(buffer) - 1;
	while (s > buffer && *s == '0' && s[-1] != '.') {
		*s = 0;
		--s;
	}
	return buffer;
}

struct texinfodata
{
	texture_stack_head *head;
	stacked_texture *tex;
};



#pragma mark -

static stacked_texture *new_stacked_texture( int channels, int use, int layer, int width, int height )
{
	stacked_texture *tex = (stacked_texture *)malloc( sizeof(stacked_texture) );
	tex->next = tex->prev = 0;
	tex->compressed_data = 0;
	tex->compressed_length = 0;
	tex->thumbnail = 0;
	tex->channels = channels;
	tex->format = 0;
	tex->use = use;
	tex->layer = layer;
	tex->width = width;
	tex->height = height;
	tex->decal_corner_x = 0;
	tex->decal_corner_y = 0;
	tex->decal_ux = tex->decal_uy = 0;
	tex->decal_vx = tex->decal_vy = 0;
	return tex;
}


static stacked_texture *new_mask_texture( const byte *data, int layer, int xoff, int yoff, int in_w, int in_h, int w, int h )
{
	stacked_texture *tex = new_stacked_texture( 1, txst_use_skinblend, layer, w, h );
	tex->compressed_length = w * h;
	byte *buf = (byte *)malloc( w * h );
	tex->compressed_data = buf;
	tex->format = 0;
	memset( buf, 0, w * h );
	for (int y = 0; y < in_h; ++y) {
		for (int x = 0; x < in_w; ++x) {
			buf[xoff + x + (yoff + y) * w] = data[y * in_w + x];
		}
	}
	return tex;
}


static bool memnchr( byte *data, byte chr, int count )
{
	for (int i = 0; i < count; ++i) {
		if (data[i] != chr) return true;
	}
	return false;
}



#pragma mark -



byte_swap_code swap_txst_tagheader[] =
{
	_begin_bs_array, 1,
	
	// unsigned short count;
	_2byte,
	
	// unsigned short padding;
	2,
	
	_end_bs_array
	
	// count * txst_stackrefs follow
};


byte_swap_code swap_txst_stackref[] =
{
	_begin_bs_array, 1,
	
	//unsigned offset;	// from start of tag
	_4byte,
	
	//unsigned length;
	_4byte,
	
	// unsigned short flags
	_2byte,
	
	//unsigned short usage_chance;
	_2byte,
	
	_end_bs_array
};


byte_swap_code swap_txst_stackheader[] =
{
	_begin_bs_array, 1,
	
	// unsigned short count;
	_2byte,
	
	// unsigned short padding;
	2,
	
	_end_bs_array
	
	// count * txst_texheaders follow
};


byte_swap_code swap_txst_texheader[] =
{
	_begin_bs_array, 1,
	
	//	unsigned offset;	// from start of stack
	_4byte,
	
	//	unsigned length;
	_4byte,
	
	//	unsigned char channels;	// 0=empty (decal only), 1=alpha, 3=rgb, 4=rgba
	_byte,
	
	//	unsigned char format;	// right now only 0=raw 8-bit
	_byte,
	
	//	unsigned char use;
	_byte,
	
	//	unsigned char layer;	// for skinblends and detailmasks, 0-based
	_byte,
	
	//	unsigned short width, height;
	_2byte, _2byte,
	
	//	signed short decal_corner_x;
	_2byte,
	
	//	signed short decal_corner_y;
	_2byte,
	
	//	signed short decal_ux, decal_uy;
	_2byte, _2byte,
	
	//	signed short decal_vx, decal_vy;
	_2byte, _2byte,
	
	_end_bs_array
	
	// count * txst_texheaders follow
};


void edit_texturestack( tag_entry *entry )
{

}


void defaultnew_texturestack( tag_entry *e )
{
	txst_tagheader h = { 0, 0 };
	set_entry_data( e, &h, sizeof(h), false );
}


static void mipdown( byte *pix, int &width, int &height, int channels )
{
	int y, x, c, v;
	if (width > 1) {
		if (height > 1) {
			for (y = 0; y < height >> 1; ++y) {
				for (x = 0; x < width >> 1; ++x) {
					for (c = 0; c < channels; ++c) {
						v = pix[(y * 2 * width + x * 2) * channels + c] +
							pix[(y * 2 * width + x * 2 + 1) * channels + c] +
							pix[((y * 2 + 1) * width + x * 2) * channels + c] +
							pix[((y * 2 + 1) * width + x * 2 + 1) * channels + c];
						pix[(y * (width>>1) + x) * channels + c] = (v+2) >> 2;
					}
				}
			}
			width >>= 1;
			height >>= 1;
		} else {
			for (x = 0; x < width >> 1; ++x) {
				for (c = 0; c < channels; ++c) {
					v = pix[(x * 2) * channels + c] +
						pix[(width + x * 2 + 1) * channels + c];
					pix[x * channels + c] = (v+1) >> 1;
				}
			}
			width >>= 1;
		}
	} else {
		if (height > 1) {
			for (y = 0; y < height >> 1; ++y) {
				for (c = 0; c < channels; ++c) {
					v = pix[(y * 2 * width) * channels + c] +
						pix[((y * 2 + 1) * width) * channels + c];
					pix[(y * (width>>1)) * channels + c] = (v+1) >> 1;
				}
			}
			height >>= 1;
		}
	}
}




void compact_stackset( stack_set *ss )
{
	int in, out;
	for (in = 0, out = 0; in < ss->used_stacks; ++in) {
		if (ss->stacks[in].textures) {
			ss->stacks[out] = ss->stacks[in];
			++out;
		} else {
			ss->modified = true;
		}
	}
	ss->used_stacks = out;
	memset( ss->stacks + out, 0, sizeof(texture_stack_head) * (ss->avail_stacks - out) );
}

stack_set *load_stackset( tag_entry *e )
{
	txst_tagheader th;
	txst_stackref sr;
	txst_stackheader sh;
	txst_texheader tex;
	
	if (!e || get_entry_type(e) != 'txst') return 0;
	byte *raw = (byte *)get_entry_data( e );
	if (!raw) return 0;
	
	byte_swap_move_be( "txst_tagheader", &th, raw, sizeof(th), 1, swap_txst_tagheader );
	
	stack_set *ss = new_empty_stackset( th.count + 20 );
	if (!ss) return 0;
	
	txst_stackref *insr = (txst_stackref *)(raw + sizeof(th));
	for (int i = 0; i < th.count; ++i) {
		byte_swap_move_be( "txst_stackref", &sr, insr + i, sizeof(sr), 1, swap_txst_stackref );
		
		unsigned flags = sr.flags;
		unsigned usage_chance = sr.usage_chance;
				
		byte *stack = raw + sr.offset;
		byte_swap_move_be( "txst_stackheader", &sh, stack, sizeof(sh), 1, swap_txst_stackheader );
		txst_texheader *intex = (txst_texheader *)(stack + sizeof(sh));
		for (int j = 0; j < sh.count; ++j) {
			byte_swap_move_be( "txst_texheader", &tex, intex + j, sizeof(tex), 1, swap_txst_texheader );
			stacked_texture *st = (stacked_texture *)( malloc(sizeof(stacked_texture) ));
			if (!st) {
				dispose_stackset( ss );
				return 0;
			}
			void *buf;
			st->compressed_data = buf = malloc(tex.length);
			st->compressed_length = tex.length;
			insert_into_texturestack( ss, i, j, st );
			if (!buf) {
				dispose_stackset( ss );
				return 0;
			}
			memcpy( buf, stack + tex.offset, tex.length );
			st->thumbnail = 0;
			st->channels = tex.channels;
			st->format = tex.format;
			st->use = tex.use;
			st->layer = tex.layer;
			st->width = tex.width;
			st->height = tex.height;
			st->decal_corner_x = tex.decal_corner_x;
			st->decal_corner_y = tex.decal_corner_y;
			st->decal_ux = tex.decal_ux;
			st->decal_uy = tex.decal_uy;
			st->decal_vx = tex.decal_vx;
			st->decal_vy = tex.decal_vy;
		}
		ss->stacks[i].flags = flags;
		ss->stacks[i].usage_chance = usage_chance;
	}
	
	compact_stackset( ss );
	
	ss->modified = false;
	ss->entry = e;
	
	return ss;
}


void save_stackset( tag_entry *entry, stack_set *ss )
{
	txst_tagheader taghead;
	txst_stackref stackref;
	txst_stackheader stackhead;
	txst_texheader texhead;
	
	int i;
	stacked_texture *tex;
	
	if (!entry || !ss || !ss->stacks) return;
	int total = sizeof(txst_tagheader) + ss->used_stacks * sizeof(txst_stackref) + ss->used_stacks * sizeof(txst_stackheader);
	for (i = 0; i < ss->used_stacks; ++i) {
		for (tex = ss->stacks[i].textures; tex; tex = tex->next) {
			if (!tex->compressed_data || tex->compressed_length <= 0) return;
			total += sizeof(txst_texheader) + tex->compressed_length;
		}
	}
	byte *buf = (byte *)malloc( total );
	if (!buf) return;
	
	taghead.count = ss->used_stacks;
	byte_swap_move_be( "txst_tagheader", buf, &taghead, sizeof(taghead), 1, swap_txst_tagheader );
	
	unsigned off1 = sizeof(txst_tagheader);
	unsigned off2 = off1 + ss->used_stacks * sizeof(txst_stackref);
	
	for (i = 0; i < ss->used_stacks; ++i) {
		int stacktot = sizeof(txst_stackheader);
		int stackcount = 0;
		for (tex = ss->stacks[i].textures; tex; tex = tex->next) {
			stackcount += 1;
			stacktot += sizeof(txst_texheader) + tex->compressed_length;
		}
		stackref.offset = off2;
		stackref.length = stacktot;
		stackref.flags = ss->stacks[i].flags;
		stackref.usage_chance = ss->stacks[i].usage_chance;
		byte_swap_move_be( "txst_stackref", buf + off1, &stackref, sizeof(stackref), 1, swap_txst_stackref );
		off1 += sizeof(stackref);
		
		unsigned off3 = 0;
		stackhead.count = stackcount;
		byte_swap_move_be( "txst_stackheader", buf + off2, &stackhead, sizeof(stackhead), 1, swap_txst_stackheader );
		off3 += sizeof(stackhead);
		
		unsigned off4 = off3 + stackcount * sizeof(txst_texheader);
		for (tex = ss->stacks[i].textures; tex; tex = tex->next) {
			texhead.offset = off4;
			texhead.length = tex->compressed_length;
			texhead.channels = tex->channels;
			texhead.format = tex->format;
			texhead.use = tex->use;
			texhead.layer = tex->layer;
			texhead.width = tex->width;
			texhead.height = tex->height;
			texhead.decal_corner_x = tex->decal_corner_x;
			texhead.decal_corner_y = tex->decal_corner_y;
			texhead.decal_ux = tex->decal_ux;
			texhead.decal_uy = tex->decal_uy;
			texhead.decal_vx = tex->decal_vx;
			texhead.decal_vy = tex->decal_vy;
			byte_swap_move_be( "txst_texheader", buf + off2 + off3, &texhead, sizeof(texhead), 1, swap_txst_texheader );
			off3 += sizeof(texhead);
			
			memcpy( buf + off2 + off4, tex->compressed_data, tex->compressed_length );
			off4 += tex->compressed_length;
		}
		if (off3 != sizeof(txst_stackheader) + stackcount * sizeof(txst_texheader) || off4 != stacktot) {
			free( buf );
			return;
		}
		off2 += stacktot;
	}
	
	if (off1 != sizeof(txst_tagheader) + ss->used_stacks * sizeof(txst_stackref) || off2 != total) {
		free( buf );
		return;
	}
	
	set_entry_data( entry, buf, total, true );
	ss->modified = false;
	ss->entry = entry;
}


void dispose_stackset( stack_set *ss )
{
	if (!ss) return;
	
	if (ss->stacks) {
		for (int i = 0; i < ss->used_stacks; ++i) {
			for (stacked_texture *tex = ss->stacks[i].textures, *next; tex; tex = next) {
				next = tex->next;
				dispose_stacked_texture( tex );
			}
		}
		free( ss->stacks );
	}
	
	free( ss );
}



stack_set *new_empty_stackset( int maxstacks )
{
	stack_set *ss = (stack_set *)malloc(sizeof(stack_set));
	if (!ss) return 0;
	ss->entry = 0;
	ss->used_stacks = 0;
	ss->avail_stacks = maxstacks;
	ss->modified = true;
	ss->stacks = (texture_stack_head *)calloc( maxstacks, sizeof(texture_stack_head) );
	if (!ss->stacks) {
		dispose_stackset( ss );
		return 0;
	}
	return ss;
}


void insert_texturestack_in_set( stack_set *ss, int index )
{
	if (!ss) return;
	ss->modified = true;
	if (index >= ss->avail_stacks || ss->used_stacks + 1 > ss->avail_stacks) {
		int newcount = ss->used_stacks + 1;
		if (newcount <= index) newcount = index + 1;
		newcount += 20;
		texture_stack_head *newstacks = (texture_stack_head*)calloc( newcount, sizeof(texture_stack_head) );
		if (!newstacks) {
			return;
		}
		memcpy( newstacks, ss->stacks, sizeof(texture_stack_head) * ss->used_stacks );
		free( ss->stacks );
		ss->stacks = newstacks;
		ss->avail_stacks = newcount;
	}
	if (index < ss->used_stacks) {
		memmove( ss->stacks + index + 1, ss->stacks + index, sizeof(texture_stack_head) * (ss->used_stacks - index) );
		++ss->used_stacks;
	} else {
		memset( ss->stacks + ss->used_stacks, 0, sizeof(texture_stack_head) * (index - ss->used_stacks) );
		ss->used_stacks = index + 1;
	}
	ss->stacks[index].textures = 0;
	ss->stacks[index].flags = 0;
	ss->stacks[index].usage_chance = 1;
}


int insert_into_texturestack( stack_set *ss, int index, int pos, stacked_texture *tex )
{
	if (!ss || !tex) return -1;
	ss->modified = true;
	if (index >= ss->used_stacks) {
		insert_texturestack_in_set( ss, index );
		if (index >= ss->used_stacks) {
			return -1;
		}
	}
	if (ss->stacks[index].textures == 0 || pos == 0) {
		LLADD( ss->stacks[index].textures, tex );
		return 0;
	}
	int step = 1;
	stacked_texture *last = ss->stacks[index].textures;
	while (step != pos && last->next) {
		last = last->next;
		++step;
	}
	LLAFTER( last, tex );
		
	return step;
}


void remove_from_texturestack( stack_set *ss, int index, stacked_texture *tex, bool collapse_set )
{
	if (!ss || !ss->stacks || !tex || index >= ss->used_stacks) return;
	ss->modified = true;
	for (stacked_texture *scan = ss->stacks[index].textures; scan; scan = scan->next) {
		if (scan == tex) {
			LLDEL( ss->stacks[index].textures, tex );
			if (collapse_set && !ss->stacks[index].textures) {
				memmove( ss->stacks + index, ss->stacks + index + 1, sizeof(texture_stack_head) * (ss->used_stacks - index - 1) );
				ss->used_stacks -= 1;
			}
		}
	}
}


#define PIECESIZE 128



stacked_texture *get_texture( stack_set *ss, int stacknum, int stacklevel )
{
	if (!ss) return 0;
	if (stacklevel < 0 || stacknum < 0 || stacknum >= ss->used_stacks) return 0;
	stacked_texture *tex = ss->stacks[stacknum].textures;
	while (tex && stacklevel--) {
		tex = tex->next;
	}
	return tex;
}


stacked_texture *duplicate_stacked_texture( const stacked_texture *tex )
{
	stacked_texture *copy = (stacked_texture *)malloc( sizeof(stacked_texture) );
	if (!copy) return 0;
	memcpy( copy, tex, sizeof(stacked_texture) );
	copy->next = copy->prev = 0;
	copy->thumbnail = 0;
	void *buf = malloc( tex->compressed_length );
	if (!buf) {
		free( copy );
		return 0;
	}
	memcpy( buf, tex->compressed_data, copy->compressed_length );
	copy->compressed_data = buf;
	return copy;
}


void dispose_stacked_texture( stacked_texture *tex )
{
	if (tex->compressed_data) {
		free( (void *)(tex->compressed_data) );
	}
	if (tex->thumbnail) {
		delete tex->thumbnail;
	}
	free( tex );
}


void recompress_texture( stacked_texture *tex, int quality )
{
	void *raw = malloc( tex->width * tex->height * tex->channels );
	if (tex->format == 1) {
		raw = malloc( tex->width * tex->height * tex->channels );
     
		decompress_jpeg( raw, tex->compressed_data, tex->compressed_length, tex->width, tex->height, tex->channels, true );
        
    } else {
		raw = (void *)(tex->compressed_data);
	}
	if (quality > 100 || quality < 1) {
		if (raw != tex->compressed_data) {
			free( (void *)(tex->compressed_data) );
			tex->compressed_data = raw;
		}
		tex->compressed_length = tex->width * tex->height * tex->channels;
		tex->format = 0;
	} else {
		int jsize = tex->width * tex->height * tex->channels * 3;
		void *jpeg = malloc( jsize );
		
        compress_jpeg( jpeg, &jsize, raw, tex->width, tex->height, tex->channels, quality );
        
		if (raw != tex->compressed_data) {
			free( raw );
		}
		if (jsize * 100 > tex->compressed_length * 95 && jsize + 1000 > tex->compressed_length) {
			free( jpeg );
			// and leave the texture alone
		} else {
			free( (void *)tex->compressed_data );
			tex->compressed_data = malloc( jsize );
			memcpy( (void *)tex->compressed_data, jpeg, jsize );
			tex->compressed_length = jsize;
			tex->format = 1;
		}
	}
}


void find_texture_in_stack( const stack_set *stacks, const stacked_texture *tex, int *outStackNum, int *outStackLevel )
{
	for (int num = 0; num < stacks->used_stacks; ++num) {
		int level = 0;
		for (const stacked_texture *scan = stacks->stacks[num].textures; scan; scan = scan->next) {
			if (tex == scan) {
				*outStackNum = num;
				*outStackLevel = level;
				return;
			}
			++level;
		}
	}
	*outStackNum = -1;
	*outStackLevel = -1;
}


