// conn.h
#ifndef __conn_h__
#define __conn_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	CONNECTOR_GROUP_TAG= 'conn',
	CONNECTOR_VERSION_NUMBER= 1,
	
	MAXIMUM_CONNECTOR_DEFINITIONS_PER_MAP= 4
};

/* ---------- structures */

enum // connector flags
{
	NUMBER_OF_CONNECTOR_DEFINITION_FLAGS
};

#define SIZEOF_STRUCT_CONNECTOR_DEFINITION 32
struct connector_definition
{
	unsigned long flags;

	file_tag collection_reference_tag;

	short normal_sequence_index;

	short_fixed origin_object_height_fraction;

	short_world_distance distance_between_interpolants;
	short damaged_sequence_index;
	
	//------- postprocessed stuff
	short collection_reference_index;
	short collection_index;
	short color_table_index;

	short unused[5];
};


//=====================================================================================================
bool read_connector_definition( connector_definition* def, tag id );
bool save_connector_definition( connector_definition* def, tag id );

bool read_connector_definition( connector_definition* def, tag_entry* e );
bool save_connector_definition( connector_definition* def, tag_entry* e );


#endif