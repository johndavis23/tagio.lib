//
//  jpeg-cpp.h
//  TagIO
//
//  Created by johndavis on 9/3/15.
//  Copyright (c) 2015 Good Lamb. All rights reserved.
//

#ifndef TagIO_jpeg_cpp_h
#define TagIO_jpeg_cpp_h
class Jpeg{
public:
void decompress_jpeg( void *out, const void *in, int in_size,
                     int width, int height, int channels, bool hq );

void compress_jpeg( void *out, int *out_len, const void *in,
                   int width, int height, int channels, int quality );

int estimate_jpeg_quality( const void *jpeg, int jpeg_len,
                          int width, int height, int channels );

};
#endif
