
#include "loli.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code loli_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
		//	word flags;
		_2byte,

		//	short fade_in_ticks;
		_2byte,
		//	short sustain_ticks;
		_2byte,
		//	short fade_out_ticks;
		_2byte,

		//	world_distance radius;
		_4byte,

		//	struct rgb_color dim_color;
		_2byte, _2byte, _2byte, _2byte,
		//	struct rgb_color bright_color;
		_2byte, _2byte, _2byte, _2byte,

		//	fixed flicker_speed_0;
		_2byte,
		//	fixed flicker_speed_1;
		_2byte,
		
		//	word flicker_amplitude_0;
		_2byte,
		//	word flicker_amplitude_1;
		_2byte,

		//	fixed jitter_speed_x;
		_2byte,
		//	short_world_distance jitter_size_x;
		_2byte,
		
		//	fixed jitter_speed_y;
		_2byte,
		//	short_world_distance jitter_size_y;
		_2byte,
		
		//	fixed jitter_speed_z;
		_2byte,
		//	short_world_distance jitter_size_z;
		_2byte,
		
	_end_bs_array
};


// ==============================  EDIT WINDOW  ========================================
void edit_loli( tag_entry* e )
{

}


static loli_definition default_loli =
{
	0,
	10,
	-1,
	10,
	INTEGER_TO_WORLD(5),
	{ 0x8080, 0x4040, 0x0000, 0 },
	{ 0xffff, 0xc0c0, 0x4040, 0 },
	2048,
	10000,
	10,
	2,
	
	2048,
	0,
	
	2048,
	0,
	
	2048,
	0
};

void defaultnew_loli( tag_entry* e )
{
	loli_definition def;
	byte_swap_move_be( "loli", &def, &default_loli, sizeof(loli_definition), 1, loli_definition_byte_swapping_data);
	set_entry_data( e, &def, sizeof(def), false );
}


void dependency_loli( tag_entry *e )
{
    #pragma unused( e )

#if 0 // no dependencies
	loli_definition def;
	read_loli_definition( &def, e );
#endif
}


// ==============================  READ TAG DATA  ========================================
bool read_loli_definition( loli_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "loli", e, def, sizeof(loli_definition), 1, loli_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_loli_definition( loli_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "loli", e, def, sizeof(loli_definition), 1, loli_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_loli_definition( loli_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'loli', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_loli_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_loli_definition( loli_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'loli', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_loli_definition( def, e );
}

