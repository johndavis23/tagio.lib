// wind.h
#ifndef __wind_h__
#define __wind_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	WIND_DEFINITION_GROUP_TAG= 0x77696e64, // 'wind' (looks like text)
	WIND_DEFINITION_VERSION_NUMBER= 1
};

enum // wind flags
{
	NUMBER_OF_WIND_DEFINITION_FLAGS
};

#define SIZEOF_STRUCT_WIND_DEFINITION 128
struct wind_definition
{
	unsigned long flags;

	int time_between_adjustments_lower_bound, time_between_adjustments_delta;
	fixed maximum_adjustment_per_tick; // Largest change in wind velocity per tick (gusting)
	
	fixed_vector3d velocity_lower_bound;
	fixed_vector3d velocity_delta;
	
	//------- postprocessed stuff
	int ticks_until_wind_changes;
	fixed_vector3d velocity;
	fixed_vector3d target_wind;

	short unused[30];
};


//=====================================================================================================
bool read_wind_definition( wind_definition* def, tag id );
bool save_wind_definition( wind_definition* def, tag id );

bool read_wind_definition( wind_definition* def, tag_entry* e );
bool save_wind_definition( wind_definition* def, tag_entry* e );




#endif