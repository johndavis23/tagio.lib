// form.h
#ifndef __form_h__
#define __form_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	FORMATION_DEFINITION_GROUP_TAG= 'form',
	FORMATION_DEFINITION_VERSION= 1
};

/* ---------- structures */

#define SIZEOF_STRUCT_FORMATION_DEFINITION 40
struct formation_definition
{
	// SHORT LINE
	short maximum_units_per_short_line_row;
	short_world_distance short_line_unit_separation;

	// LONG LINE
	short maximum_units_per_long_line_row;
	short_world_distance long_line_unit_separation;

	// LOOSE LINE
	short maximum_units_per_loose_line_row;
	short_world_distance loose_line_unit_separation;

	// STAGGERED LINE
	short maximum_units_per_staggered_line_row;
	short_world_distance staggered_line_unit_separation;

	// BOX
	short_world_distance box_unit_separation;

	// MOB
	short_world_distance minimum_mob_unit_separation;
	short_fixed mob_x_bias;

	short_world_distance minimum_encirclement_radius;
	short_world_distance encirclement_unit_separation;
	// SHALLOW ENCIRCLEMENT
	angle shallow_encirclement_arc_length;

	// DEEP ENCIRCLEMENT
	angle deep_encirclement_arc_length;

	// VANGUARD
	short_world_distance vanguard_front_row_separation;
	short_world_distance vanguard_unit_separation;
	short_world_distance vanguard_row_separation;

	// CIRCLE
	short_world_distance circle_unit_separation;
	
	short unused;
};


//=====================================================================================================
bool read_formation_definition( formation_definition* def, tag id );
bool save_formation_definition( formation_definition* def, tag id );

bool read_formation_definition( formation_definition* def, tag_entry* e );
bool save_formation_definition( formation_definition* def, tag_entry* e );



#endif