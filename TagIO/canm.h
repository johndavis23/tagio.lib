// canm.h
#ifndef __canm_h__
#define __canm_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "bytes_swapping.h"
#endif


#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

tag_entry* parse_camera_animation_from_ase( tag_file* t_file, char * ase_file );

#endif
