
#include "unit.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 #include <string.h>
 

// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code unit_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// file_tag monster_tag;
	_4byte,

	// int number_of_customizations
	_4byte,

	// struct unit_customization_settings custom_colors[MAXIMUM_CUSTOMIZATIONS_PER_UNIT];
	_begin_bs_array, MAXIMUM_CUSTOMIZATIONS_PER_UNIT,
		// file_tag logo_txst_tag;
		_4byte,
		
		// int logo_table_index;
		_4byte,
		
		// rgb_color primary_color;
		_2byte, _2byte, _2byte, _2byte,
		
		// rgb_color secondary_color;
		_2byte, _2byte, _2byte, _2byte,
		
	_end_bs_array,
	
	// short unused[55];
	7*sizeof(short),

	// short player_index;
	// file_tag parent_unit_tag; // only interesting if this is a custom unit definition
	sizeof(short),
	sizeof(file_tag),

	// computed when postprocessing this definition
	
	// short monster_type;
	// short unused2;
	2*sizeof(short),
	
	_end_bs_array
};

unit_definition default_unit =
{
	static_cast<file_tag>(-1),
	0,
	{	{ static_cast<file_tag>(-1), -1, { 0xffff, 0, 0, 0 }, { 0, 0, 0xffff, 0 } },
		{ static_cast<file_tag>(-1), -1, { 0xffff, 0, 0, 0 }, { 0, 0, 0xffff, 0 } },
		{ static_cast<file_tag>(-1), -1, { 0xffff, 0, 0, 0 }, { 0, 0, 0xffff, 0 } },
		{ static_cast<file_tag>(-1), -1, { 0xffff, 0, 0, 0 }, { 0, 0, 0xffff, 0 } }	},
	0, 0, 0, 0, 0, 0, 0,
	0,
	static_cast<file_tag>(-1),
	0,
	0
};

// ==============================  EDIT WINDOW  ========================================
void edit_unit( tag_entry* e )
{

}

void defaultnew_unit( tag_entry *e )
{
	unit_definition def;
	byte_swap_move_be( "unit", &def, &default_unit, sizeof(unit_definition), 1, unit_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_unit( tag_entry *e )
{
	unit_definition def;
	read_unit_definition( &def, e );
	add_tag_dependency( 'mons', def.monster_tag );
	
	for( int ii = 0; ii < MAXIMUM_CUSTOMIZATIONS_PER_UNIT; ii++ )
		add_tag_dependency( 'txst', def.custom_colors[ii].logo_txst_tag );
}

void update_unit( tag_entry *e )
{
	if (get_entry_version( e ) != 2)
	{
		unit_definition def;
		read_unit_definition( &def, e );
		def.number_of_customizations = 0;
		memcpy( def.custom_colors, default_unit.custom_colors, sizeof(def.custom_colors) );
		save_unit_definition( &def, e );
		set_entry_version( e, 2 );
	}
}

// ==============================  READ TAG DATA  ========================================
bool read_unit_definition( unit_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "unit", e, def, SIZEOF_STRUCT_UNIT_DEFINITION, 1, unit_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_unit_definition( unit_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "unit", e, def, SIZEOF_STRUCT_UNIT_DEFINITION, 1, unit_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_unit_definition( unit_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'unit', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_unit_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_unit_definition( unit_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'unit', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_unit_definition( def, e );
}

