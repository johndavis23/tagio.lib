// scen.h
#ifndef __scen_h__
#define __scen_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif


#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	SCENERY_DEFINITION_GROUP_TAG= 0x7363656e, // 'scen' (looks like text)
	SCENERY_DEFINITION_VERSION_NUMBER= 2,

	MAXIMUM_SCENERY_DEFINITIONS_PER_MAP= 32
};

/* ---------- definition */

enum // flags
{
	_scenery_is_solid_bit,
	_scenery_has_random_facing_bit,
	_scenery_is_large_object_bit,
	_scenery_is_netgame_flag_bit,
	_scenery_respects_monster_visibility_bit,
	_scenery_adjusts_model_permutation_bit,
	_scenery_animates_bit,
	_scenery_marks_terrain_impassable_bit,
	_scenery_vitality_affects_model_animation_bit,
	_scenery_destruction_starts_model_animation_bit,
	_scenery_is_skelmodel_bit,
	_scenery_uses_tree_physics_bit,
	_scenery_uses_tree_physics_when_ruined_bit,
	_scenery_can_be_activated_bit,
	_scenery_can_be_deactivated_bit,
	_scenery_is_initially_active_bit,
	
	NUMBER_OF_SCENERY_DEFINITION_FLAGS,

	_scenery_is_solid_flag= FLAG(_scenery_is_solid_bit),
	_scenery_has_random_facing_flag= FLAG(_scenery_has_random_facing_bit),
	_scenery_is_large_object_flag= FLAG(_scenery_is_large_object_bit),
	_scenery_is_netgame_flag_flag= FLAG(_scenery_is_netgame_flag_bit),
	_scenery_respects_monster_visibility_flag= FLAG(_scenery_respects_monster_visibility_bit),
	_scenery_adjusts_model_permutation_flag= FLAG(_scenery_adjusts_model_permutation_bit),
	_scenery_animates_flag= FLAG(_scenery_animates_bit),
	_scenery_marks_terrain_impassable_flag= FLAG(_scenery_marks_terrain_impassable_bit),
	_scenery_vitality_affects_model_animation_flag= FLAG(_scenery_vitality_affects_model_animation_bit),
	_scenery_destruction_starts_model_animation_flag= FLAG(_scenery_destruction_starts_model_animation_bit),
	_scenery_is_skelmodel_flag = FLAG(_scenery_is_skelmodel_bit),
	_scenery_uses_tree_physics_flag= FLAG(_scenery_uses_tree_physics_bit),
	_scenery_uses_tree_physics_when_ruined_flag= FLAG(_scenery_uses_tree_physics_when_ruined_bit),
	_scenery_can_be_activated_flag= FLAG(_scenery_can_be_activated_bit),
	_scenery_can_be_deactivated_flag= FLAG(_scenery_can_be_deactivated_bit),
	_scenery_is_initially_active_flag= FLAG(_scenery_is_initially_active_bit)
};

enum
{
	_scenery_impact_projectile_group= 0,
	_scenery_detonation_projectile_group,
	NUMBER_OF_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION,
	MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION= 4,
	
	_scenery_sequence_undamaged= 0,
	_scenery_sequence_ruined,
	NUMBER_OF_SEQUENCES_PER_SCENERY_DEFINITION,
	MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION= 3
};

#define SIZEOF_STRUCT_SCENERY_DEFINITION 128
struct scenery_definition
{
	unsigned long flags; 
	
	file_tag collection_reference_tag;
	
	file_tag object_tag;
	file_tag projectile_tag; // if not NONE, this piece of scenery is a dormant projectile (.sequence_index will be ignored)
	
	short valid_netgame_scoring_type; // invalid if not a flag
	short netgame_flag_number; // invalid if not a flag

	short unused[2];
	
	file_tag projectile_group_tags[MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION];
	
	short sequence_indexes[MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION];
	
	short unused2[3];
	
	short model_permutation_delta;
	
	short unused3[1];
	
	file_tag skelmodel_tags[MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION];
	
	short unused4[19];
	
	short skelmodel_indices[MAXIMUM_SEQUENCES_PER_SCENERY_DEFINITION];
	short projectile_group_types[MAXIMUM_PROJECTILE_GROUPS_PER_SCENERY_DEFINITION];
	short collection_reference_index;
	short impact_projectile_group_type;
	short object_type;
	short projectile_type;
};


//=====================================================================================================
bool read_scenery_definition( scenery_definition* def, tag id );
bool save_scenery_definition( scenery_definition* def, tag id );

bool read_scenery_definition( scenery_definition* def, tag_entry* e );
bool save_scenery_definition( scenery_definition* def, tag_entry* e );


#endif