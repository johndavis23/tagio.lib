
#include "soli.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code sound_list_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// short number_of_sounds;
	_2byte,
	
	// short unused[7];
	7*sizeof(short),
	
	// struct sound_list_entry sounds[MAXIMUM_SOUNDS_PER_SOUND_LIST_DEFINITION];
	
		_begin_bs_array, MAXIMUM_SOUNDS_PER_SOUND_LIST_DEFINITION,
		
		// file_tag tag;
		_4byte,
		
		// short unused[1];
		// short index;
		2*sizeof(short),
		
		_end_bs_array,
	
	_end_bs_array
};

sound_list_definition default_soli =
{
	0,
	0, 0, 0, 0, 0, 0, 0,
	// sound list entries
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0,
	static_cast<file_tag>(-1), 0, 0
};

// ==============================  EDIT WINDOW  ========================================
void edit_soli( tag_entry* e )
{

}

void defaultnew_soli( tag_entry *e )
{
	sound_list_definition def;
	byte_swap_move_be( "soli", &def, &default_soli, sizeof(sound_list_definition), 1, sound_list_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_soli( tag_entry *e )
{
	sound_list_definition def;
	read_sound_list_definition( &def, e );
	for (int i = 0; i < def.number_of_sounds; ++i)
	{
		add_tag_dependency( 'soun', def.sounds[i].tag );
	}
}

// ==============================  READ TAG DATA  ========================================
bool read_sound_list_definition( sound_list_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "soli", e, def, SIZEOF_STRUCT_SOUND_LIST_DEFINITION, 1, sound_list_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_sound_list_definition( sound_list_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "soli", e, def, SIZEOF_STRUCT_SOUND_LIST_DEFINITION, 1, sound_list_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_sound_list_definition( sound_list_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'soli', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_sound_list_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_sound_list_definition( sound_list_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'soli', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_sound_list_definition( def, e );
}



