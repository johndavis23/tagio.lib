#ifndef __BYTE_SWAPPING_H__
#define __BYTE_SWAPPING_H__

/*
BYTE_SWAPPING.H
Tuesday, August 22, 1995 9:20:34 AM  (Jason)
*/

#ifdef __cplusplus
extern "C" {
#endif

/* ---------- constants */

enum // byte_swap_code constants
{
	// positive numbers are runs (of bytes) to leave untouched
	// zero is invalid

	_byte= 1,
	_2byte= -2,
	_4byte= -4,
	
	_begin_bs_array= -100,
	_end_bs_array= -101
};

/* ---------- macros */

#define SWAP2(q) ((((unsigned short)(q))>>8) | ((((unsigned short)(q))<<8)&0xff00))
#define SWAP4(q) (((((unsigned long) (q)))>>24) | ((((unsigned long) (q))>>8)&0xff00) | ((((unsigned long) (q))<<8)&0xff0000) | ((((unsigned long) (q))<<24)&0xff000000))

/* ---------- types */

typedef short byte_swap_code;

/* ---------- prototypes/BYTE_SWAPPING.H */

void byte_swap_memory(char *name, void *memory, int count, byte_swap_code code);
void byte_swap_data(char *name, void *data, int data_size, int data_count, byte_swap_code *codes);
void byte_swap_move(char *name, void *destination, const void *source, int data_size, int data_count, byte_swap_code *codes);

#ifdef __powerc
#define byte_swap_memory_be( name, memory, count, code ) ((void)0)
#define byte_swap_data_be( name, data, data_size, data_count, codes ) ((void)0)
#define byte_swap_move_be( name, destination, source, data_size, data_count, codes ) memcpy( (destination), (source), (data_size) * (data_count) )
#define byte_swap_memory_le( name, memory, count, code )  byte_swap_memory( (name), (memory), (count), (code) )
#define byte_swap_data_le( name, data, data_size, data_count, codes ) byte_swap_data( (name), (data), (data_size), (data_count), (codes) )
#define byte_swap_move_le( name, destination, source, data_size, data_count, codes ) byte_swap_move( (name), (destination), (source), (data_size), (data_count), (codes) )
#else
#define byte_swap_memory_be( name, memory, count, code )  byte_swap_memory( (name), (memory), (count), (code) )
#define byte_swap_data_be( name, data, data_size, data_count, codes ) byte_swap_data( (name), (data), (data_size), (data_count), (codes) )
#define byte_swap_move_be( name, destination, source, data_size, data_count, codes ) byte_swap_move( (name), (destination), (source), (data_size), (data_count), (codes) )
#define byte_swap_memory_le( name, memory, count, code ) ((void)0)
#define byte_swap_data_le( name, data, data_size, data_count, codes ) ((void)0)
#define byte_swap_move_le( name, destination, source, data_size, data_count, codes ) memcpy( (destination), (source), (data_size) * (data_count) )
#endif

#ifdef __cplusplus
}
#endif

#endif // __BYTE_SWAPPING_H__
