
#include "arti.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 

// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code artifact_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	
	// file_tag monster_restriction_tag; // or NONE
	// short specialization_restriction; // bow, sword, etc ... or NONE
	_4byte,
	_2byte,
	
	// short initial_charges_lower_bound, initial_charges_delta; // not displayed if zero
	_2byte, _2byte,
	
	// word pad;
	sizeof(word),
	
	// file_tag collection_tag;
	// short sequence_indexes[MAXIMUM_NUMBER_OF_ARTIFACT_SEQUENCES];
	_4byte,
	_begin_bs_array, MAXIMUM_NUMBER_OF_ARTIFACT_SEQUENCES, _2byte, _end_bs_array,
	
	// file_tag projectile_tags[MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES];
	_begin_bs_array, MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES, _4byte, _end_bs_array,
	
	// unsigned long bonus_monster_flags;
	// unsigned long bonus_terrain_flags;
	_4byte,
	_4byte,
	
	// short_fixed bonus_effect_modifiers[MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS];
	_begin_bs_array, MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS, _2byte, _end_bs_array,
		
	// struct monster_attack_definition override_attack;
	SIZEOF_STRUCT_MONSTER_ATTACK_DEFINITION, // byteswap it later

	// file_tag special_ability_string_list_tag;
	_4byte,
	
	// short unused[2];
	2*sizeof(short),
	
	// short collection_index;
	// short special_ability_string_list_index;
	// short projectile_types[MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES];
	2*sizeof(short),
	MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES*sizeof(short),

	_end_bs_array
};

artifact_definition default_arti =
{
	0,
	static_cast<file_tag>(-1),
	0,
	0, 0,
	0,
	static_cast<file_tag>(-1),
	0, 0, 0, 0,
	static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1), static_cast<file_tag>(-1),
	0,
	0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// monster attack definition
	0,
	0,
	static_cast<file_tag>(-1),
	0, 0,
	// monster attack animation
	0,
	0,
	0,
	0,
	// end monster attack animation
	0,
	0, 0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	// end monster attack definition
	static_cast<fixed_fraction>(-1),
	0, 0,
	0,
	0,
	0, 0, 0, 0
};

// ==============================  EDIT WINDOW  ========================================
void edit_arti( tag_entry* e )
{

}

void defaultnew_arti( tag_entry *e )
{
	artifact_definition def;
	
	byte_swap_move_be( "arti", &def, &default_arti, sizeof(artifact_definition), 1, artifact_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_arti( tag_entry *e )
{
	artifact_definition def;
	read_artifact_definition( &def, e );
	add_tag_dependency( '.256', def.collection_tag );
	for (int i = 0; i < MAXIMUM_NUMBER_OF_ARTIFACT_PROJECTILES; ++i)
	{
		add_tag_dependency( 'proj', def.projectile_tags[i] );
	}
	add_tag_dependency( 'stli', def.special_ability_string_list_tag );
}


// ==============================  READ TAG DATA  ========================================
bool read_artifact_definition( artifact_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "arti", e, def, SIZEOF_STRUCT_ARTIFACT_DEFINITION, 1, artifact_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_artifact_definition( artifact_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "arti", e, def, SIZEOF_STRUCT_ARTIFACT_DEFINITION, 1, artifact_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_artifact_definition( artifact_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'arti', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_artifact_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_artifact_definition( artifact_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'arti', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_artifact_definition( def, e );
}


