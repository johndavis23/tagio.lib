
#include <stdlib.h>
#include "memalign.h"

void *alloc_align( int size, int align )
{
	void *rawptr, *aptr;
	
	size += sizeof( void * ) + (align - 1);
	rawptr = malloc( size );
	if (!rawptr) return 0;
	aptr = (void *)( (((size_t)rawptr) + sizeof(void *) + (align - 1)) & (~(align - 1)) );
	((void **)aptr)[-1] = rawptr;
	return aptr;
}


void free_align( void *ptr )
{
	free( ((void **)ptr)[-1] );
}