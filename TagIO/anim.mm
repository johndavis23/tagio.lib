
#include "anim.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
static byte_swap_code animation_shadow_map_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	//short width, height;
	_2byte, _2byte,

	//short bytes_per_row;
	//word pad;
	_2byte,
	sizeof(short),

	//byte *shadow_map;
	_4byte,
	
	_end_bs_array
};

static byte_swap_code model_animation_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	
	// short number_of_frames;
	// short ticks_per_frame;
	_2byte,
	_2byte,
	
	// struct model_animation_frame_definition[MAXIMUM_FRAMES_PER_ANIMATION];
	
		_begin_bs_array, MAXIMUM_FRAMES_PER_ANIMATION,
		
		// unsigned long flags;
		_4byte,
		
		// file_tag model_tag;
		// short model_index; // postprocessed
		_4byte,
		sizeof(short),

		// short permutation_index;
		_2byte,

		// word unused[2];
		2*sizeof(short),
		
		_end_bs_array,
	
	// word unused[222];
	222*sizeof(short),
	
	// short shadow_map_width, shadow_map_height;
	// short shadow_bytes_per_row;
	// word pad;
	_2byte, _2byte,
	_2byte,
	sizeof(short),

	// short origin_offset_x, origin_offset_y;
	_2byte, _2byte,

	// file_tag forward_sound_tag;
	// file_tag backward_sound_tag;
	_4byte,
	_4byte,
	
	// short forward_sound_type;
	// short backward_sound_type;
	2*sizeof(short),

	// word unused2[20];
	20*sizeof(short),

	// int shadow_maps_offset, shadow_maps_size;
	// byte *shadow_maps;
	_4byte, _4byte,
	_4byte,
	
	_end_bs_array
};


// ==============================  EDIT WINDOW  ========================================
void edit_anim( tag_entry* e )
{

}


// ==============================  READ TAG DATA  ========================================
bool read_model_animation_definition( model_animation_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "anim", e, def, SIZEOF_STRUCT_MODEL_ANIMATION_DEFINITION, 1, model_animation_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_model_animation_definition( model_animation_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "anim", e, def, SIZEOF_STRUCT_MODEL_ANIMATION_DEFINITION, 1, model_animation_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_model_animation_definition( model_animation_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'anim', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_model_animation_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_model_animation_definition( model_animation_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'anim', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_model_animation_definition( def, e );
}


