
#include "mesh.h"

 #include <string.h>

 
#include "byte_swapping.h"
#include "myth_defs.h"
#include "tagpick.h"
#include "map_actions.h"
#include "markers.h"
#include <assert.h>


// ==============================  BYTE SWAPPING DATA  ========================================
byte_swap_code mesh_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// file_tag landscape_collection_tag; // .256 tag for color and shadow maps
	// file_tag media_collection_tag;
	_4byte, _4byte,
	
	// short submesh_width, submesh_height;
	// long mesh_offset, mesh_size;
	// void *mesh;
	_2byte, _2byte,
	_4byte, _4byte,
	sizeof(void *),

	// long data_offset, data_size;
	// void *data;
	_4byte, _4byte,
	sizeof(void *),
	
	// long model_count, models_offset, models_size;
	// void *models; // new_model_data structures which reference 'mode' tags
	_4byte, _4byte, _4byte,
	sizeof(void *),
	
	// long marker_count, markers_offset, markers_size; // array of marker_data structures
	// void *markers; // combined with marker list in scenario_header
	_4byte, _4byte, _4byte,
	sizeof(void *),
	
	// file_tag mesh_lighting_tag; // shadow intensity, ambient light, etc.
	// file_tag connector_tag;
	_4byte,
	_4byte,
	
	// unsigned long flags;
	_4byte,
	
	// file_tag particle_system_tag;
	_4byte,

	// teams may be played by any number of players	
	// long team_count;
	_4byte,

	// short_fixed dark_fraction;
	// short_fixed light_fraction;
	_2byte,
	_2byte,
	// struct rgb_color dark_color;
	// struct rgb_color light_color;
	_2byte, _2byte, _2byte, _2byte,
	_2byte, _2byte, _2byte, _2byte,
	// fixed transition_point;
	_4byte,

	// short_world_distance ceiling_height;
	_2byte,
	// short unused1;
	sizeof(short),

	// rectangle2d edge_of_mesh_buffer_zones;
	_2byte, _2byte, _2byte, _2byte,

	// file_tag global_ambient_sound_tag;
	_4byte,

	// long map_action_count, map_actions_offset, map_action_buffer_size;
	_4byte, _4byte, _4byte,

	// file_tag map_description_string_list_tag;
	// file_tag postgame_collection_tag;
	// file_tag pregame_collection_tag;
	// file_tag overhead_map_collection_tag;
	_4byte,
	_4byte,
	_4byte,
	_4byte,
	// file_tag next_mesh_tags[NUMBER_OF_NEXT_MESH_TAGS];
	// file_tag cutscene_movie_tags[NUMBER_OF_CUTSCENE_MOVIE_TAGS];
	// file_tag storyline_string_tags[NUMBER_OF_STORYLINE_STRING_TAGS];
	_begin_bs_array, NUMBER_OF_NEXT_MESH_TAGS, _4byte, _end_bs_array,
	_begin_bs_array, NUMBER_OF_CUTSCENE_MOVIE_TAGS, _4byte, _end_bs_array,
	_begin_bs_array, NUMBER_OF_STORYLINE_STRING_TAGS, _4byte, _end_bs_array,

	// long media_coverage_region_offset, media_coverage_region_size;
	// short *media_coverage_region;
	_4byte, _4byte,
	sizeof(short *),
	
	// long mesh_LOD_data_offset, mesh_LOD_data_size;
	// byte *mesh_LOD_data;
	_4byte, _4byte,
	sizeof(word *),

	// struct rgb_color global_tint_color;
	// short_fixed global_tint_fraction;
	_2byte, _2byte, _2byte, _2byte,
	_2byte,
	// word pad;
	sizeof(short),

 	// file_tag wind_tag;
	_4byte,

	// file_tag screen_collection_tags[NUMBER_OF_SCREEN_COLLECTION_TAGS];
	_begin_bs_array, NUMBER_OF_SCREEN_COLLECTION_TAGS, _4byte, _end_bs_array,
	
	// struct rgb_color blood_color;
	_2byte, _2byte, _2byte, _2byte,

	// file_tag picture_caption_string_list_tag;
	// file_tag narration_sound_tag;
	// file_tag win_ambient_sound_tag;
	// file_tag loss_ambient_sound_tag;
	_4byte,
	_4byte,
	_4byte,
	_4byte,

	// For Creative's Environmental Audio Extensions
	// unsigned long reverb_environment;
	// float reverb_volume;
	// float reverb_decay_time;
	// float reverb_damping;
	_4byte,
	_4byte,
	_4byte,
	_4byte,

	// long connector_count, connectors_offset, connectors_size; // array of connectors (i.e. fences)
	// struct saved_connector_data *connectors;
	_4byte, _4byte, _4byte,
	_4byte,
	
	// char cutscene_names[NUMBER_OF_CUTSCENE_MOVIE_TAGS][64];
	64*NUMBER_OF_CUTSCENE_MOVIE_TAGS*sizeof(char),

	// file_tag hints_string_list_tag;
	_4byte,
	
	// struct rgb_color fog_color;
	// float fog_density;
	_2byte, _2byte, _2byte, _2byte,
	_4byte,

	// file_tag difficulty_level_override_string_list_tag;
	_4byte,

	// joe added these fields:
	//short new_field_block_valid_code; // must == 0xFEDC
	_2byte,
	
	// short padd;
	_2byte,

	// file_tag detail_stack_tag;
	_4byte,
	// file_tag environment_texture_tag;
	_4byte,
	// file_tag ground_sprites_tag;
	_4byte,
	// file_tag level_picture_tag;
	_4byte,
	// file_tag collapsed_detail_stack_tag;
	_4byte,
	
	_4byte, _4byte, _4byte, _4byte,
	_4byte, _4byte, _4byte, _4byte,
	_4byte, _4byte,
	_2byte, _2byte,
	_4byte, _4byte, _4byte, _4byte,
	
	//sizeof(int),
	
	// short unused[226];
	196*sizeof(short),
	
	// short connector_type;
	// short map_description_string_index;
	// short overhead_map_collection_index;
	// short landscape_collection_index;
	// short detail_textures_index;
	// short environment_texture_index;
	// short global_ambient_sound_index;
	// short media_collection_index;
	// short hints_string_list_index;
	// short padding
	10*sizeof(short),
	
	// byte editor_data[MESH_HEADER_EDITOR_DATA_SIZE];
	MESH_DEFINITION_EDITOR_DATA_SIZE,
	
	_end_bs_array
};


byte_swap_code mesh_cell_bs_data[]=
{
	_begin_bs_array, 1,
	
	// short_world_distance physical_height; // (might have a few free bits here)
	// word normal; // two 8-bit indexes into the precalculated normal table
	// word flags; // includes terrain type (for pathfinding)
	_2byte,
	_2byte,
	_2byte,

	// short first_object_index; // postprocessed
	sizeof(short),
	
	// short_world_distance media_height;
	_2byte,

	// short_world_distance render_height;
	_2byte,
	
	_end_bs_array
};


// ==============================  READ TAG DATA  ========================================
bool read_mesh_definition( mesh_definition* def, tag_entry* e )
{
	assert( sizeof(mesh_definition) == 1024 );
	return load_tag_data_into_struct( "mesh", e, def, SIZEOF_STRUCT_MESH_DEFINITION, 1, mesh_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_mesh_definition( mesh_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "mesh", e, def, SIZEOF_STRUCT_MESH_DEFINITION, 1, mesh_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_mesh_definition( mesh_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'mesh', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_mesh_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_mesh_definition( mesh_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'mesh', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_mesh_definition( def, e );
}



// ==============================  EDIT WINDOW  ========================================
void edit_mesh_header( tag_entry* e )
{

}


void dependency_mesh( tag_entry *e )
{
    int i, j;
    
    // first the header
    mesh_definition def;
    read_mesh_definition( &def, e );
    
    add_tag_dependency( 'txst', def.landscape_texturestack_tag );
    add_tag_dependency( 'txst', def.detail_stack_tag );
    add_tag_dependency( 'grsp', def.ground_sprites_tag );
    add_tag_dependency( 'txst', def.collapsed_detail_stack_tag );
    add_tag_dependency( 'medi', def.media_tag );
    add_tag_dependency( 'glli', def.mesh_lighting_tag );
    add_tag_dependency( 'txst', def.environment_texture_tag );
    add_tag_dependency( 'part', def.particle_system_tag );
    add_tag_dependency( 'amso', def.global_ambient_sound_tag );
    add_tag_dependency( 'stli', def.map_description_string_list_tag );
    add_tag_dependency( 'wind', def.wind_tag );
    add_tag_dependency( 'soun', def.narration_sound_tag );
    add_tag_dependency( 'conn', def.connector_tag );
    add_tag_dependency( 'stli', def.hints_string_list_tag );
    
    add_tag_dependency( '.256', def.pregame_collection_tag );
    add_tag_dependency( '.256', def.overhead_map_collection_tag );
    add_tag_dependency( '.256', def.postgame_collection_tag );
    
    add_tag_dependency( 'text', def.storyline_string_tags[_storyline_pregame] );
    add_tag_dependency( 'stli', def.picture_caption_string_list_tag );
    
    add_tag_dependency( 'jpeg', def.postgame_pics[0] );
    add_tag_dependency( 'jpeg', def.postgame_pics[1] );
    add_tag_dependency( 'text', def.postgame_text[0] );
    add_tag_dependency( 'text', def.postgame_text[1] );
    add_tag_dependency( 'quik', def.postgame_mp3[0] );
    add_tag_dependency( 'quik', def.postgame_mp3[1] );
    
    add_tag_dependency( '.256', def.level_picture_tag );
    
    // now the markers
    const byte *raw = (const byte *)get_entry_data( e );
    for (i = 0; i < def.marker_palette_entries; ++i)
    {
        marker_palette_entry m;
        byte_swap_move_be( "marker palette", &m, raw + def.data_offset + def.marker_palette_offset + i * sizeof(marker_palette_entry), sizeof(marker_palette_entry), 1, marker_palette_data_bs_data );
        switch (m.type)
        {
            case _marker_scenery:
                add_tag_dependency( 'scen', m.tag );
                break;
            case _marker_monster:
                add_tag_dependency( 'unit', m.tag );
                break;
            case _marker_sound_source:
                add_tag_dependency( 'amso', m.tag );
                break;
            case _marker_projectile:
                add_tag_dependency( 'proj', m.tag );
                break;
            case _marker_local_projectile_group:
                add_tag_dependency( 'lpgr', m.tag );
                break;
        }
    }
    
    // now the script
    const byte *parameter_start = raw + def.data_offset + def.map_actions_offset + def.map_action_count * sizeof(map_action);
    const byte *parameters_end = raw + def.data_offset + def.map_actions_offset + def.map_action_buffer_size;
    while (parameter_start < parameters_end)
    {
        map_action_parameter_header ph;
        byte_swap_move_be( "map action parameter header", &ph, parameter_start,
                          sizeof(ph), 1, map_action_parameter_header_byte_swap_data );
        parameter_start += sizeof(ph);
        file_tag type;
        int size;
        
        switch (ph.type)
        {
            case _parameter_flag:
                type = 0; size = 1; break;
                
            case _parameter_character:
                type = 0; size = 1; break;
                
            case _parameter_monster_identifier:
                type = 0; size = 2; break;
                
            case _parameter_action_identifier:
                type = 0; size = 2; break;
                
            case _parameter_angle:
                type = 0; size = 2; break;
                
            case _parameter_integer:
                type = 0; size = 4; break;
                
            case _parameter_world_distance:
                type = 0; size = 4; break;
                
            case _parameter_field_name:
                type = 0; size = 4; break;
                
            case _parameter_fixed:
                type = 0; size = 4; break;
                
            case _parameter_projectile_group_tag:
                type = 'prgr'; size = 4; break;
                
            case _parameter_string_list_tag:
                type = 'stli'; size = 4; break;
                
            case _parameter_sound_tag:
                type = 'soun'; size = 4; break;
                
            case _parameter_projectile_tag:
                type = 'proj'; size = 4; break;
                
            case _parameter_world_point2d:
                type = 0; size = 8; break;
                
            case _parameter_world_rectangle2d:
                type = 0; size = 16; break;
                
            case _parameter_object_identifier:
                type = 0; size = 2; break;
                
            case _parameter_model_identifier:
                type = 0; size = 2; break;
                
            case _parameter_sound_source_identifier:
                type = 0; size = 2; break;
                
            case _parameter_world_point3d:
                type = 0; size = 12; break;
                
            case _parameter_local_projectile_group_identifier:
                type = 0; size = 2; break;
                
            case _parameter_model_animation_identifier:
                type = 0; size = 2; break;
                
            case _parameter_scenery_identifier:
                type = 0; size = 2; break;
                
            case _parameter_scenery_tag:
                type = 'scen'; size = 4; break;
                
            default:
                type = 0; size = parameters_end - parameter_start; ph.count = 1; break;
        }
        
        if (type)
        {
            for (j = 0; j < ph.count; ++j)
            {
                file_tag id;
               // byte_swap_move_be( "map action parameter element", &id, parameter_start, //TODO SCRIPT DEPENDENCY
               //                   sizeof(id), 1, long_byte_swap_data );
              //  add_tag_dependency( type, id );
                parameter_start += 4;
            }
        }
        else
        {
            // it seems as parameters are dword aligned
            parameter_start += (((size * ph.count) + 3) & 0xfffffffc);
        }
    }
    
    // do this LAST to optimize the file layout
    for (i = 0; i < NUMBER_OF_NEXT_MESH_TAGS; ++i)
    {
        add_tag_dependency( 'mesh', def.next_mesh_tags[i] );
    }
}
struct XPoint{short h; short v;};
void new_mesh( tag_entry *e ) //TODO New Mesh
{
     XPoint size;
    size.h = 4;
    size.v = 4;
//	if (XGDialog::ModalDialog( 169, NewMeshDlogProc, &size ))
	{
		mesh_definition h;
		int i;
		
		memset( &h, 0, sizeof(h) );
		
		h.landscape_texturestack_tag = -1;
		h.media_tag = -1;
		
		h.submesh_width = size.h / 32;
		h.submesh_height = size.v / 32;
		
		h.mesh_offset = 0;
		h.mesh_size = size.h * size.v * sizeof(mesh_cell);
		
		// write data_offset and data.size last
		
		h.marker_palette_entries = 0;
		h.marker_palette_offset = h.mesh_offset + h.mesh_size;
		h.marker_palette_size = h.marker_palette_entries * sizeof(marker_palette_entry);
		
		h.marker_count = 0;
		h.markers_offset = h.marker_palette_offset + h.marker_palette_size;
		h.markers_size = h.marker_count * sizeof(marker_data);
		
		h.mesh_lighting_tag = -1;
		h.connector_tag = -1;
		
		h.flags = 0;
		
		h.particle_system_tag = -1;
		h.team_count = 2;
		
		h.ceiling_height = FLOAT_TO_WORLD(32.0);
		
		h.edge_of_mesh_buffer_zones.top = 32;
		h.edge_of_mesh_buffer_zones.left = 32;
		h.edge_of_mesh_buffer_zones.bottom = 32;
		h.edge_of_mesh_buffer_zones.right = 32;
		
		h.global_ambient_sound_tag = -1;
		h.map_action_count = 0;
		h.map_actions_offset = h.markers_offset + h.markers_size;
		h.map_action_buffer_size = 0;
		
		h.map_description_string_list_tag = -1;
		h.postgame_collection_tag = -1;
		h.pregame_collection_tag = -1;
		h.overhead_map_collection_tag = -1;
		for (i = 0; i < NUMBER_OF_NEXT_MESH_TAGS; ++i)
		{
			h.next_mesh_tags[i] = -1;
		}
		for (i = 0; i < NUMBER_OF_CUTSCENE_MOVIE_TAGS; ++i)
		{
			h.cutscene_movie_tags[i] = -1;
		}
		for (i = 0; i < NUMBER_OF_STORYLINE_STRING_TAGS; ++i)
		{
			h.storyline_string_tags[i] = -1;
		}
		
		h.media_coverage_region_offset = h.map_actions_offset + h.map_action_buffer_size;
		h.media_coverage_region_size = 0;
		
		h.mesh_LOD_data_offset = h.media_coverage_region_offset + h.media_coverage_region_size;
		h.mesh_LOD_data_size = 0;
		
		h.wind_tag = -1;
		
		for (i = 0; i < NUMBER_OF_SCREEN_COLLECTION_TAGS; ++i)
		{
			h.screen_collection_tags[i] = -1;
		}
		
		h.picture_caption_string_list_tag = -1;
		h.narration_sound_tag = -1;
		h.win_ambient_sound_tag = -1;
		h.loss_ambient_sound_tag = -1;
		
		h.connector_count = 0;
		h.connectors_offset = h.mesh_LOD_data_offset + h.mesh_LOD_data_size;
		h.connectors_size = 0;
		
		h.hints_string_list_tag = -1;
		h.difficulty_level_override_string_list_tag = -1;
		
		h.new_field_block_valid_code = 0xFEDC;
		h.detail_stack_tag = -1;
		h.environment_texture_tag = -1;
		h.ground_sprites_tag = -1;
		h.level_picture_tag = -1;
		h.collapsed_detail_stack_tag = -1;
		
		h.postgame_pics[0] = h.postgame_pics[1] = -1;
		h.postgame_text[0] = h.postgame_text[1] = -1;
		h.postgame_mp3[0] = h.postgame_mp3[1] = -1;
		h.postgame_speed[0] = h.postgame_speed[1] = 1;
		h.prologue_quik[0] = h.prologue_quik[1] = h.prologue_quik[0] = h.prologue_quik[1] = -1;
		
		// write this last
		h.data_offset = sizeof(mesh_definition);
		h.data_size = h.connectors_offset + h.connectors_size;
		
		mesh_cell c;
		memset( &c, 0, sizeof(c) );
		
		c.physical_height = FLOAT_TO_WORLD(0.0f);
		c.normal = 00;
		c.flags = (_terrain_grass << 0) + (_terrain_grass << TERRAIN_TYPE_BITS);
		c.first_object_index = -1;
		c.media_height = FLOAT_TO_WORLD(0.0f);
		c.render_height = 0xffff;
		
		
		byte *b = (byte *)malloc( sizeof(h) + h.data_size );
		byte_swap_move_be( "mesh_header", b, &h, sizeof(h), 1, mesh_definition_byte_swapping_data );
		
		for (i = 0; i < size.h * size.v; ++i)
		{
			byte_swap_move_be( "mesh_cell", b + h.data_offset + h.mesh_offset + i * sizeof(c),
								&c, sizeof(c), 1, mesh_cell_bs_data );
		}
		
		set_entry_data( e, b, sizeof(h) + h.data_size, false );
		
		free( b );
		
		{
			//edit_mesh_header( e );
			//TTagEditForm *form = get_entry_form( e );
			//if (form)
			//{
			//	form->TagInfoChanged();
			//	form->ReloadTag();
			//}
		}
	}
}

