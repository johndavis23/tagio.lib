// canm.cpp
#include "canm.h"


#include <assert.h>
#include <string.h>

typedef struct ase_channel_s
{
	struct ase_channel_s* prev, *next;
	
	int tick;
	float q[4];
} ase_channel_t;

ase_channel_t* alloc_ase_channel( void )
{
	ase_channel_t* c = (ase_channel_t*)malloc(sizeof(ase_channel_t));
	if(c)
		c->next = 0;
	return c;
}

void free_channel_chain( ase_channel_t* chain )
{
	ase_channel_t* next;
	
	if( !chain )
		return;
		
	for( ; chain; chain = next )
	{
		next = chain->next;
		free(chain);
	}
}

byte_swap_code canm_int_codes[] =
{
	_begin_bs_array, 1,
	_4byte,
	_end_bs_array
};

byte_swap_code canm_q_codes[] =
{
	_begin_bs_array, 1,
	
	_4byte,
	_4byte,
	_4byte,
	_4byte,
	
	_end_bs_array
};
	
