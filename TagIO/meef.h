// meef.h
#ifndef __meef_h__
#define __meef_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif


#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	MESH_EFFECT_DEFINITION_GROUP_TAG= 0x6d656566, // 'meef' (looks like text)
	MESH_EFFECT_DEFINITION_VERSION_NUMBER= 1,
	
	MAXIMUM_MESH_EFFECT_DEFINITIONS_PER_MAP= 32
};


enum // mesh_effect definition flags
{
	_mesh_effect_is_inverted_bit,
	_mesh_effect_has_shockwave_bit,
	NUMBER_OF_MESH_EFFECT_DEFINITION_FLAGS,
	
	_mesh_effect_is_inverted_flag= FLAG(_mesh_effect_is_inverted_bit),
	_mesh_effect_has_shockwave_flag= FLAG(_mesh_effect_has_shockwave_bit)
};

#define SIZEOF_STRUCT_MESH_EFFECT_DEFINITION 36
struct mesh_effect_definition
{
	unsigned long flags;
	short_world_distance amplitude; // Max height of the wave
	
	short_world_distance cutoff_radius;
	short_fixed dissipation; // Effectively how thick the mesh wave is

	short_world_distance velocity;
	short_world_distance epicenter_offset; // the starting offset from the epicenter of the explosion

	short camera_shaking_type;

	file_tag collection_reference_tag;
	short sequence_index;
	short number_of_radial_points;

	short_world_distance shockwave_thickness;
	short_world_distance shockwave_height_offset;

	// computed during postprocessing
	
	short collection_reference_index;
	short collection_index, color_table_index;
	short pad;
};


//=====================================================================================================
bool read_mesh_effect_definition( mesh_effect_definition* def, tag id );
bool save_mesh_effect_definition( mesh_effect_definition* def, tag id );

bool read_mesh_effect_definition( mesh_effect_definition* def, tag_entry* e );
bool save_mesh_effect_definition( mesh_effect_definition* def, tag_entry* e );



#endif