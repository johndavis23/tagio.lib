
#include "obje.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
byte_swap_code object_definition_byte_swapping_data[]=
{
	_begin_bs_array, 1,
	
	// word flags;
	_2byte,
	
	// short_world_distance gravity; // world_distance/(ticks^2)
	// short_fixed elasticity; // how much velocity is preserved after a collision
	_2byte, _2byte,
	
	// short_world_distance terminal_velocity; // for gravity only
	_2byte,
	
	// short_fixed maximum_vitality, maximum_vitality_delta;
	// short_fixed scale_lower_bound, scale_delta;
	_2byte, _2byte,
	_2byte, _2byte,
	
	// short_fixed effect_modifiers[MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS];
	_begin_bs_array, MAXIMUM_NUMBER_OF_EFFECT_MODIFIERS, _2byte, _end_bs_array,
	
	// short_fixed minimum_damage; // how much damage must be done before it takes effect
	_2byte,

	// short unused[7];
	7*sizeof(short),

	_end_bs_array
};

object_definition default_obje =
{
	0,
	0,
	0,
	0,
	0, 0,
	0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0,
	0, 0, 0, 0, 0, 0, 0
};


// ==============================  EDIT WINDOW  ========================================
void edit_obje( tag_entry* e )
{

}

void defaultnew_obje( tag_entry *e )
{
	object_definition def;
	byte_swap_move_be( "obje", &def, &default_obje, sizeof(object_definition), 1, object_definition_byte_swapping_data );
	set_entry_data( e, &def, sizeof(def), false );
}

void dependency_obje( tag_entry *e )
{
    #pragma unused( e )

#if 0 // no dependencies
	object_definition def;
	read_object_definition( &def, e );
#endif
}


// ==============================  READ TAG DATA  ========================================
bool read_object_definition( object_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "obje", e, def, SIZEOF_STRUCT_OBJECT_DEFINITION, 1, object_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_object_definition( object_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "obje", e, def, SIZEOF_STRUCT_OBJECT_DEFINITION, 1, object_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_object_definition( object_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'obje', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_object_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_object_definition( object_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	e = get_entry( 'obje', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_object_definition( def, e );
}
