// amso.h
#ifndef __amso_h__
#define __amso_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	AMBIENT_SOUND_DEFINITION_GROUP_TAG= 'amso',
	AMBIENT_SOUND_DEFINITION_VERSION_NUMBER= 1,
	
	MAXIMUM_AMBIENT_SOUND_DEFINITIONS_PER_MAP= 32
};

/* ---------- ambient sound definition */

enum // constants
{
	_ambient_sound_background_sound= 0,
	_ambient_sound_random_sound,
	NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS,
	MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS= 6
};

enum // flags
{
	NUMBER_OF_AMBIENT_SOUND_DEFINITION_FLAGS // <=32
};

#define SIZEOF_STRUCT_AMBIENT_SOUND_DEFINITION 64
struct ambient_sound_definition
{
	unsigned long flags;
	
	short_world_distance inner_radius, outer_radius;
	
	short period_lower_bound, period_delta; // of random sound
	
	file_tag sound_tags[MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS];
	
	short_world_distance random_sound_radius;
	
	short unused[5];
	
	short sound_indexes[MAXIMUM_NUMBER_OF_AMBIENT_SOUND_DEFINITION_SOUNDS];
	
	short phase, period;
};


//=====================================================================================================
bool read_ambient_sound_definition( ambient_sound_definition* def, tag id );
bool save_ambient_sound_definition( ambient_sound_definition* def, tag id );

bool read_ambient_sound_definition( ambient_sound_definition* def, tag_entry* e );
bool save_ambient_sound_definition( ambient_sound_definition* def, tag_entry* e );


#endif