// damage.h
#ifndef __damage_h__
#define __damage_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

enum // damage types
{
	_damage_explosion,
	_damage_magical,
	_damage_hold,
	_damage_healing, // heals damage
	_damage_kinetic,
	_damage_steel_slashing,
	_damage_stoning, // turns to stone (and causes damage?)
	_damage_claws_slashing,
	_damage_explosive_electricity,
	_damage_fire,
	_damage_gas, // only damages monsters, can't cause hard death
	_damage_charm, // turns monster to caster's team
	_damage_tain
};

enum // damage definition flags
{
	_damage_proportional_to_velocity_bit,
	_damage_area_of_effect_bit,
	_damage_does_not_interrupt_events_bit,
	_damage_is_long_range_bit, // this damage was not delivered hand-to-hand

	_damage_can_cause_paralysis_bit,
	_damage_can_stun_bit,
	_damage_cannot_be_blocked_bit,
	_damage_cannot_be_healed_bit,

	_damage_does_not_effect_monsters_bit,
	_damage_detonates_explosives_immediately_bit,
	_damage_proportional_to_mana_bit,
	_damage_can_destroy_large_object_bit,
	
	_damage_instantaneous_bit,
	_damage_cannot_hurt_owner_bit,
	_damage_does_not_make_monsters_flinch_bit,
	_damage_can_cause_confusion_bit,

	NUMBER_OF_DAMAGE_DEFINITION_FLAGS,

	_damage_proportional_to_velocity_flag= FLAG(_damage_proportional_to_velocity_bit),
	_damage_area_of_effect_flag= FLAG(_damage_area_of_effect_bit),
	_damage_does_not_interrupt_events_flag= FLAG(_damage_does_not_interrupt_events_bit),
	_damage_is_long_range_flag= FLAG(_damage_is_long_range_bit),
	_damage_can_cause_paralysis_flag= FLAG(_damage_can_cause_paralysis_bit),
	_damage_can_stun_flag= FLAG(_damage_can_stun_bit),
	_damage_cannot_be_blocked_flag= FLAG(_damage_cannot_be_blocked_bit),
	_damage_cannot_be_healed_flag= FLAG(_damage_cannot_be_healed_bit),
	_damage_does_not_effect_monsters_flag= FLAG(_damage_does_not_effect_monsters_bit),
	_damage_detonates_explosives_immediately_flag= FLAG(_damage_detonates_explosives_immediately_bit),
	_damage_proportional_to_mana_flag= FLAG(_damage_proportional_to_mana_bit),
	_damage_can_destroy_large_object_flag= FLAG(_damage_can_destroy_large_object_bit),
	_damage_instantaneous_flag= FLAG(_damage_instantaneous_bit),
	_damage_cannot_hurt_owner_flag= FLAG(_damage_cannot_hurt_owner_bit),
	_damage_does_not_make_monsters_flinch_flag= FLAG(_damage_does_not_make_monsters_flinch_bit),
	_damage_can_cause_confusion_flag= FLAG(_damage_can_cause_confusion_bit)
};

#define SIZEOF_STRUCT_DAMAGE_DEFINITION 16
struct damage_definition
{
	short type;
	word flags;
	
	short_fixed damage_lower_bound, damage_delta;
	
	short_world_distance radius_lower_bound, radius_delta;
	short_world_distance rate_of_expansion; // world units per tick

	short_fixed damage_to_velocity;
};

enum // damage data flags
{
	_detonation_projectile_group_handled_by_obstacle_bit, // respect .detonation_projectile_group_type
	_damage_caused_constantly_bit,
	
	NUMBER_OF_DAMAGE_DATA_FLAGS,
	
	_detonation_projectile_group_handled_by_obstacle_flag= FLAG(_detonation_projectile_group_handled_by_obstacle_bit),
	_damage_caused_constantly_flag= FLAG(_damage_caused_constantly_bit)
};

#define SIZEOF_STRUCT_DAMAGE_DATA 36
struct damage_data
{
	short owner_unit_index; // the unit (cohort, regiment, etc.) who the owner belongs to (and thus player and team)
	short owner_monster_index; // the monster index (or NONE) which created this projectile
	short owner_monster_identifier; // the monster index (or NONE) which created this projectile
	
	short detonation_projectile_group_type;

	world_point3d epicenter; // center of object causing the damage
	world_vector3d vector; // velocity of object causing the damage
	
	short_fixed scale; // scale multiplier
	
	short_world_distance velocity;
	short_fixed damage;

	word flags;
	short_fixed mana_used;
};


//=====================================================================================================
bool read_damage_definition( damage_definition* def, tag id );
bool save_damage_definition( damage_definition* def, tag id );

bool read_damage_definition( damage_definition* def, tag_entry* e );
bool save_damage_definition( damage_definition* def, tag_entry* e );

#endif