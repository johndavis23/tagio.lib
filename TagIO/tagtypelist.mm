#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __tagprivate_h__
#include "tagprivate.h"
#endif

// originals
void edit_amso( tag_entry* e );
void defaultnew_amso( tag_entry *e );
void dependency_amso( tag_entry *e );

void edit_arti( tag_entry* e );
void defaultnew_arti( tag_entry *e );
void dependency_arti( tag_entry *e );

void edit_core( tag_entry* e );
void dependency_core( tag_entry *e );

void edit_conn( tag_entry* e );
void defaultnew_conn( tag_entry *e );
void dependency_conn( tag_entry *e );

void edit_form( tag_entry* e );
void defaultnew_form( tag_entry *e );
void dependency_form( tag_entry *e );

void edit_ligh( tag_entry* e );
void defaultnew_ligh( tag_entry *e );
void dependency_ligh( tag_entry *e );

void edit_phys( tag_entry* e );
void defaultnew_phys( tag_entry *e );
void dependency_phys( tag_entry *e );

void edit_lpgr( tag_entry* e );
void defaultnew_lpgr( tag_entry *e );
void dependency_lpgr( tag_entry *e );

void edit_medi( tag_entry* e );
void defaultnew_medi( tag_entry *e );
void dependency_medi( tag_entry *e );

void edit_meef( tag_entry* e );
void defaultnew_meef( tag_entry *e );
void dependency_meef( tag_entry *e );

void edit_meli( tag_entry* e );
void defaultnew_meli( tag_entry *e );
void dependency_meli( tag_entry *e );

void edit_mesh( tag_entry* e );
void new_mesh( tag_entry* e );
void edit_mesh_header( tag_entry* e );
void dependency_mesh( tag_entry *e );

//void edit_anim( tag_entry* e );
// waaay obsolete

void edit_mons( tag_entry* e );
void defaultnew_mons( tag_entry *e );
void dependency_mons( tag_entry *e );

void edit_obje( tag_entry* e );
void defaultnew_obje( tag_entry *e );
void dependency_obje( tag_entry *e );

void edit_obpc( tag_entry* e );
void defaultnew_obpc( tag_entry *e );
void dependency_obpc( tag_entry *e );

void edit_part( tag_entry* e );
void defaultnew_part( tag_entry *e );
void dependency_part( tag_entry *e );

void edit_prgr( tag_entry* e );
void defaultnew_prgr( tag_entry *e );
void dependency_prgr( tag_entry *e );

void edit_proj( tag_entry* e );
void defaultnew_proj( tag_entry *e );
void dependency_proj( tag_entry *e );
void update_proj( tag_entry *e );

void edit_scen( tag_entry* e );
void defaultnew_scen( tag_entry *e );
void dependency_scen( tag_entry *e );
void update_scen( tag_entry *e );

void edit_soli( tag_entry* e );
void defaultnew_soli( tag_entry *e );
void dependency_soli( tag_entry *e );

void edit_soun( tag_entry* e );
void defaultnew_soun( tag_entry *e );
void dependency_soun( tag_entry *e );

void edit_stli( tag_entry* e );
void defaultnew_stli( tag_entry *e );
void dependency_stli( tag_entry *e );

void edit_temp( tag_entry* e );
void defaultnew_temp( tag_entry *e );
void dependency_temp( tag_entry *e );

void edit_text( tag_entry* e );
void defaultnew_text( tag_entry *e );
void dependency_text( tag_entry *e );

void edit_unit( tag_entry* e );
void defaultnew_unit( tag_entry *e );
void dependency_unit( tag_entry *e );
void update_unit( tag_entry *e );

void edit_wind( tag_entry* e );
void defaultnew_wind( tag_entry *e );
void dependency_wind( tag_entry *e );

// brave new tags
void edit_texturestack( tag_entry *e );
void defaultnew_texturestack( tag_entry *e );
void dependency_texturestack( tag_entry *e );

void edit_skelmodel( tag_entry *e );
void defaultnew_skelmodel( tag_entry *e );
void dependency_skelmodel( tag_entry *e );

void edit_skelanim( tag_entry *e );
void defaultnew_skelanim( tag_entry *e );
void dependency_skelanim( tag_entry *e );
void update_skelanim( tag_entry *e );


void edit_loli( tag_entry* e );
void defaultnew_loli( tag_entry *e );
void dependency_loli( tag_entry *e );

void edit_glli( tag_entry* e );
void defaultnew_glli( tag_entry *e );
void dependency_glli( tag_entry *e );

void edit_grsp( tag_entry* e );
void defaultnew_grsp( tag_entry *e );
void dependency_grsp( tag_entry *e );

void edit_spri( tag_entry* e );
void defaultnew_spri( tag_entry *e );
void dependency_spri( tag_entry *e );

void edit_game( tag_entry *e );
void defaultnew_game( tag_entry *e );
void dependency_game( tag_entry *e );

tag_type_info tagtypelist[] = {
	// tag-a-cola classic
	{ 'amso', 1, "Ambient Sound", "Ambient Sounds", _tag_universal, edit_amso, NULL, defaultnew_amso, dependency_amso, NULL },
	{ 'arti', 1, "Artifact", "Artifacts", _tag_universal, edit_arti, NULL, defaultnew_arti, dependency_arti, NULL },
	{ 'core', 1, "Classic Collection Reference", "Classic Collection References", _tag_universal, NULL, NULL, NULL, dependency_core, NULL },
	{ '.256', 4, "Classic Collection", "Classic Collections", _tag_universal, NULL, NULL, NULL, NULL, NULL },
	{ 'conn', 1, "Connector", "Connectors", _tag_universal, edit_conn, NULL, defaultnew_conn, dependency_conn, NULL },
	{ 'ditl', 1, "Dialog String List", "Dialog String Lists", _tag_localized, NULL, NULL, NULL, NULL, NULL },
	{ 'bina', 1, "Dialog", "Dialogs", _tag_localized, NULL, NULL, NULL, NULL, NULL },
	{ 'font', 1, "Font", "Fonts", _tag_localized, NULL, NULL, NULL, NULL, NULL },
	{ 'form', 1, "Formation", "Formations", _tag_universal, edit_form, NULL, NULL, dependency_form, NULL },
	{ 'geom', 1, "Geometry", "Geometries", _tag_universal, NULL, NULL, NULL, NULL, NULL },
	{ 'inte', 1, "Interface", "Interface", _tag_universal, NULL, NULL, NULL, NULL, NULL },
	{ 'ligh', 1, "Lightning", "Lightning", _tag_universal, edit_ligh, NULL, defaultnew_ligh, dependency_ligh, NULL },
	{ 'phys', 1, "Local Physics", "Local Physics", _tag_universal, edit_phys, NULL, defaultnew_phys, dependency_phys, NULL },
	{ 'lpgr', 1, "Local Projectile Group", "Local Projectile Groups", _tag_universal, edit_lpgr, NULL, defaultnew_lpgr, dependency_lpgr, NULL },
	{ 'medi', 1, "Media Type", "Media Types", _tag_universal, edit_medi, NULL, defaultnew_medi, dependency_medi, NULL },
	{ 'meef', 1, "Mesh Effect", "Mesh Effects", _tag_universal, edit_meef, NULL, defaultnew_meef, dependency_meef, NULL },
	{ 'meli', 1, "Mesh Lighting", "Mesh Lighting", _tag_universal, edit_meli, NULL, defaultnew_meli, dependency_meli, NULL },
	{ 'mesh', 11, "Mesh", "Meshes", _tag_universal, edit_mesh, edit_mesh_header, new_mesh, dependency_mesh, NULL },
	{ 'anim', 1, "Classic Model Animation", "Classic Model Animations", _tag_universal, NULL, NULL, NULL, NULL, NULL },
	{ 'mode', 4, "Classic Model", "Classic Models", _tag_universal, NULL, NULL, NULL, NULL, NULL },
	{ 'mons', 1, "Monster", "Monsters", _tag_universal, edit_mons, NULL, defaultnew_mons, dependency_mons, NULL },
	{ 'obje', 1, "Object", "Objects", _tag_universal, edit_obje, NULL, defaultnew_obje, dependency_obje, NULL },
	{ 'obpc', 1, "Observer Constants", "Observer Constants", _tag_universal, edit_obpc, NULL, defaultnew_obpc, dependency_obpc, NULL },
	{ 'part', 1, "Particle System", "Particle Systems", _tag_universal, edit_part, NULL, defaultnew_part, dependency_part, NULL },
	{ 'pref', 1, "Preferences", "Preferences", _tag_localized, NULL, NULL, NULL, NULL, NULL },
	{ 'prel', 1, "Preloaded Data", "Preloaded Data", _tag_universal, NULL, NULL, NULL, NULL, NULL },
	{ 'prgr', 1, "Projectile Group", "Projectile Groups", _tag_universal, edit_prgr, NULL, defaultnew_prgr, dependency_prgr, NULL },
	{ 'proj', 2, "Projectile", "Projectiles", _tag_universal, edit_proj, NULL, NULL/*defaultnew_proj*/, dependency_proj, update_proj },
	{ 'reco', 1, "Recording", "Recordings", _tag_localized, NULL, NULL, NULL, NULL, NULL },
	{ 'save', 1, "Saved Game", "Saved Games", _tag_localized, NULL, NULL, NULL, NULL, NULL },
	{ 'scen', 2, "Scenery", "Scenery", _tag_universal, edit_scen, NULL, defaultnew_scen, dependency_scen, update_scen },
	{ 'soli', 1, "Sound List", "Sound Lists", _tag_localized, edit_soli, NULL, defaultnew_soli, dependency_soli, NULL },
	{ 'soun', 1, "Sound", "Sounds", _tag_universal, edit_soun, NULL, defaultnew_soun, dependency_soun, NULL },
	{ 'stli', 1, "String List", "String Lists", _tag_localized, edit_stli, NULL, defaultnew_stli, NULL /* dependency_stli */, NULL },
	{ 'temp', 1, "Template", "Templates", _tag_localized, edit_temp, NULL, defaultnew_temp, NULL /* dependency_temp */, NULL },
	{ 'text', 1, "Text", "Text", _tag_localized, edit_text, NULL, defaultnew_text, NULL /* dependency_text */, NULL },
	{ 'unit', 2, "Units", "Units", _tag_universal, edit_unit, NULL, defaultnew_unit, dependency_unit, update_unit },
	{ 'wind', 1, "Wind", "Wind", _tag_universal, edit_wind, NULL, defaultnew_wind, dependency_wind, NULL },
	// brave new tags
	{ 'txst', 1, "Texture Stack", "Texture Stacks", _tag_universal, edit_texturestack, NULL, defaultnew_texturestack, NULL /* dependency_texturestack */, NULL },
	{ 'skmd', 1, "Skeletal Model", "Skeletal Models", _tag_universal, edit_skelmodel, NULL, defaultnew_skelmodel, dependency_skelmodel, NULL },
	{ 'skan', 2, "Skeletal Animation", "Skeletal Animation", _tag_universal, edit_skelanim, NULL, defaultnew_skelanim, dependency_skelanim, update_skelanim },
	{ 'canm', 1, "Camera Animation", "Camera Animations", _tag_universal, NULL, NULL, NULL, NULL, NULL },
	{ 'loli', 1, "Local Light", "Local Lights", _tag_universal, edit_loli, NULL, defaultnew_loli, dependency_loli, NULL },
	{ 'glli', 1, "Global Light", "Global Lights", _tag_universal, edit_glli, NULL, defaultnew_glli, dependency_glli, NULL },
	{ 'grsp', 1, "Ground Sprites", "Ground Sprites", _tag_universal, edit_grsp, NULL, defaultnew_grsp, dependency_grsp, NULL },
	{ 'spri', 1, "Sprite Definition", "Sprite Definitions", _tag_universal, edit_spri, NULL, defaultnew_spri, dependency_spri, NULL },
	{ 'game', 1, "Game Parameter", "Game Parameters", _tag_localized, edit_game, NULL, defaultnew_game, dependency_game, NULL },
	{ 'rawd', 1, "Raw File Data", "Raw Files", _tag_universal, 0, 0, 0, 0, NULL },
	{ 'jpeg', 1, "JPEG File Data", "JPEG Files", _tag_universal, 0, 0, 0, 0, NULL },
	{ 'quik', 1, "Quicktime/MP3", "Quicktime/MP3", _tag_localized, 0, 0, 0, 0, NULL },
	{ 0, 0 } };

