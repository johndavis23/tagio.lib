
#ifndef __tagpick_h__
#define __tagpick_h__

#ifndef __tags_h__
#include "tags.h"
#endif


tag pick_tag_of_type( tag type, tag default_id, tag_file *same_file );

inline tag pick_tag_of_type( tag type, tag default_id, tag_entry *same_file_as_entry )
{
	return pick_tag_of_type( type, default_id, get_entry_file( same_file_as_entry ) );
}


#endif

