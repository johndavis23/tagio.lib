// medi.h
#ifndef __medi_h__
#define __medi_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	MEDIA_DEFINITION_GROUP_TAG= 'medi',
	MEDIA_DEFINITION_VERSION_NUMBER= 1,

	MAXIMUM_MEDIA_DEFINITIONS_PER_MAP= 2
};

/* ---------- media */

enum
{
	_media_does_not_reflect_bit,
	
	NUMBER_OF_MEDIA_DEFINITION_FLAGS,
	
	_media_does_not_reflect_flag= FLAG(_media_does_not_reflect_bit)
};

enum
{
	_media_projectile_group_small_splash= 0, // small object, rising out or sinking into
	_media_projectile_group_medium_splash, // etc..
	_media_projectile_group_large_splash,
	_media_projectile_group_bloody_splash, // blood entering and dissolving in the water
	_media_projectile_group_floating_splash, // smack (light, floating object)
	_media_projectile_group_steam_splash, // lit grenade
	_media_projectile_group_disturbance, // ripples only
	NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS,
	MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS= 16
};

#define SIZEOF_STRUCT_MEDIA_DEFINITION 256
struct media_definition
{
	unsigned long flags;
	
	file_tag collection_reference_tag;
	
	short unused[8];

	file_tag projectile_group_tags[MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS];

	short_fixed reflection_tint_fraction;
	
	struct rgb_color reflection_tint_color;
		
	fixed_fraction surface_effect_density; // percentage of maximum density
	file_tag surface_effect_local_projectile_group_tag;

	world_vector3d wobble_magnitude;
	world_vector3d wobble_phase_multiplier;

	short effects_per_cell;
	short time_between_building_effects;
	
	fixed_fraction reflection_transparency;
	
	short_fixed min_opacity;
	short_fixed max_opacity;
	short_fixed max_opacity_depth;
	fixed_fraction spec_min, spec_max;
	short_fixed emboss_fraction;
	
	struct {
		short_fixed wobble_scale;
		short_fixed wobble_speed;
		short_fixed base_scale[2];
		short_fixed base_speed[2];
	} texdata[2][2];
	
	short unused2[18];
	
	// computed during postprocessing

	short surface_effect_local_projectile_group; // The index for the group we actually created
	
	short surface_effect_local_projectile_group_type;
	short collection_reference_index;
	
	short projectile_group_types[MAXIMUM_NUMBER_OF_MEDIA_DEFINITION_PROJECTILE_GROUPS];
};


//=====================================================================================================
bool read_media_definition( media_definition* def, tag id );
bool save_media_definition( media_definition* def, tag id );

bool read_media_definition( media_definition* def, tag_entry* e );
bool save_media_definition( media_definition* def, tag_entry* e );



#endif