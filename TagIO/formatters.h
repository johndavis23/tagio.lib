/*
** FORMATTERS.H
*/
#ifndef __formatters_h__
#define __formatters_h__

#include <math.h>

// core math
float incremental_round( float value, float increment );

char get_delta( char lower, char upper );
char get_upper_bound( char lower, char delta );

unsigned char get_delta( unsigned char lower, unsigned char upper );
unsigned char get_upper_bound( unsigned char lower, unsigned char delta );

short get_delta( short lower, short upper );
short get_upper_bound( short lower, short delta );

unsigned short get_delta( unsigned short lower, unsigned short upper );
unsigned short get_upper_bound( unsigned short lower, unsigned short delta );

long get_delta( long lower, long upper );
long get_upper_bound( long lower, long delta );

unsigned long get_delta( unsigned long lower, unsigned long upper );
unsigned long get_upper_bound( unsigned long lower, unsigned long delta );

float get_delta( float lower, float upper );
float get_upper_bound( float lower, float delta );


float short_fixed_to_float( short_fixed val );
short_fixed float_to_short_fixed( float val );

float fixed_to_float( fixed val );
fixed float_to_fixed( float val );

float short_fixed_fraction_to_float( short_fixed_fraction val );
short_fixed_fraction float_to_short_fixed_fraction( float val );

float fixed_fraction_to_float( fixed_fraction val );
fixed_fraction float_to_fixed_fraction( float val );

float short_percentage_to_float( short_fixed_fraction val );
short_fixed_fraction float_to_short_percentage( float val );

float percentage_to_float( fixed_fraction val );
fixed_fraction float_to_percentage( float val );

float short_world_distance_to_float( short_world_distance val );
short_world_distance float_to_short_world_distance( float val );

float world_distance_to_float( world_distance val );
world_distance float_to_world_distance( float val );

float angle_to_float( angle val );
angle float_to_angle( float val );


inline short float_to_clamped_short( float f )
{
	int i = (int)round(f);
	if (i > 32767) i = 32767;
	if (i < -32767) i = -32767;
	return (short)i;
}



// GUI functions
char *format_float( double value, int precision );


#endif