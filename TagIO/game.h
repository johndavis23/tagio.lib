// game.h
#ifndef __game_h__
#define __game_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif

 
 

#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */
enum
{
	GAME_PARAMETERS_DEFINITION_GROUP_TAG = 'game',
	GAME_PARAMETERS_DEFINTION_VERSION_NUMBER = 1
};

/* ---------- game parameters definition */
#define SIZEOF_STRUCT_GAME_PARAMETERS_DEFINITION 32

struct game_parameters_definition
{
	file_tag training_map;
	
	file_tag first_map;
	
	file_tag default_netmap;
	
	short unused[10];
};


//=====================================================================================================
bool read_game_parameters_definition( game_parameters_definition* def, tag id );
bool save_game_parameters_definition( game_parameters_definition* def, tag id );

bool read_game_parameters_definition( game_parameters_definition* def, tag_entry* e );
bool save_game_parameters_definition( game_parameters_definition* def, tag_entry* e );

#endif // __game_h__
