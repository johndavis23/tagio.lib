// text.h
#ifndef __text_h__
#define __text_h__

#ifndef __tags_h__
#include "tags.h"
#endif

#ifndef __BYTE_SWAPPING_H__
#include "byte_swapping.h"
#endif


#ifndef __myth_defs_h__
#include "myth_defs.h"
#endif

/* ---------- constants */

enum
{
	STRING_DEFINITION_GROUP_TAG= 0x74657874, // 'text' (looks exactly like text)
	STRING_DEFINITION_VERSION_NUMBER= 1,
	MAXIMUM_STRING_DEFINITIONS_PER_MAP= 32,
	MAX_STRING_SIZE= 16384
};

struct string_definition
{
	long string_length;
	char string_buffer[MAX_STRING_SIZE];
};

//=====================================================================================================
bool read_string_definition( string_definition* def, tag id );
bool save_string_definition( string_definition* def, tag id );

bool read_string_definition( string_definition* def, tag_entry* e );
bool save_string_definition( string_definition* def, tag_entry* e );


#endif