
#include "damage.h"

 

 
 
#include "tagpick.h"
#include <assert.h>
 
 


// ==============================  BYTE SWAPPING DATA  ========================================
// Actually, the Byte Swapping occurs in the tag.  We shouldn't do it here.
static byte_swap_code damage_definition_byte_swapping_data[]=
{
	_end_bs_array
};



// ==============================  EDIT WINDOW  ========================================
void edit_damage( tag_entry* e )
{

}


// ==============================  READ TAG DATA  ========================================
bool read_damage_definition( damage_definition* def, tag_entry* e )
{
	return load_tag_data_into_struct( "damage", e, def, SIZEOF_STRUCT_DAMAGE_DEFINITION, 1, damage_definition_byte_swapping_data);
}


// ==============================  SAVE TAG DATA  ========================================
bool save_damage_definition( damage_definition* def, tag_entry* e )
{
	return save_tag_data_from_struct( "damage", e, def, SIZEOF_STRUCT_DAMAGE_DEFINITION, 1, damage_definition_byte_swapping_data);
}


// ==============================  READ TAG DATA (WITH ID) ========================================
bool read_damage_definition( damage_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	// CW9 doesn't like 'damage' so this is the hex equivalent
	e = get_entry(0x64616D616765, id);
//	e = get_entry( 'damage', id );
	if( 0 == e ) {
		return FALSE;
	}
	
	return read_damage_definition( def, e );	
}


// ==============================  SAVE TAG DATA (WITH ID) ========================================
bool save_damage_definition( damage_definition* def, tag id )
{
	tag_entry* e;
	
	assert( def );
	
	// CW9 doesn't like 'damage' so this is the hex equivalent
	e = get_entry(0x64616D616765, id);
//	e = get_entry( 'damage', id );
	if( 0 == e ) {
		return FALSE;
	}

	return save_damage_definition( def, e );
}


