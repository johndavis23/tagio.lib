
#include <stdio.h>
#include "fvec_altivec.h"

extern vector float *one, *two, *three;

fvec testfunc()
{
	fvec a = fv_abs(*one);//fv_neg(fv_one);
	fvec b = *two;//fv_half;
	fvec c = fv_clamp_neg(fv_add( a, b ));
	fvec d = fv_neg(fv_add( c, *three ));
	return fv_clamp_pos(fv_add( c, d ));
}

int main()
{
	fvec c = testfunc();
	printf( "c = %f", *(float *)&c );
}





