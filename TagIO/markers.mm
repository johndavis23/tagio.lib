
#ifndef __markers_h__
#include "markers.h"
#endif


byte_swap_code marker_data_bs_data[]=
{
	_begin_bs_array, 1,
	
	// unsigned long flags;
	_4byte,
	
	// short type; // _marker_observer, _marker_scenery, ...
	// short palette_index; // index into global marker palette list
	_2byte,
	_2byte,

	// short identifier;
	_2byte,
	
	// short minimum_difficulty_level;
	_2byte,
	
	// world_location3d location;

		// world_point3d position;
		// world_vector3d velocity;
		// short_world_distance height; // height above ground (or under media?)
		_4byte, _4byte, _4byte,
		_2byte, _2byte, _2byte,
		_2byte,
		
		// angle yaw, pitch;
		_2byte, _2byte,
	
	SIZEOF_MARKER_USER_DATA,
	
	// angle roll
	_2byte,
	
	// short unused[13];
	1*sizeof(short),
	
	1*sizeof(struct marker_data *),

	// short data_index, data_identifier; // usually object_index (can be model_index), can be NONE
	2*sizeof(short),
	
	_end_bs_array
};


byte_swap_code marker_palette_data_bs_data[]=
{
	_begin_bs_array, 1,
	
	// short type; // of marker (types which don't need palettes don't have them)
	// word flags;
	_2byte,
	_2byte,
	
	// file_tag tag; // subgroup tag based on type (types which don't need subgroups don't have them)
	_4byte,
	
	// short team_index; // to establish ownership; only planned to be used for monsters and observers
	// short pad;
	_2byte,
	sizeof(short),
	
	// unsigned long extra_flags;
	_4byte,

	// short unused[6];
	6*sizeof(short),
	
	// short count; // total references on map; temporary field constructed during palette editing
	// short tag_index; // NONE if couldn't load tag
	2*sizeof(short),
	
	_end_bs_array
};
