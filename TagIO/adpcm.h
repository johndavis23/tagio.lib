#ifndef __adpcm_h__
#define __adpcm_h__

/*
	ADPCM.H
	
	Adaptive Pulse Code Modulation for Sound, based on the IMA findings
	(ie Intel DVI)
*/

#define IMA_SAMPLES_PER_BYTE (2)

#ifdef OBSOLETE
/*
	buffer- contains the compressed sounds.
	number_of_samples- is the number of samples (essentially 2*buffer length, but possibly 1 fewer)
	destination- must be number_of_samples*sizeof(short)
*/
void adpcm_decompress( char *buffer, unsigned int number_of_samples, short *destination );

/*
	source-.contains the linear 16 bit samples to compress
	number_of_samples- is the number of samples (sizeof source pointer)
	destination- must be (number_of_samples/2+number_of_samples&1)*sizeof(char)
*/
void adpcm_compress( short *source, unsigned int number_of_samples, char *destination );

#endif

struct async_adpcm_decompressor_state
{
	short *destination; // just needs to be number_of_samples_to_decompress*sizeof(short)
	int number_of_samples_to_decompress;

	// passed each time.
	char *source_samples;
	int number_of_samples;
	int source_offset; // must be BYTES!
	long predsample;
	int index;
};

void initialize_adpcm_decompressor_state( char *source, int number_of_samples, async_adpcm_decompressor_state *state );

void apple_adpcm_compress(
	short *source,
	unsigned int number_of_samples,
	char *destination,
	short *compression_index,
	short *predicted
	);

void apple_adpcm_packet_decompress(
	short state,
	char *samples,
	short *destination,
	long samples_to_decompress
	);

#endif // __adpcm_h__
