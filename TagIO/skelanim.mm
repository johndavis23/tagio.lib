

#ifndef __skelanim_h__
#include "skelanim.h"
#endif

#ifndef __tags_h__
#include "tags.h"
#endif


#ifndef __read_skl_h__
#include "read_skl.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "formatters.h"

 
#include "tagpick.h"


byte_swap_code bs_skan_bone_frame_pos[] = {
	_begin_bs_array, 1,
	_2byte, _2byte, _2byte,	// offset
	_2byte, _2byte, _2byte, _2byte, // quat
	_end_bs_array
};

byte_swap_code bs_skan_frame[] = {
	_begin_bs_array, 1,
	_4byte, // distance delta
	_2byte,	// attach_bone
	2,	// run-time index
	_4byte, // attach_id
	_end_bs_array
};

skan_frame *import_skan_frame( skan_frame *f, int bone_count )
{
	byte_swap_data_be( "skan frame", f, sizeof(skan_frame), 1, bs_skan_frame );
	byte_swap_data_be( "skan_bone_frame_pos", f + 1, sizeof(skan_bone_frame_pos), bone_count, bs_skan_bone_frame_pos );
	int size = sizeof( skan_frame ) + bone_count * sizeof(skan_bone_frame_pos);
	size = (size + 3) & ~3;
	return (skan_frame *)((char *)f + size);
}

byte_swap_code bs_skan_animation_v1[] = {
	_begin_bs_array, 1,
	MAX_SKAN_ANIM_NAME+1,	// name
	_2byte, // keyframe
	_2byte,	// frame_count
	_2byte, // keybone
	_2byte, // flags
	_4byte, // fps
	_4byte, // offset_scale
	_4byte, // crossfade in
	_4byte, // crossfade interrupt
	_4byte, // crossfade finish
	_end_bs_array
};

byte_swap_code bs_skan_animation[] = {
	_begin_bs_array, 1,
	MAX_SKAN_ANIM_NAME+1,	// name
	_2byte,	// frame_count
	_2byte, // flags
	_2byte, // fixed_fps
	_2byte, // offset_shift
	_4byte, // crossfade in
	_4byte, // crossfade interrupt
	_4byte, // crossfade finish
	_end_bs_array
};

skan_animation *import_skan_animation( skan_animation *a, int bone_count )
{
	byte_swap_data_be( "skan animation", a, sizeof(*a), 1, bs_skan_animation );
	skan_frame *f = (skan_frame *)(a + 1);
	for (int i = 0; i < a->frame_count; ++i) {
		f = import_skan_frame( f, bone_count );
	}
	return (skan_animation *)f;
}


byte_swap_code bs_skan_state[] = {
	_begin_bs_array, 1,
	MAX_SKAN_STATE_NAME+1,	// name
	_2byte,	// anim_count
	_2byte, // flags
	_4byte, // total_probability
	// _2byte * 2 * anim_count
	_end_bs_array
};

skan_state *import_skan_state( skan_state *s )
{
	byte_swap_data_be( "skan state", s, sizeof(*s) - sizeof(s->animations), 1, bs_skan_state );
	byte_swap_memory_be( "skan state anim list", s->animations, s->anim_count * 2, _2byte );
	return (skan_state *)(s->animations + s->anim_count);
}


byte_swap_code bs_skan_bone_base[] = {
	_begin_bs_array, 1,
	MAX_SKAN_BONE_NAME+1,	// name
	_4byte, _4byte, _4byte,		// offset[3]
	_4byte, _4byte, _4byte, _4byte,		// quat[4]
	_4byte,	// parent
	_end_bs_array
};


byte_swap_code bs_skan_tag_header[] = {
	_begin_bs_array, 1,
	_4byte,	// bone_count
	_4byte, // anim_count
	_4byte,	// state_count
	4, 4, 4,	// pointers
	_end_bs_array
};

void *setup_skan_ptrs( skan_tag_header *skan )
{
	skan->baseframe = (skan_bone_base *)(skan + 1);
	skan->animations = (skan_animation **)(skan->baseframe + skan->bone_count);
	skan->states = (skan_state **)(skan->animations + skan->anim_count);
	return skan->states + skan->state_count;
}


void *import_skan( skan_tag_header *skan )
{
	int i;
	void *ptr;
	
	byte_swap_data_be( "skan header", skan, sizeof(*skan), 1, bs_skan_tag_header );
	ptr = setup_skan_ptrs( skan );
	byte_swap_data_be( "skan bone base", skan->baseframe, sizeof(skan_bone_base), skan->bone_count, bs_skan_bone_base );
	for (i = 0; i < skan->anim_count; ++i) {
		skan->animations[i] = (skan_animation *)ptr;
		ptr = import_skan_animation( (skan_animation *)ptr, skan->bone_count );
	}
	for (i = 0; i < skan->state_count; ++i) {
		skan->states[i] = (skan_state *)ptr;
		ptr = import_skan_state( (skan_state *)ptr );
	}
	return ptr;
}

#pragma mark -

void defaultnew_skelanim( tag_entry *e )
{
	skan_tag_header head;
	memset( &head, 0, sizeof(head) );
	byte_swap_data_be( "skan header", &head, sizeof(head), 1, bs_skan_tag_header );
	set_entry_data( e, &head, sizeof(head), false );
}

// only private structure ripped from TskanEditForm
struct statedata
{
	skan_state header;
	int *probabilities;
	skan_animation *animations;
};

void dependency_skelanim( tag_entry *e )
{
/*	skan_bone_base *basebones = (skan_bone_base*)malloc(sizeof(skan_bone_base)*255);
    skan_bone_base *basebones_start = basebones;
    int basebones_length = 0;
	statedata *states;
    statedata *states_start = states;
    int state_length = 0;
	skan_animation *animations;
    skan_animation *animations_start = animations;
    int anim_length = 0;
	const void *tagdata;
	unsigned taglength;
	
	int frame_size;
	
	int i, j;
	
	// most of this ripped from EditAttachments and ReloadTag
	if( !get_entry_data( e, &tagdata, &taglength ) )
		return;
		
	byte *buf = (byte *)malloc( taglength );
	skan_tag_header *skan = (skan_tag_header *)buf;
	memcpy( buf, tagdata, taglength );
	
	void *end = import_skan( (skan_tag_header *)buf );
	if( end != (void *)(buf + taglength) )
	{
		assert( 0 );
		free( buf );
		return;
	}
	
	for( i = 0; i < skan->bone_count; ++i )
	{
        basebones_length++;
        basebones = &skan->baseframe[i];
        basebones++;
	}
    basebones = basebones_start;
    
	frame_size = sizeof(skan_frame);
	frame_size += basebones_length * sizeof(skan_bone_frame_pos);
	frame_size = (frame_size + 3) & ~3;
	
	for( i = 0; i < skan->anim_count; ++i )
	{
		int anim_size = sizeof(skan_animation) + frame_size * skan->animations[i]->frame_count;
		skan_animation *anim = (skan_animation *)malloc( anim_size );
		memcpy( anim, skan->animations[i], anim_size );
        animations = anim;
        animations++;
        anim_length++;
	}
    animations = animations_start;
    
	for( i = 0; i < skan->state_count; ++i )
	{
		statedata *state = new statedata;
		state->header = *(skan->states[i]);
		for( j = 0; j < skan->states[i]->anim_count; ++j )
		{
			state->animations += animations[skan->states[i]->animations[j][0]];
			state->probabilities += skan->states[i]->animations[j][1];
		}
        states = state;
        states++;
        state_length++;
	}

	for( i = 0; i < skan->anim_count; ++i )
	{
		int frame_size = (sizeof(skan_frame) + basebones_length * sizeof(skan_bone_frame_pos) + 3) & ~3;
		skan_frame *frame = (skan_frame *)(animations[i] + 1);

		for( j = 0; j < animations[i].frame_count; ++j )
		{
			if( frame->frame_action != (word)-1 )
			{
				int kind = (frame->frame_action & _skan_frame_action_kind_mask) >> _skan_frame_action_first_kind_bit;
				tag type;
				
				switch( kind )
				{
				case _skan_frame_action_kind_sound:
					type = 'soun';
					break;
				case _skan_frame_action_kind_skmd:
					type = 'skmd';
					break;
				case _skan_frame_action_kind_loli:
					type = 'loli';
					break;
				case _skan_frame_action_kind_lpgr:
					type = 'lpgr';
					break;
				case _skan_frame_action_kind_project:
					type = 'proj';
					break;
				default:
					type = '????';
					break;
				}
					
				if( type != '????' )
				{
					add_tag_dependency( type, frame->action_tag_id );
				}
			}
			frame = (skan_frame *)((byte *)frame + frame_size);
		}
	}
	
	
	free( buf );
	
	// duplication of free_data();
//	basebones.DeleteAll();
    free(basebones);
    
    
	for( i = 0; i < state_length; ++i )
	{
		delete states[i];
	}
    delete states;
	
	for( i = 0; i < animation_length; ++i )
	{
		delete animations[i];
	}
	animations.DeleteAll();*/
    //TODO IMPLEMENT SKL ANIMATIOND EPENDENCY
}

void edit_skelanim( tag_entry *e )
{

}

void update_skelanim( tag_entry *e )
{
	int i, j, k;
	
	for (;;)
	{
		int version = get_entry_version( e );
		const void *tagdata;
		unsigned taglength;
		byte *in_buf;
		byte *out_buf;
		void *ptr;
		switch (version)
		{
			case 1: {
			
				// first load in the old data
				
					if (!get_entry_data( e, &tagdata, &taglength )) return;
					in_buf = (byte *)malloc( taglength );
					skan_tag_header_v1 *v1header = (skan_tag_header_v1 *)in_buf;
					skan_animation_v1 *v1anim;
					skan_frame_v1 *v1frame;
					skan_state *v1state;
					skan_bone_frame_pos *v1pos;
					
					memcpy( in_buf, tagdata, taglength );
					byte_swap_data_be( "skan v1 header", v1header, sizeof(*v1header), 1, bs_skan_tag_header );
					v1header->baseframe = (skan_bone_base *)(v1header + 1);
					v1header->animations = (skan_animation_v1 **)(v1header->baseframe + v1header->bone_count);
					v1header->states = (skan_state **)(v1header->animations + v1header->anim_count);
					ptr = v1header->states + v1header->state_count;
					byte_swap_data_be( "skan bone base v1", v1header->baseframe, sizeof(skan_bone_base), v1header->bone_count, bs_skan_bone_base );
					for (i = 0; i < v1header->anim_count; ++i)
					{
						v1header->animations[i] = (skan_animation_v1 *)ptr;
						v1anim = (skan_animation_v1 *)ptr;
						byte_swap_data_be( "skan animation v1", v1anim, sizeof(*v1anim), 1, bs_skan_animation_v1 );
						v1frame = (skan_frame_v1 *)(v1anim + 1);
						for (j = 0; j < v1anim->frame_count; ++j)
						{
							byte_swap_data_be( "skan frame v1", v1frame, sizeof(*v1frame), 1, bs_skan_frame );
							byte_swap_data_be( "skan bone frame pos v1", v1frame+1, sizeof(skan_bone_frame_pos), v1header->bone_count, bs_skan_bone_frame_pos );
							int size = sizeof( *v1frame ) + v1header->bone_count * sizeof(skan_bone_frame_pos);
							size = (size + 3) & ~3;
							v1frame = (skan_frame_v1 *)((byte *)v1frame + size);
						}
						ptr = v1frame;
					}
					for (i = 0; i < v1header->state_count; ++i)
					{
						v1header->states[i] = v1state = (skan_state *)ptr;
						byte_swap_data_be( "skan state v1", v1state, sizeof(*v1state) - sizeof(v1state->animations), 1, bs_skan_state );
						byte_swap_memory_be( "skan state anim list v1", v1state->animations, v1state->anim_count * 2, _2byte );
						ptr = v1state->animations + v1state->anim_count;
					}
					
				// we've got the old one, now make the new one
				
					skan_animation *v2anim;
					skan_frame *v2frame;
					skan_state *v2state;
					skan_bone_frame_pos *v2pos;
					
					out_buf = (byte *)malloc( taglength * 2 );
					skan_tag_header *v2header = (skan_tag_header *)out_buf;
					v2header->bone_count = v1header->bone_count;
					v2header->anim_count = v1header->anim_count;
					v2header->state_count = v1header->state_count;
					
					v2header->baseframe = (skan_bone_base *)(v2header + 1);
					v2header->animations = (skan_animation **)(v2header->baseframe + v2header->bone_count);
					v2header->states = (skan_state **)(v2header->animations + v2header->anim_count);
					
					ptr = v2header->states + v2header->state_count;
					
					memcpy( v2header->baseframe, v1header->baseframe, v2header->bone_count * sizeof(skan_bone_base) );
					
					for (i = 0; i < v2header->anim_count; ++i)
					{
						v1anim = v1header->animations[i];
						v2header->animations[i] = v2anim = (skan_animation *)ptr;
						float max_offset = 32000.0f * v1anim->offset_scale;
						float rescale = 1.0f;
						int shift = 0;
						while (max_offset * rescale < 16383.0f)
						{
							rescale *= 2.0f;
							shift += 1;
						}
						while (max_offset * rescale > 32767.0f)
						{
							rescale *= 0.5f;
							shift -= 1;
						}
						memcpy( v2anim->name, v1anim->name, sizeof(v2anim->name) );
						v2anim->name[MAX_SKAN_ANIM_NAME] = 0;
						v2anim->frame_count = v1anim->frame_count;
						v2anim->flags = v1anim->flags;
						v2anim->fixed_fps = float_to_short_fixed( v1anim->fps );
						v2anim->offset_shift = shift;
						v2anim->crossfade_in_time = v1anim->crossfade_in_time;
						v2anim->crossfade_interrupted_time = v1anim->crossfade_interrupted_time;
						v2anim->crossfade_finished_time = v1anim->crossfade_finished_time;
						
						v1frame = (skan_frame_v1 *)(v1anim + 1);
						v2frame = (skan_frame *)(v2anim + 1);
						for (j = 0; j < v2anim->frame_count; ++j)
						{
							if (v2anim->flags & _skelanim_turn_driven_flag)
							{
								v2frame->fixed_distance_delta = (int)round( v1frame->distance_delta * FULL_CIRCLE / 360.0 );
							}
							else
							{
								v2frame->fixed_distance_delta = float_to_fixed( v1frame->distance_delta );
							}
							v2frame->frame_action = v1frame->frame_action;
							v2frame->action_tag_index = v1frame->action_tag_index;
							v2frame->action_tag_id = v1frame->action_tag_id;
							
							if (j == v1anim->keyframe)
							{
								v2frame->frame_action |= _skan_frame_action_keyframe_flag;
								v2frame->frame_action = (v2frame->frame_action & ~_skan_frame_action_bone_mask) | (v1anim->keybone << _skan_frame_action_first_bone_bit);
							}
							
							v1pos = (skan_bone_frame_pos *)(v1frame + 1);
							v2pos = (skan_bone_frame_pos *)(v2frame + 1);
							for (k = 0; k < v2header->bone_count; ++k)
							{
								v2pos->shortOffset[0] = float_to_clamped_short( v1pos->shortOffset[0] * v1anim->offset_scale * rescale );
								v2pos->shortOffset[1] = float_to_clamped_short( v1pos->shortOffset[1] * v1anim->offset_scale * rescale );
								v2pos->shortOffset[2] = float_to_clamped_short( v1pos->shortOffset[2] * v1anim->offset_scale * rescale );
								
								v2pos->shortQuat[0] = float_to_clamped_short( v1pos->shortQuat[0] * 32767.0 / 32000.0 );
								v2pos->shortQuat[1] = float_to_clamped_short( v1pos->shortQuat[1] * 32767.0 / 32000.0 );
								v2pos->shortQuat[2] = float_to_clamped_short( v1pos->shortQuat[2] * 32767.0 / 32000.0 );
								v2pos->shortQuat[3] = float_to_clamped_short( v1pos->shortQuat[3] * 32767.0 / 32000.0 );
								
								++v1pos;
								++v2pos;
							}
							
							int v1size = sizeof( *v1frame ) + v1header->bone_count * sizeof(skan_bone_frame_pos);
							v1size = (v1size + 3) & ~3;
							v1frame = (skan_frame_v1 *)((byte *)v1frame + v1size);
							
							int v2size = sizeof( *v2frame ) + v2header->bone_count * sizeof(skan_bone_frame_pos);
							v2size = (v2size + 3) & ~3;
							v2frame = (skan_frame *)((byte *)v2frame + v2size);
						}
						
						ptr = v2frame;
					}
					
					for (i = 0; i < v2header->state_count; ++i)
					{
						v1state = v1header->states[i];
						v2header->states[i] = v2state = (skan_state *)ptr;
						memcpy( v2state, v1state, sizeof(skan_state) + (v1state->anim_count - 1) * 2 * sizeof(short) );
						ptr = v2state->animations + v2state->anim_count;
					}
					
					assert( (byte *)ptr - out_buf <= taglength );
					
				// finally byte-swap and export the output
					
					byte_swap_data_be( "skan bone base", v2header->baseframe, sizeof(skan_bone_base), v2header->bone_count, bs_skan_bone_base );
					
					for (i = 0; i < v2header->anim_count; ++i)
					{
						v2anim = v2header->animations[i];
						v2frame = (skan_frame *)(v2anim + 1);
						for (j = 0; j < v2anim->frame_count; ++j)
						{
							byte_swap_data_be( "skan frame pos", v2frame+1, sizeof(skan_bone_frame_pos), v2header->bone_count, bs_skan_bone_frame_pos );
							byte_swap_data_be( "skan frame", v2frame, sizeof(*v2frame), 1, bs_skan_frame );
							
							int v2size = sizeof( *v2frame ) + v2header->bone_count * sizeof(skan_bone_frame_pos);
							v2size = (v2size + 3) & ~3;
							v2frame = (skan_frame *)((byte *)v2frame + v2size);
						}
						byte_swap_data_be( "skan animation", v2anim, sizeof(*v2anim), 1, bs_skan_animation );
					}
					
					for (i = 0; i < v2header->state_count; ++i)
					{
						v2state = v2header->states[i];
						byte_swap_memory_be( "skan state anim list", v2state->animations, v2state->anim_count * 2, _2byte );
						byte_swap_data_be( "skan state", v2state, sizeof(*v2state) - sizeof(v2state->animations), 1, bs_skan_state );
					}
					
					byte_swap_data_be( "skan header", v2header, sizeof(*v2header), 1, bs_skan_tag_header );
					
					set_entry_data( e, out_buf, (byte *)ptr - out_buf, false );
					set_entry_version( e, 2 );
					free( out_buf );
				} break;
				
			case 2:
				return;
			default:
				assert(0);
				return;
		}
	}
}

#pragma mark -


